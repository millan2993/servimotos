<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes();

Route::get('/account', 'HomeController@index')->name('account');

// Obtener los estados de un país
Route::post('/getStatesById', 'RegionController@getStatesById');

// Obtener las ciudades de un estado
Route::post('/getCitiesByState', 'RegionController@getCitiesByState');