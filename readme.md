## Sobre Servimotos

Servimotos es un proyecto realizado con el fin de ser utilizado en talleres de motos, llevando tecnología y procesos metodológicos como el panel canvas a aquellas personas que deseen optimizar los procesos internos de la compañía.

### Proceso de instalación

A continuación se presenta una guia de instalación en un entorno Ubuntu.

##### Instalar librerías requeridas
Ejecutar en la consonla: apt-get update && apt-get -y upgrade && apt-get -y install apache2 php php-mysql libapache2-mod-php curl zip vim php-mbstring php-dom php-json php-mcrypt php-curl php-zip wget mysql-server mysql-client

##### Configurar base de datos
Ejecutar:
- mysql -h localhost -u root -p
- create database servimotos character set utf8;

##### Configurar apache
- Ejecutar: echo "ServerName localhost" >> /etc/apache2/apache2.conf && a2enmod rewrite

- En el archivo /etc/apache2/apache2.conf en el directorio /var/www/ reemplazar la siguiente directiva:
`AllowOverride All`

- service apache2 restart

##### Instalar composer
curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

##### Descargar proyecto 
- descomprimir y copiar este proyecto en /var/www/html/
- Llevar el proyecto a la carpeta principal: mv /var/www/servimotos-master/ /var/www/html


##### Instalar proyecto
- cd /var/www/html/
- composer install --no-dev


##### Configuraciones del proyecto
- Renombrar el archivo .env.copy a .env y configurar la conexión a la base de datos.
- Ejecutar: 
    - php artisan key:generate
    - chown -R www-data:www-data /var/www/html && chmod -R 777 /var/www/html
    - php artisan migrate

##### Configuración inicial de la base de datos
Ejecutar el script "datos iniciales.sql" en la base de datos.


### Licencia

Este proyecto fue realizado con fines educativos y está licenciado bajo la [licencia MIT](http://opensource.org/licenses/MIT).
