<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Payments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('reference', 60)->nullable();
            $table->dateTime('date');
            $table->double('value', 10, 2);
            $table->string('comments', 2000);
            $table->tinyInteger('state');
            $table->timestamps();

            $table->integer('id_orden')->unsigned();

            $table->foreign('id_orden', 'fk_payments_ordenes')
                ->references('id')->on('ordenes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payments', function (Blueprint $table) {
            $table->dropForeign('fk_payments_ordenes');
        });

        Schema::dropIfExists('payments');
    }
}
