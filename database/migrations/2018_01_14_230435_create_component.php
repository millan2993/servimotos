<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComponent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('desc', 300);
            $table->tinyInteger('status');
            $table->timestamps();

            $table->integer('id_module')->unsigned();
            $table->foreign('id_module', 'fk_components_modules')
                ->references('id')->on('modules');
        });

        Schema::table('permissions', function (Blueprint $table) {
            $table->integer('id_component')->nullable()->unsigned();

            $table->foreign('id_component', 'fk_permissions_components')
                ->references('id')->on('components');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropForeign('fk_permissions_components');
            $table->dropColumn('id_component');
        });

        Schema::table('components', function (Blueprint $table) {
            $table->dropForeign('fk_components_modules');
        });

        Schema::dropIfExists('components');
    }
}
