<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ordenes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordenes', function (Blueprint $table) {
            $table->increments('id');
            $table->text('observation')->nullable();
            $table->dateTime('fechaEntrega')->nullable();
            $table->tinyInteger('isVehicle')->nullable();
            $table->timestamps();

            $table->integer('id_client')->unsigned()->nullable();
            $table->integer('id_vehicle')->unsigned()->nullable();

            $table->foreign('id_client', 'fk_ordenes_clients')
                ->references('id')->on('clients');

            $table->foreign('id_vehicle', 'fk_ordenes_vehicles')
                ->references('id')->on('vehicles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ordenes', function (Blueprint $table) {
            $table->dropForeign('fk_ordenes_clients');
            $table->dropForeign('fk_ordenes_vehicles');
        });

        Schema::dropIfExists('ordenes');
    }
}
