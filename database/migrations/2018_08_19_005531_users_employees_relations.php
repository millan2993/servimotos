<?php

use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersEmployeesRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        try {
            DB::beginTransaction();

            Schema::table('users', function (Blueprint $table) {
                $table->integer('id_employee')->unsigned()->nullable();
                $table->integer('id_client')->unsigned()->nullable();

                $table->foreign('id_employee', 'fk_users_employees')
                    ->references('id')->on('employees');

                $table->foreign('id_client', 'fk_users_clients')
                    ->references('id')->on('clients');

                $table->dropColumn('avatar');
            });

            Schema::table('employees', function (Blueprint $table) {
                $table->string('photo', 250)->nullable();
            });

            Schema::table('clients', function (Blueprint $table) {
                $table->string('photo', 250)->nullable();
            });

            DB::commit();

        } catch(QueryException $e) {
            DB::rollBack();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
