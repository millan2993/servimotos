<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vehicles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicles', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('type')->unsigned();
            $table->string('placa', 10)->nullable();
            $table->string('marca')->nullable();
            $table->mediumInteger('modelo')->unsigned()->nullable();
            $table->string('color', 45)->nullable();
            $table->timestamps();

            $table->integer('id_city')->unsigned()->nullable();

            $table->foreign('id_city', 'fk_vehicles_city')
                ->references('id')->on('cities');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vehicles', function (Blueprint $table) {
            $table->dropForeign('fk_vehicles_city');
        });

        Schema::dropIfExists('vehicles');
    }
}
