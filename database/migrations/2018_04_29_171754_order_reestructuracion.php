<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderReestructuracion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_status', function (Blueprint $table) {
            $table->tinyInteger('id')->unsigned()->default(1);
            $table->string('name', 20);

            $table->primary('id', 'pk_orders_status');
        });

        DB::table('orders_status')->insert([
            ['id' => 1, 'name' => 'Abierta'],
            ['id' => 2, 'name' => 'En proceso'],
            ['id' => 3, 'name' => 'Cerrada'],
            ['id' => 4, 'name' => 'Anulada']
        ]);


        /* **************** ORDERS *************** */
        Schema::rename('ordenes', 'orders');

        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('isVehicle');

            $table->string('code', 20);
            $table->dateTime('expected_date')->nullable();
            $table->dateTime('delivered_date')->nullable();
            $table->tinyInteger('id_status')->unsigned();

            $table->foreign('id_status', 'fk_orders_status')
                ->references('id')->on('orders_status');

            $table->unique('code', 'uq_orders_code');
        });


        /* ********************  Order details  ***************** */
        Schema::rename('orden_detalles', 'order_details');

        Schema::table('order_details', function (Blueprint $table){
            $table->integer('amount');
        });


        /* **************  Services  *************** */
        Schema::table('services', function (Blueprint $table) {
            $table->double('price', 12, 3);
        });


        /* ******************** Activity status ************** */
        Schema::create('activity_status', function (Blueprint $table) {
            $table->tinyInteger('id')->unsigned()->default(1);
            $table->string('name', 20);

            $table->primary('id', 'pk_activity_status_id');
        });

        DB::table('activity_status')->insert([
            ['id' => 1, 'name' => 'Pendiente' ],
            ['id' => 2, 'name' => 'En proceso'],
            ['id' => 3, 'name' => 'Terminada' ]
        ]);


        /* *****************  Activities  ***************  */
        Schema::table('activities', function (Blueprint $table) {
            $table->string('code', 20);
            $table->float('execution_percent', 4, 2)->nullable()->default(0);
            $table->integer('id_service')->unsigned()->nullable();

            $table->unique('code', 'uq_activities_code');

            $table->foreign('id_service', 'fk_activities_services')
                ->references('id')->on('services');

            $table->foreign('id_status', 'fk_activities_status')
                ->references('id')->on('activity_status');
        });


        Schema::table('clients', function (Blueprint $table) {
            $table->tinyInteger('sex');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
