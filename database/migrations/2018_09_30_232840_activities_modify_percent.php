<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ActivitiesModifyPercent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activities', function (Blueprint $table) {

//            $table->double('execution_percent', 5, 2)->default(0)->change();
//            ALTER TABLE activities MODIFY execution_percent DOUBLE(5,2) DEFAULT 0;

        });

        DB::statement('ALTER TABLE activities MODIFY execution_percent DOUBLE(5,2) DEFAULT 0');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE activities MODIFY execution_percent DOUBLE(4,2) DEFAULT 0');
    }
}
