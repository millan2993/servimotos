<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Employees extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('sex');
            $table->string('first_name', 45);
            $table->string('last_name', 45);
            $table->tinyInteger('type_document');
            $table->string('document', 15)->unique();
            $table->string('email', 200)->nullable();
            $table->string('cellphone', 15)->nullable();
            $table->string('phone', 15)->nullable();
            $table->timestamps();

            $table->integer('id_user')->unsigned()->nullable();
            $table->integer('id_address')->unsigned()->nullable();

            $table->foreign('id_address', 'fk_employees_address')
                ->references('id')->on('address');

            $table->foreign('id_user', 'fk_employees_users')
                ->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropForeign('fk_employees_address');
        });

        Schema::dropIfExists('employees');
    }
}
