<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Address extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address', function (Blueprint $table) {
            $table->increments('id');
            $table->string('primary', 200)->nullable();
            $table->string('secundary', 200)->nullable();
            $table->string('barrio', 100)->nullable();
            $table->string('postal_code', 10)->nullable();
            $table->integer('id_city')->unsigned();

            $table->foreign('id_city', 'fk_address_cities')
                ->references('id')->on('cities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('address', function (Blueprint $table) {
            $table->dropForeign('fk_address_cities');
        });

        Schema::dropIfExists('address');
    }
}
