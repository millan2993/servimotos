<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdenDetalles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orden_detalles', function (Blueprint $table) {
            $table->increments('id');
            $table->text('observation')->nullable();
            $table->double('price', 10, 2);
            $table->timestamps();

            $table->integer('id_order')->unsigned();
            $table->integer('id_service')->unsigned();

            $table->foreign('id_order', 'fk_order_details_orders')
                ->references('id')->on('ordenes');

            $table->foreign('id_service', 'fk_ordendetalles_services')
                ->references('id')->on('services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orden_detalles', function (Blueprint $table) {
            $table->dropForeign('fk_ordendetalles_ordenes');
            $table->dropForeign('fk_ordendetalles_services');
        });

        Schema::dropIfExists('orden_detalles');
    }
}
