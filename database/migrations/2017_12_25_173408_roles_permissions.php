<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RolesPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles_permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_role')->unsigned();
            $table->integer('id_permission')->unsigned();
            $table->timestamps();

            $table->foreign('id_permission', 'fk_rolespermissions_permissions')
                ->references('id')->on('permissions')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('id_role', 'fk_rolespermissions_roles')
                ->references('id')->on('roles');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles_permissions', function (Blueprint $table) {
            $table->dropForeign('fk_rolespermissions_permissions');
            $table->dropForeign('fk_rolespermissions_roles');
        });

        Schema::dropIfExists('roles_permissions');
    }
}
