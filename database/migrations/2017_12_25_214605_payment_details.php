<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PaymentDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ip_address', 45)->nullable();
            $table->string('location', 45)->nullable();
            $table->string('user_agent', 200);
            $table->timestamps();

            $table->integer('id_payment')->unsigned();

            $table->foreign('id_payment', 'fk_paymentdetails_payments')
                ->references('id')->on('payments');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payment_details', function (Blueprint $table) {
            $table->dropForeign('fk_paymentdetails_payments');
        });

        Schema::dropIfExists('payment_details');
    }
}
