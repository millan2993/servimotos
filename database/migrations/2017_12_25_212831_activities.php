<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Activities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('estimated_time')->nullable();
            $table->tinyInteger('id_status')->unsigned()->default(1);
            $table->dateTime('start')->nullable();
            $table->dateTime('end')->nullable();
            $table->timestamps();

            $table->integer('id_order_detail')->unsigned()->nullable();
            $table->integer('id_employee')->unsigned()->nullable();

            $table->foreign('id_order_detail', 'fk_activities_orderdetails')
                ->references('id')->on('orden_detalles');

            $table->foreign('id_employee', 'fk_activities_employees')
                ->references('id')->on('employees');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activities', function (Blueprint $table) {
            $table->dropForeign('fk_activities_ordendetalles');
            $table->dropForeign('fk_activities_employees');
        });

        Schema::dropIfExists('activities');
    }
}
