/**
 * Created by alex on 17/02/18.
 */

// Variable para definir si se está creando un tipo de servicio
var adding = false;

$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    /**
     * Modulo de servicios
     */

    // Adicionar un nuevo tipo de servicio
    $('#btn-new-typeservice').on('click', function () {
        if (!adding) {
            adding = true;
            $('table tbody').append('<tr class="animated">' +
                '<td><input class="form-control" type="text" name="name" placeholder="Nombre" autofocus></td>' +
                '<td><textarea class="form-control" name="desc" rows="1" placeholder="Descripción"></textarea></td>' +
                '<td class="text-navy">' +
                    '<i class="fa fa-refresh iHandLink" title="Cancelar" onclick="cancelNewTypeService(this)"></i> ' +
                    '<i class="fa fa-save iHandLink" title="Guardar" onclick="saveNewTypeService(this)"></i>' +
                '</td></tr>');
        }
    });


    $('#country').on('change', getStatesById);
    $('#state').on('change', getCityByState);


    // adicionar item a orden de compra
    $('#add_item').on('click', add_itemOC);


    /**
     * Validar el formulario de ordenes de compra
     */
    $('form#createOC').submit(function (e) {
        e.preventDefault();

        // validar si hay errores
        var errores = validarCrearOC();
        if (errores.length > 0) {
            show_error(errores, 'warning');
            return false;
        }

        this.submit();
    });

    delete_item_service();


    $('#client').on('change', loadVehicles);

    $('#typeuser').on('change', loadTypeUsers);

    $('#owner').on('change', updateusername);


    $('#execution_percent_edit_activity').on('focus', function() {
        var value = $('#execution_percent').val();
        $('#execution_percent_edit_activity').text(value);
    });


    $('#execution_percent_edit_activity').on('focusout', function() {
        var value = $('#execution_percent_edit_activity').text();

        value = (value.length == 0 || value < 0) ? 0 : value;
        value = (value > 100) ? 100 : value;

        $('#execution_percent_edit_activity').text(value + ' %');
        $('#execution_percent').val(value);
    });




    $("#execution_percent_edit_activity").keypress(function(e) {

        var value = parseInt($('#execution_percent_edit_activity').text());

        switch (e.key) {
            case 'Backspace':
            case 'Delete':
            case 'F5':
                return true;
            case 'Escape':
            case 'Enter':
                $("#execution_percent_edit_activity").blur();
                break;
            case 'ArrowUp':
                if (value + 1 <= 100) {
                    $('#execution_percent_edit_activity').text(value + 1);
                }
                break;
            case 'ArrowDown':
                if (value - 1 >= 0) {
                    $('#execution_percent_edit_activity').text(value - 1);
                }
                break;
        }

        if (isNaN(String.fromCharCode(e.which))) {
            e.preventDefault();
        }

        return true;
    });

});



/**
 * Crear un nuevo tipo de servicio
 * @param button
 */
function saveNewTypeService(button) {

    var inputName = $(button).parent().siblings().children('input');
    var tdName = inputName.parent();
    var name = inputName.val();

    var textAreaDesc = $(button).parent().siblings().children('textarea');
    var desc = textAreaDesc.val();

    // Validar que el nombre no esté vacío
    if (name == '') {
        if (!tdName.hasClass('has-error')) {
            tdName.addClass('has-error');
            tdName.append('<span class="help-block m-b-none">El nombre es obligatorio</span>');
            inputName.focus();
        }
        return;
    }

    var tr = $(button).parents('tr');
    var id = tr.attr('id');

    if (typeof id === 'undefined') {
        url = 'typeservices/ajaxcreate';
    } else {
        url = 'typeservices/ajaxedit/' + id.match('[0-9]+')[0];
    }


    $.ajax({
        url: url,
        type: "post",
        data: {'name': name, 'desc': desc },
        dataType:"json",
        success:function(response) {

            toastrOptions();

            // Mostrar mensaje en caso que haya ocurrido un error
            if (typeof response.errors !== 'undefined') {
                if (typeof id === 'undefined') {
                    Command: toastr.error(response.errors.message, 'Error al crear el tipo de servicio');
                } else {
                    Command: toastr.error(response.errors.message, 'Error al editar el tipo de servicio');
                }
                return;
            } else {
                if (typeof id === 'undefined') {
                    Command: toastr.success('El tipo de servicio se ha creado exitosamente');
                } else {
                    Command: toastr.success('El tipo de servicio se ha actualizado exitosamente');
                }
            }


            // Ingresar los campos guardados en las columnas
            tdName.html(name);
            textAreaDesc.parent().html('<p>' + desc + '</p>');
            $(button).parent().html(
                '<i class="fa fa-edit iHandLink" title="Editar" onclick="editTypeService(this)"></i> ' +
                '<i class="fa fa-trash-o iHandLink" title="Eliminar" onclick="deleteTypeService(this)"></i>');


            // Asignar el atributo id si se está creando
            if (typeof id === 'undefined') {
                tdName.parent().attr('id', 'row-' + response.id)
            }

            adding = false;
        }
    });

}


/**
 * Cancelar el crear un nuevo tipo de servicio
 * @param button
 */
function cancelNewTypeService(button) {
    $(button).parents('tr').addClass('slideOutRight');
    setTimeout(function () {
        $(button).parents('tr').remove();
    }, 800);
    adding = false;
}


/**
 * Eliminar un tipo de servicio
 *
 * @param button
 */
function deleteTypeService(button) {
    var tr = $(button).parents('tr');
    var id = tr.attr('id').match('[0-9]+')[0];

    $.ajax({
        url: '/account/typeservices/ajaxdelete/' + id,
        type: "get",
        dataType:"json",
        success:function(response) {

            toastrOptions();

            // Mostrar mensaje en caso que haya ocurrido un error
            if (typeof response.errors !== 'undefined' || response.delete == false) {
                Command: toastr.error(response.errors.message, 'Ha ocurrido un error al eliminar el tipo de servicio');
                return;
            }

            // En caso que haya sido eliminado, se muestra el mensaje y se elimina el row de la tabla
            if(response.deleted == true) {
                Command: toastr.success('El tipo de servicio se ha eliminado exitosamente');

                tr.addClass('slideOutRight');
                setTimeout(function () {
                    tr.remove();
                }, 800);
            }

        }
    });
}



/**
 * Editar un tipo de servicio
 *
 * @param button
 */
var name, desc;
function editTypeService(button) {
    var tr = $(button).parents('tr');
    var id = tr.attr('id').match('[0-9]+')[0];

    var tdName = $(tr).children('td').first();
    name = tdName.text();

    var tdDesc = $(tr).children('td').eq(1);
    desc = tdDesc.children('p').text();

    $(tr).html('<td class="col-md-3"><input class="form-control" type="text" name="name" placeholder="Nombre" autofocus value="' + name + '"></td>' +
                '<td><textarea class="form-control" name="desc" rows="1" placeholder="Descripción">'+ desc +'</textarea></td>' +
                '<td class="col-md-1 text-navy">' +
                    '<i class="fa fa-refresh iHandLink" title="Refrescar" onclick="refreshTypeService(this)"></i> ' +
                    '<i class="fa fa-save iHandLink" title="Guardar" onclick="saveNewTypeService(this)"></i>' +
                '</td>');

}


/**
 * Inicializar datos al editar tipo de servicio
 *
 * @param button
 */
function refreshTypeService(button) {
    $(button).parent().siblings().children('input').val(name);
    $(button).parent().siblings().children('textarea').val(desc);
}


/**
 * Obtener los estados de un pais
 */
function getStatesById() {

    var country = $(this).val();

    $.ajax({
        url: '/getStatesById',
        type: "post",
        data: { id: country },
        dataType:"json",
        success:function(response) {
            $('#state').html('<option></option>');
            $('#state').select2('val', '');

            var states = response.states;
            for (var i = 0; i < states.length; i++) {
                $('#state').append(
                    '<option value="'+ states[i].id + '">'+ states[i].name +'</option>'
                );
            }
        }
    });
}



/**
 * Obtener las ciudades por estado
 */
function getCityByState() {

    var state = $(this).val();

    $.ajax({
        url: '/getCitiesByState',
        type: "post",
        data: { id: state },
        dataType:"json",
        success:function(response) {
            $('#city').html('<option></option>');
            $('#city').select2('val', '');

            var cities = response.cities;
            for (var i = 0; i < cities.length; i++) {
                $('#city').append(
                    '<option value="'+ cities[i].id + '">'+ cities[i].name +'</option>'
                );
            }
        }
    });
}


/**
 * Adicionar un item - servicio a una orden de compra
 */
function add_itemOC() {

    var row = $("#items tr:last");

    row.find(".order-service").each(function(index) {
        $(this).select2('destroy');
    });

    var newrow = row.clone();

    $(newrow).find( "select[name='service[]']" ).val(0);
    $(newrow).find( "input[name='amount[]']" ).val(1);
    $(newrow).find( "input[name='price[]']" ).val('');
    $(newrow).find( "input[name='subtotal[]']" ).val('');
    newrow.addClass('animated bounceInRight');

    $("#items").append(newrow);

    $(".order-service").select2({
        placeholder: "Seleccione un servicio",
        width: '100%'
    });

    delete_item_service();
}


/**
 * Eliminar un item - servicio de una orden de compra
 */
function delete_item_service(){

    $('.delete').off();
    $('.delete').click(function() {
        var items = $('#items').children().length;

        if (items > 1) {
            var item = $(this).closest('tr.item');
            item.addClass('animated bounceOutRight');

            setTimeout(function () {
                item.remove();
            }, 500);
        }
    });
}



/**
 * Obtener precio al seleccionar un servicio en orden de compra
 */
function getServicesDetailsOC(service) {

    var items = $(service).closest('tr').siblings();
    var services = $(items).find('select[name="service[]"]');

    // validar que hayan otros servicios diferentes al actual
    if (services.length > 0) {

        $(services).each(function () {
            if (service.value == this.value) {
                $(service).val(0).blur();
                $(service).parent().addClass('has-error');
                console.log('no se puede adicionar');
                return false;
            }
        });
    }


    // validar que sea un servicio válido
    if (service.value > 0) {

        $.ajax({
            url: '/account/services/getprice',
            type: "post",
            data: {id: service.value},
            dataType: "json",
            success: function (response) {
                var item = $(service).closest('tr.item');

                var amount = $(item).find("input[name='amount[]']").val();
                var subtotal = response.price * amount;

                $(item).find("input[name='price[]']").val(response.price);
                $(item).find("input[name='subtotal[]']").val(subtotal);
            }
        });
    }
}


/**
 * Calcular el subtotal
 * @param field
 */
function calcSubtotalOC(field) {
    var item = $(field).closest('tr.item');

    var amount = $(item).find( "input[name='amount[]']" ).val();
    var price = $(item).find( "input[name='price[]']" ).val();

    var subtotal = price * amount;
    $(item).find( "input[name='subtotal[]']" ).val(subtotal);
}


/**
 * Validar datos al crear una OC
 */
function validarCrearOC() {
    var errors = [] ;

    // validar servicios seleccionados
    $('#items select[name="service[]"]').each(function () {

        if (this.value <= 0) {
            $(this).parent().addClass('has-error');
            errors.push('Debe seleccionar un servicio.');
            return false;
        }
    });

    // validar cantidades
    $('#items input[name="amount[]"]').each(function () {
        let amount = (this.value == '') ? 0 : parseInt(this.value);

        if (typeof amount != 'number' || amount <= 0) {
            $(this).parent().addClass('has-error');
            errors.push('La cantidad debe ser un número mayor a 0.');
            return false;
        }
    });

    // Validar precios unitarios
    $('#items input[name="price[]"]').each(function () {
        let price = (this.value == '') ? 0 : parseInt(this.value);

        if (typeof price != 'number' || price <= 0) {
            $(this).parent().addClass('has-error');
            errors.push('El precio debe ser un valor mayor a 0.');
            return false;
        }
    });

    return errors;
}



/**
 * Configuración por defecto para las alertas
 */
function toastrOptions() {
    toastr.options = {
        "closeButton": true,
        "progressBar": true,
        "preventDuplicates": false,
        "positionClass": "toast-top-right",
        "showDuration": "100",
        "hideDuration": "800",
        "timeOut": "5000",
        "extendedTimeOut": "100",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
}


/**
 * Actualizar porcentaje de ejecución al cambiar de lista en el board
 *
 * @param event
 * @param ui
 */
function changeListOnBoard(event, ui) {

    var idActivity = $(ui.item).attr('id');
    var board = $(event.target).attr('id');
    // $('.connectList').sortable( "disable" );

    $.ajax({
        url: '/account/activities/changestatus',
        type: "post",
        data: {id: idActivity, 'board': board},
        dataType: "json",
        success: function (response) {
            toastrOptions();

            if (response.status == true) {
                Command: toastr.success('La actividad ha cambiado de estado.');
            } else {
                Command: toastr.error('Ha ocurrido un error inesperado. Intentelo de nuevo.');
            }
        }
    });
}



/**
 * cargar los vehiculos de un cliente
 */
function loadVehicles() {
    var client = $(this).val();
    $('#vehicle').val(null).trigger('change');

    $.ajax({
        url: '/account/vehicles/vehiclesByClient',
        type: "post",
        data: { 'client': client },
        dataType:"json",
        success:function(response) {
            $('#vehicle').html('<option></option>');

            var vehicles = response.vehicles;
            for (var i = 0; i < vehicles.length; i++) {
                $('#vehicle').append(
                    '<option value="'+ vehicles[i].id + '">'+ vehicles[i].placa +'</option>'
                );
            }
        }
    });
}




/**
 * Cargar los empleados o clientes para los tipos de usuarios
 */
function loadTypeUsers() {
    var type = $(this).val();
    $('#owner').val(null).trigger('change');

    var url;
    if (type == 1) {
        url = '/account/employees/loadToSelect';
    } else {
        url = '/account/clients/loadToSelect';
    }

    $.ajax({
        url: url,
        type: "post",
        dataType:"json",
        success:function(response) {
            $('#owner').html('<option></option>');

            var data = response.data;
            for (var i = 0; i < data.length; i++) {
                $('#owner').append(
                    '<option value="' + data[i].id + '">' + data[i].name + '</option>'
                );
            }

            if (response.status != 'ok') {
                console.log(response.message);
            }
        }
    });
}


/**
 * Actualizar le nombre de usuario
 */
function updateusername() {
    var username = $(this).find('option:selected').text();
    $('#name').val(username);
}


// Tooltips para las notificaciones
$('.tooltip').tooltip({
    selector: "[data-toggle=tooltip]",
    container: "body"
});


/**
 * Mostrar mensajes de error
 *
 * @param msg
 * @param type
 */
function show_error(msg, type) {
    msg = (typeof msg === 'object') ? msg.join(' <br> ') : msg;

    $('#errors').html('<div class="alert alert-' + type + ' ">' +
            '<button type="button" class="close text-center" data-dismiss="alert" aria-hidden="true">×</button>' +
            '<strong>' + msg + '</strong>' +
        '</div>');

    $('#errors').show();
}


/**
 *
 * @param table id de la tabla
 */
function createTable(table) {

    $(table).DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        "language": {
            "paginate": {
                "previous": "Atrás",
                "next": 'Siguiente'
            },
            "emptyTable":     "No hay datos disponibles",
            "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
            "infoEmpty":      "Mostrando 0 registros",
            "infoFiltered":   "(filtrados de _MAX_ registros)",
            "lengthMenu":     "Mostrando _MENU_ registros",
            "loadingRecords": "Cargando...",
            "search":         "Buscar:",
            "zeroRecords":    "No se encontraron registros",
            "thousands":      ".",
            "decimal":        ","
        },
//                searching: false,
//                info: false,
//                paging: false,
//                lengthChange:false,
        initComplete : function() {
            $("#service_filter").detach().appendTo('#new-search-area');
        },
        buttons: [
            {
                extend: 'excel',
                text: 'Exportar a Excel',
                title: 'Services',
                className: [
                    'hidden-xs'
                ]
            },
            {
                extend: 'pdf',
                text: 'Exportar a PDF',
                title: 'Services',
                className: [
                    'hidden-xs'
                ]
            },
            {
                extend: 'print',
                text: 'Imprimir',
                className: [
                    'hidden-xs'
                ],
                customize: function (win){
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                        .addClass('compact')
                        .css('font-size', 'inherit');
                }
            }
        ]
    });

}
