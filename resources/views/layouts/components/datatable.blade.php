<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover dataTables-example" >
        <thead>
        <tr>
            @foreach($titles as $title)
                <th>{{ $title }}</th>
            @endforeach
        </tr>
        </thead>

        <tbody>
        @foreach($data as $value)
            <tr>
                <td class="col-md-2">{{ $value->name }}</td>
                <td class="col-md-1 text-navy"><i class="fa {{ ($value->status) ? 'fa-check' : 'fa-times'}}"></i></td>
                <td class="col-md-2">{{ $value->module }}</td>
                <td class="col-md-4">
                    <p>{{ $value->desc }}</p>
                </td>
                <td class="text-navy col-md-3">
                    <a href="{{ url('account/permissions/show/'.$value->id) }}" class="btn btn-primary">Ver</a>
                    <a href="{{ url('account/permissions/edit/'.$value->id) }}" class="btn btn-primary">Editar</a>
                    <a href="{{ url('account/permissions/destroy/'.$value->id) }}" class="btn btn-primary">Eliminar</a>
                </td>
            </tr>
        @endforeach

        </tbody>

    </table>
</div>