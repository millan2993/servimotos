@if($errors->has($input))
    <span class="help-block m-b-none">
        <ul>
        @foreach($errors->get($input) as $err)
            <li>{{ $err }}</li>
        @endforeach
        </ul>
    </span>
@elseif($errors->errores->has($input) )
    <span class="help-block m-b-none">
        <ul>
        @foreach($errors->errores->get($input) as $err)
            <li>{{ $err }}</li>
        @endforeach
        </ul>
    </span>
@endif