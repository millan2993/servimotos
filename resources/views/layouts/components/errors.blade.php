@php
    if (Session::has('success')) {
        $class = 'alert-success';
        $messages = Session::pull('success');

    } elseif (Session::has('warning')) {
        $class = 'alert-warning';
        $messages = Session::pull('warning');

    } elseif (Session::has('info')) {
        $class = 'alert-info';
        $messages = Session::pull('info');

    } else {
        $class = 'alert-danger';
        $messages = Session::pull('error');
    }
@endphp

@if ($messages)
    <div class="alert alert-block {{ $class }}">
        <button type="button" class="close" data-dismiss="alert">×</button>
        @if(is_array($messages))
            <ul>
                @foreach (array_reverse($messages) as $message)
                    <li>{{ $message }}</li>
                @endforeach
            </ul>
        @else
            <strong>{{ $messages }}</strong>
        @endif
    </div>
@endif

@if ($errors->any() || $errors->errores->any())
    <div class="alert alert-block {{ 'alert-danger' }} ">
        <button type="button" class="close text-center" data-dismiss="alert" aria-hidden="true">×</button>
        <strong>Verifique los datos ingresados:</strong>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<div id="errors" hidden></div>
