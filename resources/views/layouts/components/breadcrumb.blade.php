@php
    $arr = [
        'Inicio'    =>  url('account'),
        'permissions' => url('account/permissions'),
        'create'    =>  url()->current()
    ];
@endphp

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-10">
        <h2>Módulos</h2>
        <ol class="breadcrumb">
            @foreach($arr as $name => $url)
                @if($loop->last)
                    <li class="active"><strong>{{ $name }}</strong></li>
                @else
                    <li><a href="{{ $url }}">{{ $name }}</a></li>
                @endif
            @endforeach
        </ol>

    </div>
    <div class="col-lg-2">

    </div>
</div>