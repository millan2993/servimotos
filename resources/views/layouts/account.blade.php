<!DOCTYPE html>
<html>

<head lang="{{ app()->getLocale() }}">

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>{{ config('app.name') }}</title>

@section('external_files_header')
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<link href="{{ asset('css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('css/style.css') }}" rel="stylesheet">

@show

</head>

<body>

@inject('view', 'Servimotos\Http\Controllers\ViewsController')

<div id="wrapper">


    {{-- navegación - Menú --}}
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">


            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="{{ asset('img/profile_small.jpg') }}"/>
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear"> <span class="block m-t-xs">
                                    <strong class="font-bold">{{ ucfirst(Auth::user()->name) }}</strong>
                             </span> <span class="text-muted text-xs block">{{ ucfirst( Auth::user()->getRoleName() ) }} <b
                                            class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li><a href="{{ url('account/users/show', [Auth::user()->id]) }}">Mi perfil</a></li>
                            <li class="divider"></li>
                            <li><a href="">Configuración</a></li>
                            <li class="divider"></li>
                            <li><a href="{{ route('logout') }}"
                                   onclick="event.preventDefault(); $('#logout-form').submit();">Cerrar sessión</a>
                            </li>

                        </ul>
                    </div>
                    <div class="logo-element">
                        +
                    </div>
                </li>
                <li class="active">
                    <a href="{{ url('/account') }}"><i class="fa fa-diamond"></i> <span class="nav-label">Dashboard</span></a>
                </li>


                {{-- Crear el menú automático--}}
                @foreach($view->menu() as $modules)
                    @if(!$modules->components->isEmpty())
                        <li>
                            <a href="{{ $modules->link }}"><i class="fa fa-th-large"></i>
                                <span class="nav-label">{{ ucfirst($modules->name) }}</span>
                                <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                @foreach($modules->components as $component)
                                    <li><a href="{{ $component->link }}">{{ ucfirst($component->name) }}</a></li>
                                @endforeach
                            </ul>
                        </li>
                    @else
                        <li>
                            <a href="{{ $modules->link }}">
                                <i class="fa fa-diamond"></i>
                                <span class="nav-label">{{ ucfirst($modules->name) }}</span>
                            </a>
                        </li>
                    @endif
                @endforeach

            </ul>

        </div>
    </nav>




    <div id="page-wrapper" class="gray-bg">



        {{-- Header - Top --}}
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i>
                    </a>
                </div>

                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Bienvenido (a) {{ ucfirst(Auth::user()->name) }}</span>
                    </li>

                    @if(!auth()->user()->isClient())
                        <li class="dropdown">
                            <a class="dropdown-toggle count-info" data-toggle="dropdown" href="#"
                               data-toggle="tooltip" data-placement="bottom" title="Actividades en pendientes">
                                <i class="fa fa-bell"></i> <span class="label label-primary"> {{ $view->getAcountActivities() }} </span>
                            </a>
                        </li>
                    @endif

                    <li>
                        <a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
                </ul>

            </nav>
        </div>


        {{--@include('layouts.components.breadcrumb')--}}


        <div class="wrapper wrapper-content">

            @include('layouts.components.errors')

            @yield('content')


            <div class="footer">
                <div class="pull-right hidden-xs">
                    Licencia <strong>MIT</strong> Sistema de gestión.
                </div>
                <div>
                    <strong>Copyright</strong> Servimotos &copy; 2017
                </div>
            </div>
        </div>

    </div>


    {{-- Formulario para cerrar sessión --}}
    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>

</div>

@section('external_files_footer')
    <!-- Mainly scripts -->
    <script src="{{ asset('js/jquery-2.1.1.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ asset('js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

    <!-- Custom and plugin javascript - Inspinia  -->
    <script src="{{ asset('js/inspinia.js') }}"></script>

    {{-- JavaScript para el proyecto --}}
    <script src="{{ asset('js/servimotos.js') }}"></script>

    <link href="{{ asset('css/servimotos.css') }}" rel="stylesheet">
@show


</body>
</html>

