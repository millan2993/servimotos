<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name') }} | Error 500 </title>

    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>

<body class="gray-bg">

    <div class="middle-box text-center animated fadeInDown">
        <h1>500</h1>
        <h3 class="font-bold">Error interno del servidor</h3>

        <div class="error-desc">
            Ha ocurrido un error inesperado. <br>
            Disculpe las molestias ocasionadas.<br/><br>

            Ir a la página principal: <br/>
            <a href="{{ config('app.url') }}" class="btn btn-primary m-t">Dashboard</a>
        </div>
    </div>

</body>

</html>
