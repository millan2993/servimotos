<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}" >

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>{{ config('app.name') }}</title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">

</head>

<body class="gray-bg">

<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <h1 class="logo-name">Sm</h1>
        </div>

        <h3>Servimotos</h3>
        <p>Sistema de facturación y control de servicios a clientes para talleres de motos</p>

        <form class="m-t" role="form" method="post" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }} ">
                <input id="email" name="email" type="email" class="form-control" placeholder="Ingrese su correo" autofocus
                    value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>

            <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }} ">
                <input id="password" name="password" type="password" class="form-control" placeholder="Ingrese su contraseña" >

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>

            <button type="submit" class="btn btn-primary block full-width m-b">Iniciar sesión</button>

            <a href=" {{ route('password.request') }} "><small>Olvidaste su contraseña?</small></a>
        </form>

        <p class="m-t"> <small>Servimotos &copy; 2018</small> </p>
    </div>
</div>

<!-- Mainly scripts -->
<script src="js/jquery-2.1.1.js"></script>
<script src="js/bootstrap.min.js"></script>

</body>

</html>
