@extends('layouts.account')

@section('external_files_header')
  @parent
  <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@endsection


@section('content')


  <div class="row">
    <div class="col-lg-4">
      <div class="ibox">
        <div class="ibox-content">
          <h3>Pendientes</h3>
          <p class="small"><i class="fa fa-hand-o-up"></i> Arrastra actividades entre las listas</p>

          <ul class="sortable-list connectList agile-list" id="todo">

            @foreach($activity->get('todo') as $item )
              <li class="success-element" id="{{ $item->id }}">
                <p>
                  @if($item->id_client)
                    Cliente: <a href="{{ url('account/clients/show', $item->id_client) }}">{{ $item->client }} </a> <br>
                  @endif

                  @if($item->id_order)
                    Orden: <a href="{{ url('account/orders/show', $item->id_order) }}">{{ $item->order_code }} </a> <br>
                  @endif

                  Servicio: <a href="{{ url('account/services/show', $item->id_service) }}">{{ $item->service }}</a> <br>
                  Cantidad: {{ $item->amount }} <br>

                  @if($item->id_employee)
                    Asignado a: <a
                        href="{{ url('account/employees/show', $item->id_employee) }}">{{ $item->employee }} </a> <br>
                  @endif

                  {{ $item->observation }}
                </p>
                <div class="agile-detail">
                  <a href="{{ url('account/activities/show', $item->id) }}" class="pull-right btn btn-xs btn-white">Ver
                    más</a>
                  <i class="fa fa-clock-o"></i> {{ $item->end }}
                </div>
              </li>
            @endforeach

          </ul>
        </div>
      </div>
    </div>
    <div class="col-lg-4">
      <div class="ibox">
        <div class="ibox-content">
          <h3>En proceso</h3>
          <ul class="sortable-list connectList agile-list" id="inprogress">

            @foreach($activity->get('doing') as $item )
              <li class="success-element" id="{{ $item->id }}">
                <p>
                  @if($item->id_client)
                    Cliente: <a href="{{ url('account/clients/show', $item->id_client) }}">{{ $item->client }} </a> <br>
                  @endif

                  @if($item->id_order)
                    Orden: <a href="{{ url('account/orders/show', $item->id_order) }}">{{ $item->order_code }} </a> <br>
                  @endif

                  Servicio: <a href="{{ url('account/services/show', $item->id_service) }}">{{ $item->service }}</a> <br>
                  Cantidad: {{ $item->amount }} <br>

                  @if($item->id_employee)
                    Asignado a: <a
                        href="{{ url('account/employees/show', $item->id_employee) }}">{{ $item->employee }} </a> <br>
                  @endif

                  {{ $item->observation }}
                </p>
                <div class="agile-detail">
                  <a href="{{ url('account/activities/show', $item->id) }}" class="pull-right btn btn-xs btn-white">Ver
                    más</a>
                  <i class="fa fa-clock-o"></i> {{ $item->end }}
                </div>
              </li>
            @endforeach

          </ul>
        </div>
      </div>
    </div>


    <div class="col-lg-4">
      <div class="ibox">
        <div class="ibox-content">
          <h3>Terminado</h3>
          <ul class="sortable-list connectList agile-list" id="completed">
            @foreach($activity->get('deliver') as $item )
              <li class="success-element" id="{{ $item->id }}">
                <p>
                  @if($item->id_client)
                    Cliente: <a href="{{ url('account/clients/show', $item->id_client) }}">{{ $item->client }} </a> <br>
                  @endif

                  @if($item->id_order)
                    Orden: <a href="{{ url('account/orders/show', $item->id_order) }}">{{ $item->order_code }} </a> <br>
                  @endif

                  Servicio: <a href="{{ url('account/services/show', $item->id_service) }}">{{ $item->service }}</a> <br>
                  Cantidad: {{ $item->amount }} <br>

                  @if($item->id_employee)
                    Asignado a: <a
                        href="{{ url('account/employees/show', $item->id_employee) }}">{{ $item->employee }} </a> <br>
                  @endif

                  {{ $item->observation }}
                </p>
                <div class="agile-detail">
                  <a href="{{ url('account/activities/show', $item->id) }}" class="pull-right btn btn-xs btn-white">Ver
                    más</a>
                  <i class="fa fa-clock-o"></i> {{ $item->end }}
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>

  </div>


@section('external_files_footer')
  @parent
  <script src=" {{ asset('js/plugins/toastr/toastr.min.js') }}"></script>

  {{-- jquery ui - necesario para que funcione el board  --}}
  <script src="{{ asset('js/jquery-ui-1.10.4.min.js') }}"></script>

  <script src="{{ asset('js/jquery.ui.touch-punch.min.js') }}"></script>

  <script>
    $(document).ready(function () {
      $("#todo, #inprogress, #completed").sortable({
        connectWith: ".connectList",
        cursor: "move",
        receive: changeListOnBoard
      });
    });
  </script>
@endsection

@endsection
