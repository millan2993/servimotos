<?php

namespace Tests\Unit\rh;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Servimotos\Modules\rh\Models\Employee;
use Servimotos\modules\rh\Repositories\Employee\EmployeeRepository;
use Servimotos\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class servicesPermissionsTest extends TestCase
{
    use WithFaker, DatabaseTransactions;

    /**
     * @var Employee
     */
    private static $employee;


    /**
     * usuario asociado al empleado que se le está haciendo las pruebas
     *
     * @var User
     */
    private static $userAssociated;

    /**
     * Indispensable para los dataproviders
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }




    /**
     * Validar permisos para crear servicios.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testCreate(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $can = $superadmin->can('create', Employee::class);
        $this->assertTrue($can, 'El superadmin no puede crear empleados');

        $can = $admin->can('create', Employee::class);
        $this->assertTrue($can, 'El Administrador no puede crear empleados');

        $can = $withPermissions->can('create', Employee::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede crear empleados, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('create', Employee::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede crear empleados, a pesar que no tiene permisos.');
    }


    /**
     * Validar permisos para ver empleados.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShow(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $this->createEmployee($withoutPermissions->id_role);

        $can = $superadmin->can('view', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El superadmin no puede visualizar empleados');

        $can = $admin->can('view', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El Administrador no puede visualizar empleados');

        $can = $withPermissions->can('view', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar empleados, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('view', [Employee::class, self::$employee->id ]);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar empleados, a pesar que no tiene permisos.');


        /* validar que un usuario pueda ver sus datos de empleado aunque no tenga el permiso ver empleados */
        $can = self::$userAssociated->can('view', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El usuario '. self::$employee->name .' no puede visualizar sus datos de empleado.');
    }




    /**
     * Validar permisos para ver empleados.
     *
     * @dataProvider usersProvider
     * @depends testShow
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testUpdate(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('update', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El superadmin no puede editar los empleados');

        $can = $admin->can('update', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El Administrador no puede editar los empleados');

        $can = $withPermissions->can('update', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede editar los empleados, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('update', [Employee::class, self::$employee->id ]);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede editar los empleados, a pesar que no tiene permisos.');


        /* validar que un usuario pueda ver sus datos de empleado aunque no tenga el permiso ver empleados */
        $can = self::$userAssociated->can('update', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El usuario '. self::$employee->name .' no puede editar los sus datos de empleado.');
    }




    /**
     * Validar permisos para ver empleados.
     *
     * @dataProvider usersProvider
     * @depends testShow
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testDelete(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El superadmin no puede eliminar empleados');

        $can = $admin->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'Un Administrador no puede eliminar empleados');

        $can = $withPermissions->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede eliminar empleados, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede eliminar empleados, a pesar que no tiene permisos.');


        /**
         * validar que los usuarios no puedan eliminar sus datos de empleado.
         */

        /* validar que un usuario no pueda eliminar sus datos de empleado aunque tenga el permiso eliminar empleados */
        $can = self::$userAssociated->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertFalse($can, 'El usuario '. self::$employee->name .' no puede eliminar sus datos de empleado.');

        $this->associateUserToEmployee($admin);
        $can = $admin->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertFalse($can, 'Un Administrador no debería poder eliminar sus datos de empleado.');

        $this->associateUserToEmployee($withPermissions);
        $can = $withPermissions->can('delete', [Employee::class, self::$employee->id ]);
        $this->assertFalse($can, 'El usuario '. $withPermissions->name .' no debería poder eliminar sus datos de empleado.');

    }


    /**
     * provee los usuarios a realizar las pruebas
     *
     * @return array
     */
    public function usersProvider() {

        $superadmin = User::where('name', 'like', 'superadmin')->first();
        $admin = User::where('name', 'like', 'Administrador')->first();
        $withPermissions = User::where('name', 'like', 'con permisos')->first();
        $withoutPermissions = User::where('name', 'like', 'sin permisos')->first();

        return [
            'users' => [
                'superadmin' => $superadmin,
                'admin' => $admin,
                'without' => $withPermissions,
                'with' => $withoutPermissions,
            ]
        ];
    }



    /**
     * crear un empleado para realizar pruebas
     *
     * @todo Asociar con UserRepository
     * @param int $id_role  // role sin permisos
     *
     * @return void
     */
    private function createEmployee(int $id_role)
    {
        $employee = new Employee();
        $employee->first_name = $this->faker->firstName();
        $employee->last_name  = $this->faker->lastName();
        $employee->type_document = $this->faker->numberBetween(1, 3);
        $employee->document   = $this->faker->numerify('########');
        $employee->sex        = $this->faker->numberBetween(1, 3);

        // creamos y de paso probamos el repository
        (new EmployeeRepository($employee))->create();
        self::$employee = $employee;

        // crear usuario
        $associatedUser = new User();
        $associatedUser->name = $this->faker->name;
        $associatedUser->email = $this->faker->email;
        $associatedUser->password = bcrypt('123');
        $associatedUser->status = true;
        $associatedUser->id_role = $id_role;
        $associatedUser->type = $this->faker->numberBetween(0, 2);
        $associatedUser->id_employee = $employee->id;
        $associatedUser->save();

        self::$userAssociated = $associatedUser;
    }


    /**
     * Asociar al usuario el empleado de prueba
     *
     * @param User $user
     *
     * @return void
     */
    private function associateUserToEmployee(User $user)
    {
        $user->id_employee = self::$employee->id;
        $user->save();
    }

}
