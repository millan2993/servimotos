<?php

namespace Tests\Unit\reports;

use Illuminate\Support\Facades\DB;
use Servimotos\Modules\order\Models\Order;
use Servimotos\Modules\reports\Repositories\IndicatorsRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ordersTest extends TestCase
{
    /**
     * @var IndicatorsRepository
     */
    private $indicator;

    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();

        $this->indicator = new IndicatorsRepository();
    }


    public function testAvg_cumplimiento_cntrega()
    {
        $avg = $this->indicator->getAvgPromedioCumplimientoOS();

        $this->assertTrue(is_numeric($avg), 'No es un promedio.');
    }


    public function testVentasPorMes()
    {
        $sales = $this->indicator->getAmountSalesByMonth();

        $this->assertTrue(is_array($sales), 'El reporte de ventas por mes está fallando.');
    }

}
