<?php

namespace Tests\Unit\Orders;

use Servimotos\Modules\order\Models\Order;
use Servimotos\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class OrderPermissionsTest extends TestCase
{
    use WithFaker, DatabaseTransactions;


    /**
     * @var Order
     */
    private static $order;


    /**
     * permissionsForServicesTest constructor.
     * Indispensable para los dataproviders
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }




    /**
     * provee los usuarios a realizar las pruebas
     *
     * @return array
     */
    public function usersProvider() {

        $superadmin = User::where('name', 'like', 'superadmin')->first();
        $admin = User::where('name', 'like', 'Administrador')->first();
        $withPermissions = User::where('name', 'like', 'con permisos')->first();
        $withoutPermissions = User::where('name', 'like', 'sin permisos')->first();

        return [
            'users' => [
                'superadmin' => $superadmin,
                'admin' => $admin,
                'without' => $withPermissions,
                'with' => $withoutPermissions
            ]
        ];
    }


    /**
     * Validar permisos para crear Ordenes de compra.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testCreateActivities(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $can = $superadmin->can('create', Order::class);
        $this->assertTrue($can, 'El superadmin no puede crear ordenes de compra');

        $can = $admin->can('create', Order::class);
        $this->assertTrue($can, 'El Administrador no puede crear ordenes de compra');

        $can = $withPermissions->can('create', Order::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede crear ordenes de compra, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('create', Order::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede crear ordenes de compra, a pesar que no tiene permisos.');
    }




    /**
     * Validar permisos para ver Ordenes de compra.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShowOrder(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $this->createOrder();

        $can = $superadmin->can('view', self::$order);
        $this->assertTrue($can, 'El superadmin no puede visualizar Ordenes de compra');

        $can = $admin->can('view', self::$order);
        $this->assertTrue($can, 'El Administrador no puede visualizar Ordenes de compra');

        $can = $withPermissions->can('view', self::$order);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar Ordenes de compra, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('view', self::$order);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar Ordenes de compra, a pesar que no tiene permisos.');
    }



    /**
     * Validar permisos para editar ordenes de compra.
     *
     * @dataProvider usersProvider
     * @depends testShowOrder
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testUpdateOrders(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('update', self::$order);
        $this->assertTrue($can, 'El superadmin no puede editar ordenes de compra');

        $can = $admin->can('update', self::$order);
        $this->assertTrue($can, 'El Administrador no puede editar ordenes de compra');

        $can = $withPermissions->can('update', self::$order);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede editar ordenes de compra, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('update', self::$order);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede editar ordenes de compra, a pesar que no tiene permisos.');
    }



    /**
     * Validar permisos para eliminar Ordenes de compra
     *
     * @dataProvider usersProvider
     * @depends testShowOrder
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testDeleteOrders(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('delete', self::$order);
        $this->assertTrue($can, 'El superadmin no puede eliminar Ordenes de compra');

        $can = $admin->can('delete', self::$order);
        $this->assertTrue($can, 'El Administrador no puede eliminar Ordenes de compra');

        $can = $withPermissions->can('delete', self::$order);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede eliminar Ordenes de compra, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('delete', self::$order);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede eliminar Ordenes de compra, a pesar que no tiene permisos.');

    }




    private function createOrder()
    {
        $order = new Order();
        $order->id_status = 1;
        $order->code = uniqid();
        $order->save();

        self::$order = $order;
    }

}
