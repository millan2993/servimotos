<?php

namespace Tests\Unit\Activities;

use Tests\TestCase;
use Servimotos\User;
use Servimotos\Modules\activities\Models\Activity;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithFaker;

class activityPermissionsTest extends TestCase
{
    use WithFaker, DatabaseTransactions;

    private static $activity;


    /**
     * permissionsForServicesTest constructor.
     * Indispensable para los dataproviders
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }



    /**
     * Validar permisos para crear actividades.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testCreateActivities(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $can = $superadmin->can('create', Activity::class);
        $this->assertTrue($can, 'El superadmin no puede crear Actividades');

        $can = $admin->can('create', Activity::class);
        $this->assertTrue($can, 'El Administrador no puede crear Actividades');

        $can = $withPermissions->can('create', Activity::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede crear Actividades, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('create', Activity::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede crear Actividades, a pesar que no tiene permisos.');
    }




    /**
     * Validar permisos para ver Actividades.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShowActivities(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $this->createActivity();

        $can = $superadmin->can('view', self::$activity);
        $this->assertTrue($can, 'El superadmin no puede visualizar Actividades');

        $can = $admin->can('view', self::$activity);
        $this->assertTrue($can, 'El Administrador no puede visualizar Actividades');

        $can = $withPermissions->can('view', self::$activity);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar Actividades, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('view', self::$activity);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar Actividades, a pesar que no tiene permisos.');
    }




    /**
     * Validar permisos para editar actividades.
     *
     * @dataProvider usersProvider
     * @depends testShowActivities
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testUpdateActivities(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('update', self::$activity);
        $this->assertTrue($can, 'El superadmin no puede editar actividades');

        $can = $admin->can('update', self::$activity);
        $this->assertTrue($can, 'El Administrador no puede editar actividades');

        $can = $withPermissions->can('update', self::$activity);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede editar actividades, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('update', self::$activity);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede editar actividades, a pesar que no tiene permisos.');

    }



    /**
     * Validar permisos para ver Actividades
     *
     * @dataProvider usersProvider
     * @depends testShowActivities
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testDeleteActivities(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('delete', self::$activity);
        $this->assertTrue($can, 'El superadmin no puede eliminar Actividades');

        $can = $admin->can('delete', self::$activity);
        $this->assertTrue($can, 'El Administrador no puede eliminar Actividades');

        $can = $withPermissions->can('delete', self::$activity);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede eliminar Actividades, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('delete', self::$activity);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede eliminar Actividades, a pesar que no tiene permisos.');

    }



    /**
     * provee los usuarios a realizar las pruebas
     *
     * @return array
     */
    public function usersProvider() {

        $superadmin = User::where('name', 'like', 'superadmin')->first();
        $admin = User::where('name', 'like', 'Administrador')->first();
        $withPermissions = User::where('name', 'like', 'con permisos')->first();
        $withoutPermissions = User::where('name', 'like', 'sin permisos')->first();

        return [
            'users' => [
                'superadmin' => $superadmin,
                'admin' => $admin,
                'without' => $withPermissions,
                'with' => $withoutPermissions
            ]
        ];
    }



    private function createActivity()
    {
        $acvitivy = new Activity();
        $acvitivy->code = uniqid();
        $acvitivy->start = now();
        $acvitivy->end = now()->addHour();
        $acvitivy->id_status = 1;
        $acvitivy->save();

        static::$activity = $acvitivy;
    }


}
