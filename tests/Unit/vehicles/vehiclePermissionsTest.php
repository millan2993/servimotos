<?php

namespace Tests\Unit\vehicles;

use Tests\TestCase;
use Servimotos\User;
use Servimotos\Modules\vehicles\models\vehicles;

use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class vehiclePermissionsTest extends TestCase
{
    use WithFaker, DatabaseTransactions;

    /**
     * @var Vehicles
     */
    private static $vehicle;


    /**
     * permissionsForServicesTest constructor.
     * Indispensable para los dataproviders
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }



    /**
     * Validar permisos para crear servicios.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testCreateVehicles(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $can = $superadmin->can('create', Vehicles::class);
        $this->assertTrue($can, 'El superadmin no puede crear Vehículos');

        $can = $admin->can('create', Vehicles::class);
        $this->assertTrue($can, 'El Administrador no puede crear Vehículos');

        $can = $withPermissions->can('create', Vehicles::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede crear Vehículos, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('create', Vehicles::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede crear Vehículos, a pesar que no tiene permisos.');
    }




    /**
     * Validar permisos para ver servicios.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShowVehicles(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $this->createVehicle();

        $can = $superadmin->can('view', self::$vehicle);
        $this->assertTrue($can, 'El superadmin no puede visualizar vehiculos');

        $can = $admin->can('view', self::$vehicle);
        $this->assertTrue($can, 'El Administrador no puede visualizar vehiculos');

        $can = $withPermissions->can('view', self::$vehicle);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar vehiculos, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('view', self::$vehicle);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar vehiculos, a pesar que no tiene permisos.');
    }



    /**
     * Validar permisos para editar vehiculos.
     *
     * @dataProvider usersProvider
     * @depends testShowVehicles
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testUpdateVehicles(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('update', self::$vehicle);
        $this->assertTrue($can, 'El superadmin no puede editar vehiculos');

        $can = $admin->can('update', self::$vehicle);
        $this->assertTrue($can, 'El Administrador no puede editar vehiculos');

        $can = $withPermissions->can('update', self::$vehicle);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede editar vehiculos, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('update', self::$vehicle);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede editar vehiculos, a pesar que no tiene permisos.');

    }



    /**
     * Validar permisos para ver servicios.
     *
     * @dataProvider usersProvider
     * @depends testShowVehicles
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testDeleteService(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('delete', self::$vehicle);
        $this->assertTrue($can, 'El superadmin no puede eliminar vehiculos');

        $can = $admin->can('delete', self::$vehicle);
        $this->assertTrue($can, 'El Administrador no puede eliminar vehiculos');

        $can = $withPermissions->can('delete', self::$vehicle);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede eliminar vehiculos, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('delete', self::$vehicle);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede eliminar vehiculos, a pesar que no tiene permisos.');

    }





    /**
     * provee los usuarios a realizar las pruebas
     *
     * @return array
     */
    public function usersProvider() {

        $superadmin = User::where('name', 'like', 'superadmin')->first();
        $admin = User::where('name', 'like', 'Administrador')->first();
        $withPermissions = User::where('name', 'like', 'con permisos')->first();
        $withoutPermissions = User::where('name', 'like', 'sin permisos')->first();

        return [
            'users' => [
                'superadmin' => $superadmin,
                'admin' => $admin,
                'without' => $withPermissions,
                'with' => $withoutPermissions
            ]
        ];
    }





    /**
     * Crear un vehículo para pruebas
     */
    private function createVehicle()
    {
        $vehicle = new Vehicles();
        $vehicle->placa = 'LID28D';
        $vehicle->marca = 'Honda';
        $vehicle->modelo= 2014;
        $vehicle->color = 'red';
        $vehicle->id_city = 1;
        $vehicle->save();

        self::$vehicle = $vehicle;
    }


}
