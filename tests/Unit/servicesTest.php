<?php

namespace Tests\Unit;

use Faker\Factory;
use Servimotos\Modules\services\Models\Service;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class services extends TestCase
{

    public $service;

    /**
     * Crear un servicio
     */
    public function testInsert()
    {
        $factory = (new Factory())->create();

        $model = new Service();
        $model->name = $factory->text(20);
        $model->desc = $factory->text(200);
        $model->type = $factory->numberBetween(1, 10);
        $model->status = $factory->boolean;
        $model->time_medium = $factory->time('H:i');
        $model->created_at = $factory->dateTime;
        $model->updated_at = $factory->dateTime;

        $this->service = $model->insert();

        $this->assertDatabaseHas('services', [
            'id' => $this->service
        ]);

    }


    /**
     * Verificar obtener todos los servicios.
     */
    public function testIndex()
    {

        $services = (new Service())->index();
        $this->assertInstanceOf('Illuminate\Support\Collection', $services, 'Ocurrio un error al consultar los servicios');
    }



    /**
     * Buscar servicios según condicional
     */
    public function testFindBy()
    {
        $fields = [ 'id', 'name' ];
        $wheres = [ 'status' =>  1 ];
        $first = true;

        $instance = ($first) ? 'stdClass' : 'Illuminate\Support\Collection';

        $services = Service::findBy($fields, $wheres, $first);
        $this->assertInstanceOf($instance, $services, 'Ocurrio un error al consultar servicios filtrados.');
    }



    /**
     * Obtener un servicio
     */
    public function testShow()
    {
        $model = new Service();
        $model->id = 1;
        $service = $model->show();

        $this->assertInstanceOf('stdClass', new \stdClass(), 'Falló al consultar un servicio.');
    }


    /**
     * Editar un servicio
     */
    public function testEdit()
    {
        $factory = (new Factory())->create();

        $service = new Service();
        $service->id = 1;
        $service->name = $factory->text(20);
        $service->type = $factory->numberBetween(1, 10);

        $service->edit();


        $this->assertDatabaseHas('services', [
            'id' => $service->id,
            'name' => $service->name
        ]);
    }


    /**
     * Eliminar un servicio
     */
    public function testDelete()
    {
        $service = new Service();
        $service->id = 7;

        $service->remove();


        $this->assertDatabaseMissing('services', [
            'id' => $service->id
        ]);
    }

}
