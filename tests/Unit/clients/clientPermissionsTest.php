<?php

namespace Tests\Unit\clients;

use Servimotos\Modules\clients\Repositories\ClientRepository;
use Servimotos\User;
use Tests\TestCase;
use Servimotos\Modules\clients\models\Clients;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class clientPermissionsTest extends TestCase
{
    use WithFaker;

    /**
     * @var Clients
     */
    private static $client;

    /**
     * usuario asociado al cliente que se realizan las pruebas
     *
     * @var User
     */
    private static $userAssociated;

    /**
     * Constructs a test case with the given name.
     *
     * @param string $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }




    /**
     * Validar permisos para crear servicios.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testCreate(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $can = $superadmin->can('create', Clients::class);
        $this->assertTrue($can, 'El superadmin no puede crear clientes');

        $can = $admin->can('create', Clients::class);
        $this->assertTrue($can, 'El Administrador no puede crear clientes');

        $can = $withPermissions->can('create', Clients::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede crear clientes, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('create', Clients::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede crear clientes, a pesar que no tiene permisos.');
    }





    /**
     * Validar permisos para ver clientes.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShow(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $this->createClient($withoutPermissions->id_role);

        $can = $superadmin->can('view', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El superadmin no puede visualizar clientes');

        $can = $admin->can('view', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El Administrador no puede visualizar clientes');

        $can = $withPermissions->can('view', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar clientes, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('view', [Clients::class, self::$client->id ]);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar clientes, a pesar que no tiene permisos.');

        /* validar que un usuario pueda ver sus datos de cliente aunque no tenga el permiso ver clientes */
        $can = self::$userAssociated->can('view', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El usuario '. self::$client->name .' no puede visualizar sus datos de cliente.');
    }


    /**
     * @dataProvider usersProvider
     * @depends testShow
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShowAll(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('viewAll', Clients::class);
        $this->assertTrue($can, 'El superadmin no puede visualizar clientes');

        $can = $admin->can('viewAll', Clients::class);
        $this->assertTrue($can, 'El Administrador no puede visualizar clientes');

        $can = $withPermissions->can('viewAll', Clients::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar clientes, a pesar que tiene permisos (viewAll).');

        $can = $withoutPermissions->can('viewAll', Clients::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar clientes, a pesar que no tiene permisos.');
    }





    /**
     * Validar permisos para ver clientes.
     *
     * @dataProvider usersProvider
     * @depends testShow
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testUpdate(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('update', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El superadmin no puede editar los clientes');

        $can = $admin->can('update', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El Administrador no puede editar los clientes');

        $can = $withPermissions->can('update', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede editar los clientes, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('update', [Clients::class, self::$client->id ]);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede editar los clientes, a pesar que no tiene permisos.');


        /* validar que un usuario pueda ver sus datos de cliente aunque no tenga el permiso ver clientes */
        $can = self::$userAssociated->can('update', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El usuario '. self::$client->name .' no puede editar los sus datos de clientes.');
    }




    /**
     * Validar permisos para ver clientes.
     *
     * @dataProvider usersProvider
     * @depends testShow
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testDelete(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('delete', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El superadmin no puede eliminar clientes');

        $can = $admin->can('delete', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'Un Administrador no puede eliminar clientes');

        $can = $withPermissions->can('delete', [Clients::class, self::$client->id ]);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede eliminar clientes, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('delete', [Clients::class, self::$client->id ]);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede eliminar clientes, a pesar que no tiene permisos.');


        /**
         * validar que los usuarios no puedan eliminar sus datos de cliente.
         */

        /* validar que un usuario no pueda eliminar sus datos de cliente aunque tenga el permiso eliminar clientes */
        $can = self::$userAssociated->can('delete', [Clients::class, self::$client->id ]);
        $this->assertFalse($can, 'El usuario '. self::$client->name .' no puede eliminar sus datos de cliente.');

        $this->associateUserToEmployee($admin);
        $can = $admin->can('delete', [Clients::class, self::$client->id ]);
        $this->assertFalse($can, 'Un Administrador no debería poder eliminar sus datos de cliente.');

        $this->associateUserToEmployee($withPermissions);
        $can = $withPermissions->can('delete', [Clients::class, self::$client->id ]);
        $this->assertFalse($can, 'El usuario '. $withPermissions->name .' no debería poder eliminar sus datos de cliente.');

    }





    /**
     * provee los usuarios a realizar las pruebas
     *
     * @return array
     */
    public function usersProvider() {

        $superadmin = User::where('name', 'like', 'superadmin')->first();
        $admin = User::where('name', 'like', 'Administrador')->first();
        $withPermissions = User::where('name', 'like', 'con permisos')->first();
        $withoutPermissions = User::where('name', 'like', 'sin permisos')->first();

        return [
            'users' => [
                'superadmin' => $superadmin,
                'admin' => $admin,
                'without' => $withPermissions,
                'with' => $withoutPermissions,
            ]
        ];
    }



    /**
     * crear un cliente para realizar pruebas
     *
     * @todo Asociar con UserRepository
     * @param int $id_role  // role sin permisos
     *
     * @return void
     */
    private function createClient(int $id_role)
    {
        $client = new Clients();
        $client->first_name = $this->faker->firstName();
        $client->last_name  = $this->faker->lastName();
        $client->type_document = $this->faker->numberBetween(1, 3);
        $client->document   = $this->faker->numerify('########');
        $client->sex        = $this->faker->numberBetween(1, 3);

        // creamos y de paso probamos el repository
        (new ClientRepository($client))->create();
        self::$client = $client;

        // crear usuario
        $associatedUser = new User();
        $associatedUser->name = $this->faker->name;
        $associatedUser->email = $this->faker->email;
        $associatedUser->password = bcrypt('123');
        $associatedUser->status = true;
        $associatedUser->id_role = $id_role;
        $associatedUser->type = $this->faker->numberBetween(0, 2);
        $associatedUser->id_client = $client->id;
        $associatedUser->save();

        self::$userAssociated = $associatedUser;
    }



    /**
     * Asociar al usuario el empleado de prueba
     *
     * @param User $user
     *
     * @return void
     */
    private function associateUserToEmployee(User $user)
    {
        $user->id_client = self::$client->id;
        $user->save();
    }


}
