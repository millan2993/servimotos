<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Servimotos\Modules\acl\Models\Role;
use Servimotos\Modules\acl\Repositories\Roles\RoleRepository;
use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class rolesTest extends TestCase
{

    private $role1 = null;
    private $role2 = null;

    public function testIndex()
    {
        try {
            $roles = DB::table('roles')
                ->select('id', 'name', 'desc', 'status')->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los roles.', [$e->getMessage()]);
            $roles = array();
        }

        $this->assertInstanceOf('Illuminate\Support\Collection', $roles, 'Ocurrio un error al consultar los roles');


        // Model
        $roles = Role::index();
        $this->assertInstanceOf('Illuminate\Support\Collection', $roles, 'Ocurrio un error al consultar los roles desde el modelo.');

    }



    public function testFindBy()
    {
        $fields = [ 'id', 'name' ];
        $wheres = [ 'status' =>  1 ];
        $first = true;

        $instance = ($first) ? 'stdClass' : 'Illuminate\Support\Collection';

        try {
            $roles = DB::table('roles')
                ->select($fields)
                ->where($wheres);

            $roles = ($first) ? $roles->first() : $roles->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los roles.', [$e->getMessage()]);
            $roles = array();
        }

        $this->assertInstanceOf($instance, $roles, 'Ocurrio un error al consultar roles filtrados');


        // Model
        $roles = Role::findBy($fields, $wheres, $first);
        $this->assertInstanceOf($instance, $roles, 'Ocurrio un error al consultar roles filtrados desde el módulo.');


    }



    public function testShow()
    {
        $id = 2;

        try {
            $role = DB::table('roles')
                ->select('id', 'name', 'desc', 'status', 'created_at', 'updated_at')
                ->where('id', '=', $id)
                ->first();

        } catch (QueryException $e) {
            logger()->error('Error al seleccionar un rol.', [$e->getMessage()]);
            $role = null;
        }

        $this->assertInstanceOf('stdClass', $role, 'Error al consultar un rol.');


        // Model
        $role = Role::show($id);
        $this->assertInstanceOf('stdClass', $role, 'Error al consultar un rol desde el modelo.');

    }



    public function testInsert()
    {
        $role = [
            'name'  =>  'aaa',
            'desc'  =>  'Prueba de creación de módulo.',
            'status'=>  false
        ];

        $role['created_at'] = Carbon::now();
        $role['updated_at'] = Carbon::now();

        DB::beginTransaction();
        try {
            $id =  DB::table('roles')->insertGetId($role);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al Crear un módulo.', [$e->getMessage()]);
            $id = false;
        }

        $this->role1 = $id;

        $this->assertTrue(is_int($id), 'Ocurrió un error al crear un usuario.');


        // Desde el model
        $id = Role::insert($role);
        $this->role2 = $id;

        $this->assertTrue(is_int($id), 'Ocurrió un error al crear un usuario desde el modelo.');


    }



    public function testUpdate()
    {
        $id = $this->role1;

        $role = [
            'name' => 'actualizado',
            'desc'  => 'El role ha sido modificado.',
            'status' => false
        ];

        $role['updated_at'] = Carbon::now();

        DB::beginTransaction();
        try {
            $updated = DB::table('roles')
                ->where('id', '=', $id)
                ->update($role);

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar el rol: ' .$id, [$e->getMessage()]);
            $updated = null;
        }

        $this->assertTrue(is_int($updated), 'Ocurrió un error al actualizar un role.');

        $updated = Role::edit($id, $role);
        $this->assertTrue(is_int($updated), 'Ocurrió un error al actualizar un role desde el modelo.');

    }



    public function testDelete()
    {
        $id = $this->role1;

        DB::beginTransaction();
        try {
            $deleted = DB::table('roles')
                ->where('id', $id)
                ->delete();

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un rol: ' .$id, [$e->getMessage()]);
            $deleted = false;
        }

       $this->assertTrue(is_int($deleted), 'Ocurrió un error al eliminar el usuario: ' .$id );

        $id = $this->role2;
        $deleted = Role::remove($id);
        $this->assertTrue(is_int($deleted), 'Ocurrió un error al eliminar el usuario ' .$id. ' desde el modelo.');

    }



    public function testHasPermission($role = 2, $permission = 'ver_permisos')
    {
        try {
            $wheres = array(
                ['roles_permissions.id_role', '=', $role],
                ['permissions.status', '=', 1 ],
                ['permissions.name', 'LIKE', $permission ]
            );

            $hasPermission = DB::table('permissions')
                ->leftjoin('roles_permissions', 'permissions.id', '=', 'roles_permissions.id_permission')
                ->where($wheres)
                ->count();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los roles.', [$e->getMessage()]);
            $hasPermission = null;
        }

        $this->assertTrue(is_int($hasPermission), 'Error al consultar un permiso para un rol.');

        
        $hasPermission = Role::hasPermission($role, $permission);
        $this->assertTrue(is_int($hasPermission), 'Error al consultar un permiso para un rol desde el modelo.');

    }


    public function testSavePermissions()
    {
        $actuales = array(8, 9, 10, 11, 12);
        $nuevos = array(10, 11, 12, 13, 14);

        $delete = array_diff($actuales, $nuevos);


        $delete = DB::table('roles_permissions')
            ->where('id_role', '=', '28')
            ->whereIn('id_permission', $delete)
            ->delete();

        dd($delete);


    }
}
