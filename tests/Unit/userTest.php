<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\Constraints\HasInDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Servimotos\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class UserTest extends TestCase
{

    /**
     * @desc    Test to Create user
     *
     * @see     23/11/2017
     * @author  Alex Millán
     */
    public function testCreate()
    {
        $faker = \Faker\Factory::create();

        $user = [
            'name'  => $faker->name,
            'email' => $faker->email,
            'avatar'=> 'avatar.jpg',
            'sex'   => $faker->boolean,
            'status'=> $faker->boolean,
            'password' => bcrypt($faker->password),
            'id_client' => 1,
            'created_at' => $faker->date('Y-m-d h:m:s'),
            'updated_at' => $faker->date('Y-m-d h:m:s')
        ];

        DB::beginTransaction();
        try {
            $id = DB::table('users')->insertGetId($user);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al Crear un usuario.', [$e->getMessage()]);
            $id = false;
        }

        $this->assertTrue(is_int($id), 'Ocurrió un error al crear un usuario.');

    }



    /**
     * @desc    Test consultar un usuario
     *
     * @param   int $id
     *
     * @see     23/11/2017
     * @author  Alex Millán
     */
    public function testShow($id = 1)
    {
        try {
            $user = DB::table('users')
                ->select('users.id', 'users.name', 'users.email', 'users.avatar',
                    'users.status', 'users.created_at', 'users.updated_at', 'roles.name as role'
                )
                ->leftjoin('roles', 'roles.id', '=', 'users.id_role')
                ->where('users.id', '=', $id)
                ->first();

        } catch (QueryException $e) {
            logger()->error('Error al consultar un usuario.', [$e->getMessage()]);
        }

        $this->assertTrue(!is_array($user), 'Error al consultar el usuario.') ;


        // Consulta desde el modelo
        $user = User::show($id);
        $this->assertTrue(!is_array($user), 'Error al consultar el usuario desde el modelo.');

    }



    /**
     * @desc    Actualizar un usuarios
     *
     * @param   int $id
     *
     * @see     24/11/2017
     * @author  Alex Millán
     */
    public function testUpdate($id = 7000)
    {
        $faker = \Faker\Factory::create();

        $user = [
            'name'  => $faker->name,
            'email' => $faker->email,
            'avatar'=> 'avatar.jpg',
            'sex'   => $faker->boolean,
            'status'=> $faker->boolean,
            'password' => bcrypt($faker->password),
            'id_client' => 1,
            'remember_token' => $faker->sha256,
            'created_at' => $faker->date('Y-m-d h:m:s'),
            'updated_at' => $faker->date('Y-m-d h:m:s')
        ];

        DB::beginTransaction();
        try {
            $updated = DB::table('users')
                ->where('id', '=', $id)
                ->update($user);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar el usuario: ' .$id, [$e->getMessage()]);
            $updated = null;
        }

        $this->assertTrue(is_int($updated), 'Ocurrió un error al actualizar un usuario.');
    }



    public function testDelete($id = 8)
    {
        DB::beginTransaction();
        try {
            $deleted = DB::table('users')
                ->where('id', $id)
                ->delete();

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un rol: ' .$id, [$e->getMessage()]);
            $deleted = null;
        }

        $this->assertTrue(is_int($deleted), 'Ha ocurrido un error al eliminar un usuario.');

    }



    /**
     * @desc    Test to read all users
     *
     * @see     23/11/2017
     * @author  Alex Millán
     */
    public function testIndex()
    {
        try {
            $users = DB::table('users')
                ->select('users.id', 'users.name', 'users.email', 'users.avatar', 'users.status', 'roles.name AS role' )
                ->leftjoin('roles', 'roles.id', '=', 'users.id_role')
                ->orderBy('users.name')
                ->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los roles.', [$e->getMessage()]);
        }

        $this->assertInstanceOf('Illuminate\Support\Collection', $users, 'Error al consultar usuarios.');

        
        $users = User::all();
        $this->assertInstanceOf('Illuminate\Support\Collection', $users, 'Error al consultar los usuarios desde el modelo');

    }



    /**
     * @desc    Buscar usuarios condicionales
     *
     * @param null  $fields
     * @param array $wheres
     *
     * @see     26/11/2017
     * @author  Alex Millán
     */
    public function testFindBy()
    {
        $fields = [ 'id', 'name' ];
        $wheres = [
            ['email', '=', 'millan2993@gmail.com' ]
        ];
        $first = true;

        $instance = ($first) ? 'stdClass' : 'Illuminate\Support\Collection';

        try {
            $users = DB::table('users')
                ->select($fields)
                ->where($wheres);
            $users = ($first) ? $users->first() : $users->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar usuarios.' [$e->getMessage()]);
            $users = null;
        }

        $this->assertInstanceOf($instance, $users, 'Ha ocurrido un error al buscar usuarios filtrados.');


        // Desde el modelo
        $users = User::findBy($fields, $wheres, $first);
        $this->assertInstanceOf($instance, $users, 'Ha ocurrido un error al buscar usuarios filtrados.');

    }


    
    /**
     * Verificar si un usuario tiene el rol especificado.
     *
     * @param string|array $role
     */
    public function testHasRole($role = array('superadmin', 'Administrado'))
    {
        $id_user = 1;
        $roles = array();

        if (is_string($role)) {
            $roles[] = $role;
        } else {
            $roles = $role;
        }


        foreach ($roles as $role) {
            try {
                $hasRole = DB::table('roles')
                    ->leftjoin('users', 'users.id_role', '=', 'roles.id');

                if (is_int($role)) {
                    $hasRole = $hasRole->where('roles.id', '=', $role);
                } else {
                    $hasRole = $hasRole->where('roles.name', 'like', $role);
                }

                $hasRole = $hasRole->where('users.id', '=', $id_user)->count();
            } catch (QueryException $e) {
                logger()->error('Error al consultar el rol del usuario', [$e->getMessage()]);
                $hasRole = 'No se pudo consultar el rol del usuario.';
            }

            if ($hasRole > 0) {
                $hasRole = true;
                break;
            }
        }

        $this->assertTrue(is_int($hasRole), 'Error al verificar un rol del usuario.');


        $role = User::hasRole(array(3, 1, 'administrador'), $id_user);

        $this->assertTrue($role, 'Error al verificar el rol del usuario desde el modelo.');

    }


}
