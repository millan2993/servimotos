<?php

namespace Tests\Unit\acl;

use Tests\TestCase;

use Servimotos\User;
use Servimotos\Modules\services\Models\Service;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;

class permissionsForServicesTest extends TestCase
{
    use WithFaker, DatabaseTransactions;


    /**
     * @var Service
     */
    private static $service;


    /**
     * permissionsForServicesTest constructor.
     * Indispensable para los dataproviders
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }




    /**
     * Validar permisos para crear servicios.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testCreateService(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        $can = $superadmin->can('create', Service::class);
        $this->assertTrue($can, 'El superadmin no puede visualizar los servicios');

        $can = $admin->can('create', Service::class);
        $this->assertTrue($can, 'El Administrador no puede visualizar los servicios');

        $can = $withPermissions->can('create', Service::class);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar los servicios, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('create', Service::class);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar los servicios, a pesar que no tiene permisos.');
    }





    /**
     * Validar permisos para ver servicios.
     *
     * @dataProvider usersProvider
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testShowService(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions )
    {
        self::$service = $this->createService();

        $can = $superadmin->can('view', self::$service);
        $this->assertTrue($can, 'El superadmin no puede visualizar los servicios');

        $can = $admin->can('view', self::$service);
        $this->assertTrue($can, 'El Administrador no puede visualizar los servicios');

        $can = $withPermissions->can('view', self::$service);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede visualizar los servicios, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('view', self::$service);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede visualizar los servicios, a pesar que no tiene permisos.');
    }




    /**
     * Validar permisos para ver servicios.
     *
     * @dataProvider usersProvider
     * @depends testShowService
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testUpdateService(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('update', self::$service);
        $this->assertTrue($can, 'El superadmin no puede editar los servicios');

        $can = $admin->can('update', self::$service);
        $this->assertTrue($can, 'El Administrador no puede editar los servicios');

        $can = $withPermissions->can('update', self::$service);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede editar los servicios, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('update', self::$service);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede editar los servicios, a pesar que no tiene permisos.');

    }




    /**
     * Validar permisos para ver servicios.
     *
     * @dataProvider usersProvider
     * @depends testShowService
     *
     * @param User $superadmin
     * @param User $admin
     * @param User $withPermissions
     * @param User $withoutPermissions
     */
    public function testDeleteService(User $superadmin, User $admin, User $withPermissions, User $withoutPermissions)
    {
        $can = $superadmin->can('delete', self::$service);
        $this->assertTrue($can, 'El superadmin no puede eliminar los servicios');

        $can = $admin->can('delete', self::$service);
        $this->assertTrue($can, 'El Administrador no puede eliminar los servicios');

        $can = $withPermissions->can('delete', self::$service);
        $this->assertTrue($can, 'El usuario '. $withPermissions->name .' no puede eliminar los servicios, a pesar que tiene permisos.');

        $can = $withoutPermissions->can('delete', self::$service);
        $this->assertFalse($can, 'El usuario '. $withoutPermissions ->name .' puede eliminar los servicios, a pesar que no tiene permisos.');

    }




    /**
     * provee los usuarios a realizar las pruebas
     *
     * @return array
     */
    public function usersProvider() {

        $superadmin = User::where('name', 'like', 'superadmin')->first();
        $admin = User::where('name', 'like', 'Administrador')->first();
        $withPermissions = User::where('name', 'like', 'con permisos')->first();
        $withoutPermissions = User::where('name', 'like', 'sin permisos')->first();

        return [
            'users' => [
                'superadmin' => $superadmin,
                'admin' => $admin,
                'without' => $withPermissions,
                'with' => $withoutPermissions
            ]
        ];
    }



    /**
     * Crear un servicio para hacer validaciones con éste
     *
     * @return Service
     */
    public function createService()
    {
        $service = new Service();
        $service->name = $this->faker->jobTitle();
        $service->desc = $this->faker->text(200);
        $service->status = true;
        $service->time_medium = $this->faker->time();
        $service->price = $this->faker->numberBetween(2000, 200000);
        $service->save();

        return $service;
    }

}
