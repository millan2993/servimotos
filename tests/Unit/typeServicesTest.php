<?php

namespace Tests\Unit;

use Faker\Factory;
use Servimotos\Modules\services\Models\TypeService;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class typeServicesTest extends TestCase
{
    /**
     * Crear un tipo de servicio
     */
    public function testInsert()
    {
        $factory = (new Factory())->create();

        $model = new TypeService();
        $model->name = $factory->text(20);
        $model->desc = $factory->text(200);
        $model->created_at = $factory->dateTime;
        $model->updated_at = $factory->dateTime;

        $idTypeService = $model->insert();

        $this->assertDatabaseHas('typeservices', [
            'id' => $idTypeService
        ]);

        return $idTypeService;
    }


    /**
     * Obtener todos los tipos de servicios
     */
    public function testIndex()
    {
        $typeServices = (new TypeService())->index();
        $this->assertInstanceOf('Illuminate\Support\Collection', $typeServices, 'Ocurrio un error al consultar los tipos de servicios');
    }




    /**
     * Buscar servicios según condicional
     * @depends testInsert
     */
    public function testFindBy(int $idTypeService)
    {
        $fields = [ 'id', 'name' ];
        $wheres = [ 'id' => $idTypeService ];

        $service = TypeService::findBy($fields, $wheres, true);
        $this->assertInstanceOf('stdClass', $service, 'Ocurrio un error al consultar un tipo de servicio filtrado.');


        $services = TypeService::findBy($fields, []);
        $this->assertInstanceOf('Illuminate\Support\Collection', $services, 'Ocurrio un error al consultar tipos de servicios filtrados.');
        
    }



    /**
     * Obtener un servicio
     * @depends testInsert
     */
    public function testShow(int $idTypeService)
    {
        $model = new TypeService();
        $model->id = $idTypeService;
        $typeService = $model->show();

        $this->assertInstanceOf('stdClass', $typeService, 'Falló al consultar un tipo de servicio.');
    }



    /**
     * Editar un servicio
     * @depends testInsert
     */
    public function testEdit(int $idTypeService)
    {
        $factory = (new Factory())->create();

        $typeService = new TypeService();
        $typeService->id = $idTypeService;
        $typeService->name = $factory->text(20);

        $typeService->edit();

        $this->assertDatabaseHas('typeservices', [
            'id' => $idTypeService,
            'name' => $typeService->name
        ]);
    }



    /**
     * Eliminar un servicio
     * @depends testInsert
     */
    public function testDelete(int $idTypeService)
    {
        $typeService = new TypeService();
        $typeService->id = $idTypeService;

        $typeService->remove();

        $this->assertDatabaseMissing('typeservices', [
            'id' => $idTypeService
        ]);
    }


}
