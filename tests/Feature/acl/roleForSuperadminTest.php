<?php

namespace Tests\Feature\acl;

use Auth;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class roleForSuperadminTest extends TestCase
{

    /**
     * url base para roles
     */
    const baseUrl = '/account/roles';


    /**
     * Requerido para poder hacer uso de los dataPrivider
     *
     * usersForSuperadminTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }



    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();

        $user = DB::table('users')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        Auth::loginUsingId($user);
    }


    /**
     * retorna los roles para verificar los permisos
     *
     * @return array
     */
    public function dataProvider()
    {
        /* *****************   Obtener los datos de los roles  ************* */

        $id_role_superadmin = DB::table('roles')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        $id_role_admin = DB::table('roles AS r')
            ->join('users AS u', 'u.id_role', '=', 'r.id')
            ->where('u.name', 'LIKE', 'Administrador')
            ->value('r.id');

        $other_role = DB::table('roles')
            ->where('name', 'NOT LIKE', 'superadmin')
            ->where('name', 'NOT LIKE', 'Administrador')
            ->first();

        $roles = [
            'role_superadmin' => $id_role_superadmin,
            'role_admin' => $id_role_admin,
            'role_other' => $other_role->id
        ];


        return [[
            'roles' => $roles,
        ]];
    }


    /**
     * validar la creación de roles
     */
    public function testCreateRole()
    {
        $this->get(self::baseUrl . '/create')
            ->assertStatus(200);


        /* crear role sin permisos */
        $data = [
            'name'  => 'prueba',
            'status'=> true,
            'desc'  => 'Este es un role de pruebas'
        ];

        $response = $this->post(self::baseUrl . '/create', $data);

        $response->assertStatus(302);
        $response->isRedirection();

//        guardar el sqlite
        $id = $this->getId($response);




        /* crear role con permisos */

        // obtener permisos
        $permissions = DB::table('permissions')
            ->pluck('id')
            ->toArray();

        $data = [
            'name'  => 'prueba',
            'status'=> true,
            'desc'  => 'Este es un role de pruebas',
            'permissions' => $permissions
        ];

        $this->post(self::baseUrl . '/create', $data)
            ->assertStatus(302);

    }


    /**
     * @param array $roles
     *
     * @dataProvider dataProvider
     */
    public function testShowRoles(array $roles)
    {
        $this->get(self::baseUrl . '/show/' . $roles['role_superadmin'])->assertStatus(200);
        $this->get(self::baseUrl . '/show/' . $roles['role_admin'])->assertStatus(200);
        $this->get(self::baseUrl . '/show/' . $roles['role_other'])->assertStatus(200);
    }


    /**
     * Validación de superadmin
     *
     * @param array $roles
     *
     * @dataProvider dataProvider
     */
    public function testPermissionsForSuperadmin(array $roles)
    {
        DB::beginTransaction();

        $this->get(self::baseUrl)->assertStatus(200);

        $this->get(self::baseUrl .'/edit/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_admin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_other'])->assertStatus(200);

        $this->get(self::baseUrl .'/destroy/'. $roles['role_superadmin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_admin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_other'])->assertStatus(302);

        DB::rollback();
    }


    /**
     * @param $response
     *|
     * @return int
     */
    private function getId($response)
    {
        $url = $this->getUrl($response);
        $id = array_last(explode('/', $url));

        return $id;
    }



    /**
     * Obtener la url completa de la respuesta
     *
     * @param $response
     *
     * @return mixed
     */
    private function getUrl($response)
    {
        return $response->headers->get('location');
    }

}
