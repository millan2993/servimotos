<?php

namespace Tests\Feature\acl;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\Unit\rolesTest;

class rolePermissionsTest extends TestCase
{

    /**
     * url base para roles
     */
    const baseUrl = '/account/roles';


    /**
     * Requerido para poder hacer uso de los dataPrivider
     *
     * usersForSuperadminTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }


    /**
     * Verificar acceso a usuarios no autenticados
     *
     * @param array $users
     *
     * @dataProvider  dataProvider
     */
    public function testUsersDoesNotLogged(array $users)
    {
        $this->get('/account')->assertRedirect('/login');
        $this->get(self::baseUrl . '/create')->assertRedirect('/login');
        $this->get(self::baseUrl .'/show/'. $users['role_admin'])->assertRedirect('/login');
        $this->get(self::baseUrl .'/edit/'. $users['role_admin'])->assertRedirect('/login');
    }





    /**
     * Validación de permisos para un role común
     *
     * @param array $roles
     * @param array $users
     *
     * @dataProvider dataProvider
     */
    public function testPermissionsForOther(array $roles, array $users)
    {
        DB::beginTransaction();

        \Auth::loginUsingId($users['other']);

        $this->get(self::baseUrl)->assertStatus(200);
        $this->get(self::baseUrl . '/create')->assertStatus(200);

        $this->get(self::baseUrl .'/show/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/show/'. $roles['role_admin'])->assertStatus(200);
        $this->get(self::baseUrl .'/show/'. $roles['role_other'])->assertStatus(200);

        $this->get(self::baseUrl .'/edit/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_admin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_other'])->assertStatus(200);

        $this->get(self::baseUrl .'/destroy/'. $roles['role_superadmin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_admin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_other'])->assertStatus(302); // 302 por al eliminar es redirigido

        DB::rollback();
    }




    /**
     * retorna los roles para verificar los permisos
     *
     * @return array
     */
    public function dataProvider()
    {
        /* *****************   Obtener los datos de los users  ************* */

        $id_superadmin = DB::table('users')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        $id_admin = DB::table('users')
            ->where('name', 'LIKE', 'Administrador')
            ->value('id');

        $other = DB::table('users AS u')
            ->join('roles AS r', 'r.id', '=', 'u.id_role')
            ->whereNotIn('u.name', ['superadmin', 'Administrador'] )
            ->value('u.id');

        $users = [
            'superadmin' => $id_superadmin,
            'admin' => $id_admin,
            'other' => $other
        ];



        /* *****************   Obtener los datos de los roles  ************* */

        $id_role_superadmin = DB::table('roles')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        $id_role_admin = DB::table('roles AS r')
            ->join('users AS u', 'u.id_role', '=', 'r.id')
            ->where('u.name', 'LIKE', 'Administrador')
            ->value('r.id');

        $other_role = DB::table('roles')
            ->where('name', 'NOT LIKE', 'superadmin')
            ->where('name', 'NOT LIKE', 'Administrador')
            ->first();

        $roles = [
            'role_superadmin' => $id_role_superadmin,
            'role_admin' => $id_role_admin,
            'role_other' => $other_role->id
        ];


        return [[
            'roles' => $roles,
            'users' => $users
        ]];
    }


}
