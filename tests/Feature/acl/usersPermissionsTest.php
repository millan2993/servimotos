<?php

namespace Tests\Feature\acl;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class usersForSuperadminTest extends TestCase
{

    /**
     * Requerido para poder hacer uso de los dataPrivider
     *
     * usersForSuperadminTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }


    /**
     * @dataProvider    usersProvider
     */
    public function testUsers($admin)
    {
        $baseUsers = '/account/users';

//            usuario no autenticado
        $this->get('/account')->assertRedirect('/login');
        $this->get($baseUsers . '/create')->assertRedirect('/login');
        $this->get($baseUsers .'/show/'. $admin)->assertRedirect('/login');
        $this->get($baseUsers .'/edit/'. $admin)->assertRedirect('/login');
    }

    /**
     * Validación de superadmin
     *
     * @dataProvider usersProvider
     */
    public function testPermissionsForSuperadmin($superadmin, $admin, $without, $with)
    {
        DB::beginTransaction();

        $baseUsers = '/account/users';

        \Auth::loginUsingId($superadmin);

        $this->get('/account')->assertStatus(200);
        $this->get($baseUsers)->assertStatus(200);
        $this->get($baseUsers . '/create')->assertStatus(200);

        $this->get($baseUsers .'/show/'. $superadmin)->assertStatus(200);
        $this->get($baseUsers .'/show/'. $admin)->assertStatus(200);
        $this->get($baseUsers .'/show/'. $without)->assertStatus(200);

        $this->get($baseUsers .'/edit/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $without)->assertStatus(200);

        $this->get($baseUsers .'/destroy/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $without)->assertStatus(302); // 302 por al eliminar es redirigido


        DB::rollback();
    }



    /**
     * Validación para admin
     *
     * @dataProvider usersProvider
     */
    public function testPermissionsForAdmin($superadmin, $admin, $without, $with)
    {
        DB::beginTransaction();

        $baseUsers = '/account/users';

        \Auth::loginUsingId($admin);

        $this->get('/account')->assertStatus(200);
        $this->get($baseUsers)->assertStatus(200);
        $this->get($baseUsers . '/create')->assertStatus(200);

        $this->get($baseUsers .'/show/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/show/'. $admin)->assertStatus(200);
        $this->get($baseUsers .'/show/'. $without)->assertStatus(200);

        $this->get($baseUsers .'/edit/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $without)->assertStatus(200);

        $this->get($baseUsers .'/destroy/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $without)->assertStatus(302); // 302 porque al eliminar es redirigido


        DB::rollback();
    }



    /**
     * validación para usuarios comunes sin permisos
     *
     * @dataProvider usersProvider
     */
    public function testOtherUsersWithoutPermission($superadmin, $admin, $without, $with)
    {
        DB::beginTransaction();

        $baseUsers = '/account/users';

        \Auth::loginUsingId($without);

        $this->get('/account')->assertStatus(200);
        $this->get($baseUsers)->assertStatus(403);
        $this->get($baseUsers . '/create')->assertStatus(403);

        $this->get($baseUsers .'/show/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/show/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/show/'. $without)->assertStatus(200);
        $this->get($baseUsers .'/show/'. $with)->assertStatus(403);

        $this->get($baseUsers .'/edit/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $without)->assertStatus(200);
        $this->get($baseUsers .'/edit/'. $with)->assertStatus(403);

        $this->get($baseUsers .'/destroy/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $without)->assertStatus(302);
        $this->get($baseUsers .'/destroy/'. $with)->assertStatus(403);


        DB::rollback();
    }



    /**
     * validación para usuarios comunes sin permisos
     *
     * @dataProvider usersProvider
     */
    public function testOtherUsersWithPermission($superadmin, $admin, $without, $with)
    {
        DB::beginTransaction();

        $baseUsers = '/account/users';

        \Auth::loginUsingId($with);

        $this->get('/account')->assertStatus(200);
        $this->get($baseUsers)->assertStatus(200);
        $this->get($baseUsers . '/create')->assertStatus(200);

        $this->get($baseUsers .'/show/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/show/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/show/'. $without)->assertStatus(200);
        $this->get($baseUsers .'/show/'. $with)->assertStatus(200);

        $this->get($baseUsers .'/edit/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/edit/'. $without)->assertStatus(200);
        $this->get($baseUsers .'/edit/'. $with)->assertStatus(200);

        $this->get($baseUsers .'/destroy/'. $superadmin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $admin)->assertStatus(403);
        $this->get($baseUsers .'/destroy/'. $without)->assertStatus(302);
        $this->get($baseUsers .'/destroy/'. $with)->assertStatus(302);


        DB::rollback();
    }



    /**
     * proveedor de los datos de usuario objetivos
     *
     * @return array
     */
    public function usersProvider() {

        $id_superadmin = DB::table('users')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        $id_admin = DB::table('users')
            ->where('name', 'LIKE', 'Administrador')
            ->value('id');

        $withoutPermissions = DB::table('users')
            ->where('name', 'LIKE', 'sin permisos')
            ->value('id');

        $withPermissions = DB::table('users')
            ->where('name', 'LIKE', 'con permisos')
            ->value('id');

        return [
            'users' => [
                'superadmin' => $id_superadmin,
                'admin' => $id_admin,
                'without' => $withoutPermissions,
                'with' => $withPermissions
            ]
        ];
    }

}
