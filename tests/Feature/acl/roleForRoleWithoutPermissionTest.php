<?php

namespace Tests\Feature\acl;

use Illuminate\Support\Facades\Auth;
use Tests\TestCase;
use Illuminate\Support\Facades\DB;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class roleForRoleWithoutPermissionTest extends TestCase
{

    /**
     * url base para roles
     */
    const baseUrl = '/account/roles';


    /**
     * Requerido para poder hacer uso de los dataPrivider
     *
     * usersForSuperadminTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }


    /**
     * Setup the test environment.
     *
     * @return void
     */
    protected function setUp()
    {
        parent::setUp();


        $user = DB::table('users AS u')
            ->leftjoin('roles AS r', 'r.id', '=', 'u.id_role')
            ->whereNotIn('r.name', ['superadmin', 'Administrador'])
            ->whereNotExists(function ($query) {
                $query->select(DB::raw(1))
                    ->from('roles_permissions AS rp')
                    ->whereRaw('rp.id_role = r.id');
            })
            ->value('u.id');

        Auth::loginUsingId($user);
    }




    /**
     * Validación de permisos para un role común
     *
     * @param array $roles
     *
     * @dataProvider dataProvider
     */
    public function testPermissions(array $roles)
    {
        DB::beginTransaction();

        $this->get(self::baseUrl)->assertStatus(403);
        $this->get(self::baseUrl . '/create')->assertStatus(403);

        $this->get(self::baseUrl .'/show/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/show/'. $roles['role_admin'])->assertStatus(403);
        $this->get(self::baseUrl .'/show/'. $roles['role_other'])->assertStatus(403);

        $this->get(self::baseUrl .'/edit/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_admin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_other'])->assertStatus(403);

        $this->get(self::baseUrl .'/destroy/'. $roles['role_superadmin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_admin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_other'])->assertStatus(302);

        DB::rollback();
    }




    /**
     * retorna los roles para verificar los permisos
     *
     * @return array
     */
    public function dataProvider()
    {
        /* *****************   Obtener los datos de los roles  ************* */

        $id_role_superadmin = DB::table('roles')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        $id_role_admin = DB::table('roles AS r')
            ->join('users AS u', 'u.id_role', '=', 'r.id')
            ->where('u.name', 'LIKE', 'Administrador')
            ->value('r.id');

        $other_role = DB::table('roles')
            ->where('name', 'NOT LIKE', 'superadmin')
            ->where('name', 'NOT LIKE', 'Administrador')
            ->first();

        $roles = [
            'role_superadmin' => $id_role_superadmin,
            'role_admin' => $id_role_admin,
            'role_other' => $other_role->id
        ];


        return [[
            'roles' => $roles,
        ]];
    }


}

