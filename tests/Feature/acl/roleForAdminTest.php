<?php

namespace Tests\Feature\acl;

use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class roleForAdminTest extends TestCase
{

    /**
     * url base para roles
     */
    const baseUrl = '/account/roles';


    /**
     * Requerido para poder hacer uso de los dataPrivider
     *
     * usersForSuperadminTest constructor.
     *
     * @param null $name
     * @param array $data
     * @param string $dataName
     */
    public function __construct($name = null, array $data = array(), $dataName = '') {
        parent::__construct($name, $data, $dataName);

        $this->createApplication();
    }



    /**
     * retorna los roles para verificar los permisos
     *
     * @return array
     */
    public function dataProvider()
    {
        /* *****************   Obtener los datos de los users  ************* */

        $user = DB::table('users')
            ->where('name', 'LIKE', 'Administrador')
            ->value('id');


        /* *****************   Obtener los datos de los roles  ************* */

        $id_role_superadmin = DB::table('roles')
            ->where('name', 'LIKE', 'superadmin')
            ->value('id');

        $id_role_admin = DB::table('roles AS r')
            ->join('users AS u', 'u.id_role', '=', 'r.id')
            ->where('u.name', 'LIKE', 'Administrador')
            ->value('r.id');

        $other_role = DB::table('roles')
            ->where('name', 'NOT LIKE', 'superadmin')
            ->where('name', 'NOT LIKE', 'Administrador')
            ->first();

        $roles = [
            'role_superadmin' => $id_role_superadmin,
            'role_admin' => $id_role_admin,
            'role_other' => $other_role->id
        ];


        return [[
            'roles' => $roles,
            'user' => $user
        ]];
    }



    /**
     * Validación de superadmin
     *
     * @param array $roles
     * @param array $user
     *
     * @dataProvider dataProvider
     */
    public function testPermissionsForAdmin(array $roles, $user)
    {
        DB::beginTransaction();

        \Auth::loginUsingId($user);

        $this->get(self::baseUrl)->assertStatus(200);
        $this->get(self::baseUrl . '/create')->assertStatus(200);

        $this->get(self::baseUrl .'/show/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/show/'. $roles['role_admin'])->assertStatus(200);
        $this->get(self::baseUrl .'/show/'. $roles['role_other'])->assertStatus(200);

        $this->get(self::baseUrl .'/edit/'. $roles['role_superadmin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_admin'])->assertStatus(403);
        $this->get(self::baseUrl .'/edit/'. $roles['role_other'])->assertStatus(200);

        $this->get(self::baseUrl .'/destroy/'. $roles['role_superadmin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_admin'])->assertStatus(302);
        $this->get(self::baseUrl .'/destroy/'. $roles['role_other'])->assertStatus(302); // 302 por al eliminar es redirigido

        DB::rollback();
    }


}
