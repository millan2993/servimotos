<?php

namespace Tests\Browser;

use Faker\Factory;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Servimotos\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class RoleTest extends DuskTestCase
{

    public function testCreateRole()
    {
        $this->browse(function (Browser $browser) {

            $faker = Factory::create();
            $name = $faker->jobTitle;
            $desc = $faker->text(100);

            $name_updated = $faker->jobTitle;
            $desc_updated = $faker->text(100);

            $browser->loginAs(User::find(1))
                ->visit('/account/roles/create')
                ->type('#name', $name)
                ->radio('status', 1)
                ->type('#desc', $desc)
                ->check('#ver_usuarios')
//                ->check('permissions[]')
                ->press('Guardar cambios')

                ->assertPathBeginsWith('/account/roles/show')
                ->assertInputValue('#name', $name)
                ->assertChecked('#ver_usuarios')
                ->assertNotChecked('#crear_usuarios')
                ->assertInputValue('#desc', $desc)

                // edit
                ->clickLink('Editar')
                ->assertPathBeginsWith('/account/roles/edit')
                ->press('Guardar cambios')
                ->assertPathBeginsWith('/account/roles/show')
                ->assertInputValue('#name', $name)
                ->assertChecked('#ver_usuarios')
                ->assertNotChecked('#crear_usuarios')
                ->assertInputValue('#desc', $desc)
            ;
        });
    }



}
