<?php

namespace Tests\Browser;

use Servimotos\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    public function testLogin()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->assertSee('Servimotos')
                ->type('#email', 'superadmin@servimotos.com')
                ->type('#password', '123')
                ->press('Iniciar sesión')
                ->waitForLocation('/account')
                ->assertPathIs('/account');
        });
    }



    public function testLogout()
    {
        $this->browse(function (Browser $browser) {
            $browser->loginAs(User::find(1))
                ->visit('/account')
                ->assertPathIs('/account')
                ->clickLink('Salir');
        });
    }
}
