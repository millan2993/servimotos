BEGIN;

INSERT INTO modules (name, `desc`, status, folder, slug) VALUES
  ('Control de usuarios', 'Control de usuarios', true, 'acl', 'acl'),
  ('Servicios', 'Servicios ofrecidos por la empresa', true, 'services', 'services'),
  ('Recursos Humanos', 'Recursos humanos', true, 'rh', 'rh' ),
  ('Clientes', 'Control de clientes', true, 'clients', 'clients'),
  ('Vehiculos', 'Gestión de vehiculos y repuestos', TRUE, 'vehicles', 'vehicles'),
  ('Gestión de pedidos', 'Gestión de pedidos y ordenes de compra.', TRUE, 'order', 'orders'),
  ('Actividades', 'Gestión de actividades.', TRUE, 'activities', 'activities'),
  ('Reportes', 'Reportes.', TRUE, 'reports', 'reports')
;


INSERT INTO components (name, `desc`, status, slug, id_module) VALUES
  ('Usuarios', 'Control de cuentas de usuarios', 1, 'users', (select id from modules where slug like 'acl' )),
  ('Roles', 'Control de roles para usuarios', 1, 'roles', (select id from modules where slug like 'acl' )),
  ('Permisos', 'Control de permisos de acceso y generales.', 1, 'permissions', (select id from modules where slug like 'acl' )),

  ('Servicios', 'Servicios ofrecidos', 1, 'services', (SELECT id FROM modules WHERE slug = 'services')),
  ('Tipo de servicios', 'Tipos de servicios.', 1, 'typeservices', (SELECT id FROM modules WHERE slug = 'services')),

  ('Empleados', 'Empleados', 1, 'employees', (SELECT id FROM modules WHERE slug = 'rh')),
  ('Clientes', 'Control de clientes', 1, 'clients', (SELECT id from modules where slug like 'clients' )),

  ('Vehículos', 'Control de cuentas de usuarios', true, 'vehicles', (select id from modules where slug like 'vehicles' )),

  ('Indicadores', 'Indicadores gráficos', true, 'indicators', (select id from modules where slug like 'reports'))
;


INSERT INTO permissions (name, `desc`, status, type, id_component) VALUES
  ('ver_usuarios', 'Ver usuarios', true, 1, 1 ),
  ('crear_usuarios', 'crear usuarios', true, 1, 1 ),
  ('editar_usuarios', 'editar usuarios', true, 1, 1 ),
  ('eliminar_usuarios', 'eliminar usuarios', true, 1, 1 ),

  ('ver_roles', 'Ver roles', true, 1, 2 ),
  ('crear_roles', 'crear roles', true, 1, 2 ),
  ('editar_roles', 'editar roles', true, 1, 2 ),
  ('eliminar_roles', 'eliminar roles', true, 1, 2 ),

  ('ver_servicios', 'Ver servicios', true, 1, (select id from components where name = 'Servicios') ),
  ('crear_servicios', 'Ver servicios', true, 1, (select id from components where name = 'Servicios') ),
  ('editar_servicios', 'Ver servicios', true, 1, (select id from components where name = 'Servicios') ),
  ('eliminar_servicios', 'Ver servicios', true, 1, (select id from components where name = 'Servicios') ),

  ('ver_empleados', 'Ver empleados', true, 1, (select id from components where name = 'Empleados') ),
  ('crear_empleados', 'Ver empleados', true, 1, (select id from components where name = 'Empleados') ),
  ('editar_empleados', 'Ver empleados', true, 1, (select id from components where name = 'Empleados') ),
  ('eliminar_empleados', 'Ver empleados', true, 1, (select id from components where name = 'Empleados') ),

  ('ver_clientes', 'Ver clientes', true, 1, (select id from components where name = 'Clientes') ),
  ('ver_todos_clientes', 'Ver todos los clientes', true, 1, (select id from components where name = 'Clientes') ),
  ('crear_clientes', 'Ver clientes', true, 1, (select id from components where name = 'Clientes') ),
  ('editar_clientes', 'Ver clientes', true, 1, (select id from components where name = 'Clientes') ),
  ('eliminar_clientes', 'Ver clientes', true, 1, (select id from components where name = 'Clientes') ),

  ('ver_vehiculos', 'Ver vehiculos', true, 1, (select id from components where name = 'Vehículos') ),
  ('ver_todos_vehiculos', 'Ver todos los vehiculos', true, 1, (select id from components where name = 'Vehículos') ),
  ('crear_vehiculos', 'Ver vehiculos', true, 1, (select id from components where name = 'Vehículos') ),
  ('editar_vehiculos', 'Ver vehiculos', true, 1, (select id from components where name = 'Vehículos') ),
  ('eliminar_vehiculos', 'Ver vehiculos', true, 1, (select id from components where name = 'Vehículos') )
;

/**
 @todo Organizar el Component de actividades
 */
INSERT INTO permissions (name, `desc`, status, type ) VALUES
  ('ver_actividades', 'Ver actividades', true, 1 ),
  ('crear_actividades', 'Crear actividades', true, 1),
  ('editar_actividades', 'Editar actividades', true, 1),
  ('eliminar_actividades', 'Eliminar actividades', true, 1 ),
  ('ver_todos_actividades', 'Ver todas las actividades', true, 1),

  ('ver_order', 'Ver order', true, 1 ),
  ('crear_order', 'Crear order', true, 1),
  ('editar_order', 'Editar order', true, 1),
  ('eliminar_order', 'Eliminar order', true, 1 ),
  ('ver_todos_order', 'Ver todas las order', true, 1)
;



INSERT INTO roles (name, `desc`, status) VALUES
  ('superadmin', 'Super administrador de la plataforma, con todos los permisos de la aplicación', true),
  ('Administrador', 'Administrador de la plataforma, con la mayoría de los permisos', true)
;

INSERT INTO users (name, email, password, status, id_role) VALUES
  ('superadmin', 'superadmin@servimotos.com', '$2y$10$JJ0dxFhAHcSAzsQXDtgBUuG9YX/46oQOEqG0WyqE4TkLpEh/SCGHK', true, (SELECT id FROM roles r WHERE r.name = 'superadmin')),
  ('Administrador', 'admin@servimotos.com', '$2y$10$JJ0dxFhAHcSAzsQXDtgBUuG9YX/46oQOEqG0WyqE4TkLpEh/SCGHK', true, (SELECT id FROM roles r WHERE r.name = 'Administrador'))
;


CREATE PROCEDURE getAmountSalesByMonth(IN lang VARCHAR(10))
  BEGIN
    SET lc_time_names = lang;
    SELECT monthname(created_at) as month, count(*) as amount
    FROM orders
    WHERE year(created_at) = YEAR(now())
    GROUP BY year(created_at), monthname(created_at), month(created_at)
    ORDER BY month(created_at);
  END;

COMMIT;