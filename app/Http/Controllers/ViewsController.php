<?php

namespace Servimotos\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Servimotos\Models\Component;
use Servimotos\Models\Module;
use Servimotos\Modules\activities\Repositories\ActivityRepository;

class ViewsController extends Controller
{

    /**
     * Crear el menú automáticamente, teniendo en cuenta que los módulos y componentes se encuentren activos
     *
     * todo:: Organizar enlaces con módulos
     * todo:: Verificar que el usuario actual tenga permisos para el módulo y permiso
     *
     * @since 1.0
     * @return array
     */
    public function menu()
    {
        return Cache::rememberForever('menu_items', function () {
            return $this->getItemsMenu();
        });


    }

    /**
     * Obtener los items del menú
     */
    private function getItemsMenu()
    {
        $url_base = config('app.url') . '/account/';
        $module = new Module();
        $component = new Component();

        $items = $module->findBy(['id', 'name', 'slug'], array(['status', '=', true]), false);

        foreach ($items as $module) {
            $module->link = $url_base . $module->slug;
            $module->components = $component->findBy(['name', 'slug'], array(['status', true], ['id_module', $module->id]), false);

            foreach ($module->components as $item) {
                $item->link = $url_base . $item->slug;
            }
        }

        return $items;
    }


    public function getAcountActivities()
    {
        return Cache::remember('countActivitiesNotification', 5, function () {
            return ActivityRepository::getCountActivitiesNotification();
        });
    }

}
