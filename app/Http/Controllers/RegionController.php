<?php

namespace Servimotos\Http\Controllers;

use Illuminate\Http\Request;
use Servimotos\Region;

class RegionController extends Controller
{
    /**
     * Obtiene los estados de un determinado país
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getStatesById(Request $request)
    {
        $id_country = (int) $request->get('id');

        if (empty($id_country) || !is_int($id_country)) {
            return response()->json([], 200);
        }

        $states = Region::getStates($id_country);

        return response()->json([
            'states' => $states
        ]);

    }


    public function getCitiesByState(Request $request)
    {
        $id_state = (int) $request->get('id');

        if (empty($id_state) || !is_int($id_state)) {
            return response()->json([], 200);
        }

        $cities = Region::getCities($id_state);

        return response()->json([
            'cities' => $cities
        ]);
    }
}
