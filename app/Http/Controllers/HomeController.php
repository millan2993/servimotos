<?php

namespace Servimotos\Http\Controllers;

use Illuminate\Http\Request;
use Servimotos\Modules\activities\Repositories\ActivityRepository;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $activity = ActivityRepository::getAllBoard();

        return view('account', [
            'activity' => $activity
        ]);
    }
}
