<?php

namespace Servimotos\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Servimotos\Modules\acl\Models\Role;
use Servimotos\Modules\clients\models\Clients;
use Servimotos\Modules\order\Models\Order;
use Servimotos\User;

class OrderFinished extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.orderFinished')
            ->to($this->getTo())
            ->cc($this->getCc())
            ->bcc($this->getBcc())
            ->subject('Su trabajo esta listo');
    }



    /**
     * Correo de cliente
     *
     * @return User
     */
    private function getTo()
    {
        return Clients::where('id', $this->order->id_client)->value('email');
    }


    /**
     * Enviar correo a todos los empleados relacionados con la actividad
     */
    private function getCc()
    {
        $employees = $this->order->getRelatedEmployees();
        return array_pluck($employees, 'email');
    }


    /**
     * Enviar copia oculta a los administradores
     *
     * @return array
     */
    private function getBcc()
    {
        $role = Role::where('name', 'Administrador')->first(['id']);
        $users = User::where('id_role', $role->id)->get(['email']);

        return array_pluck($users, 'email');
    }

}
