<?php

namespace Servimotos;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class Region extends Model
{

    /**
     * Obtener todos los paises o el país con el ID indicado
     *
     * @param int|null $id
     * @return mixed
     */
    public static function getCountries(int $id = null)
    {
        $countries = DB::table('countries')->select('id', 'name');

        if (empty($id)) {
            return $countries->get();
        } else {
            return $countries->where('id', $id)->first();
        }
    }



    /**
     * Obtener todos los estados del país con el ID indicado
     *
     * @param int $idCountry
     * @return mixed
     */
    public static function getStates(int $idCountry)
    {
        return DB::table('states')->select('id', 'name')->where('id_country', $idCountry)->get();
    }



    /**
     * Obtiene el estado con el ID indicado
     *
     * @param int $id
     */
    public static function getState(int $id)
    {
        return DB::table('states')->select('id', 'name')->where('id', $id)->first();
    }



    /**
     * Obtener todos las ciudades del estado con el ID indicado
     *
     * @param int $idState
     * @return mixed
     */
    public static function getCities(int $idState)
    {
        return DB::table('cities')->select('id', 'name')->where('id_state', $idState)->get();
    }



    /**
     * Obtiene la ciudad con el ID indicado
     *
     * @param int $id
     */
    public static function getCity(int $id)
    {
        return DB::table('cities')->select('id', 'name')->where('id', $id)->first();
    }


    public static function saveAddress(int $idCity, $primary = null, $secondary = null, $barrio = null, $postal_code = null)
    {
        try {
            DB::beginTransaction();

            $id = DB::table('address')->insertGetId([
                    'primary' => $primary,
                    'secundary' => $secondary,
                    'barrio' => $barrio,
                    'postal_code' => $postal_code,
                    'id_city' => $idCity
                ]);

            DB::commit();
            return $id;

        } catch (QueryException $e) {
            DB::rollBack();

            $msg = 'Ocurrió un error al guardar la dirección.';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);
            throw $e;
        }
    }


    /**
     * Eliminar una dirección
     *
     * @param $id
     * @return void
     */
    public static function deleteAddress(int $id)
    {
        try {
            DB::beginTransaction();

            DB::table('address')
                ->where('id', $id)
                ->delete();

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }

}
