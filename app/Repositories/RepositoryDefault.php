<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/01/18
 * Time: 18:01
 */

namespace Servimotos\Repositories;


interface RepositoryDefault
{

    /**
     * @desc    Show main data for all resources
     *
     * @return  mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function all();


    /**
     * @desc Get a resource
     *
     * @param $id
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function show($id);


    /**
     * @desc Get a resource by conditionals
     *
     * @param $fields   array   Fields to get
     * @param $wheres   array   Conditionals
     * @param $first    boolean Get first row
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function findBy($fields = array(), $wheres = array(), $first);


    /**
     * @desc    Create a new resource
     *
     * @param   array   $data
     *
     * @return  mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function create($data);


    /**
     * @desc Update data for a resource
     *
     * @param int   $id
     * @param array $data
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function update($id, $data);


    /**
     * @desc
     *
     * @param $id int
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function delete($id);

}