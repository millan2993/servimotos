<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14/01/18
 * Time: 20:20
 */

namespace Servimotos\Repositories;


use Servimotos\Models\Module;

class ModuleRepository implements ModuleRepositoryInterface
{

    private $model;

    /**
     * ModuleReposititory constructor.
     */
    public function __construct(Module $module)
    {
        $this->model = $module;
    }

    public function findBy($fields, $wheres = array(), $first = false)
    {
        return $this->model->findBy($fields, $wheres, $first);
    }


    public function getComponents($id_module = null, $status = true, $permissions = false)
    {
        if ( empty($id_module) ) {
            logger()->warning('No se puede consultar componentes de un modulo sin identificador.');
            return array();
        }

        $this->model->id = $id_module;
        $this->model->status = $status;

        return $this->model->getComponents($permissions);
    }


}