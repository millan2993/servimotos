<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14/01/18
 * Time: 21:54
 */

namespace Servimotos\Repositories;


interface ComponentRepositoryInterface
{
    public function findBy($fields, $wheres = array(), $first = false);

    public function getPermissions($id_component = null);

    public function getPermissionsByRole($id_component, $id_role);
}