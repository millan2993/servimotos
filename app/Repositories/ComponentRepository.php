<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14/01/18
 * Time: 21:54
 */

namespace Servimotos\Repositories;


use Servimotos\Models\Component;

class ComponentRepository implements ComponentRepositoryInterface
{
    private $model;


    /**
     * ComponentRepository constructor.
     * @param Component $component
     */
    public function __construct(Component $component)
    {
        $this->model = $component;
    }

    public function findBy($fields, $wheres = array(), $first = false)
    {
        return $this->model->findBy($fields, $wheres, $first);
    }

    public function getPermissions($id_component = null, $type = null, $status = true)
    {
        if ( empty($id_component) ) {
            logger()->warning('Error al consultar componetes de un modulo sin identificador.');
            return array();
        }

        $this->model->id = $id_component;
        $this->model->type = $type;
        $this->model->status = $status;

        return $this->model->getPermissions();
    }


    public function getPermissionsByRole($id_component, $id_role)
    {
        $this->model->id = $id_component;

        return $this->model->getPermissionsByRole($id_role);
    }

}