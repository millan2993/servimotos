<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 14/01/18
 * Time: 20:21
 */

namespace Servimotos\Repositories;


interface ModuleRepositoryInterface
{

    public function findBy($fields, $wheres = array(), $first = false);

    public function getComponents($id_module = null);

}