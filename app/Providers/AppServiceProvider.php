<?php

namespace Servimotos\Providers;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        Schema::defaultStringLength(191);


        if ( env( 'APP_ENV', 'local' ) !== 'local' )
        {
            \DB::connection()->disableQueryLog();
        }

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $base = config('app.name') . '\Repositories';

        $this->app->bind(
            $base . '\ModuleRepositoryInterface',
            $base . '\ModuleRepository'
        );

        $this->app->bind(
            $base . '\ComponentRepositoryInterface',
            $base . '\ComponentRepository'
        );

        if ($this->app->environment('local', 'testing')) {
             $this->app->register(DuskServiceProvider::class);
        }

    }
}
