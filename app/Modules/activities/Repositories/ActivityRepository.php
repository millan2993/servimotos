<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 21/06/18
 * Time: 11:45 PM
 */

namespace Servimotos\Modules\activities\Repositories;


use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\activities\Models\Activity;
use Servimotos\Modules\activities\Repositories\ActivityInterface;
use Servimotos\Modules\order\Models\Order;
use Servimotos\Modules\order\Repositories\OrderRepository;

class ActivityRepository implements ActivityInterface
{
    /**
     * @var Activity
     */
    public $activity;


    /**
     * ActivityRepository constructor.
     * @param $activity
     */
    public function __construct(Activity $activity)
    {
        $this->activity = $activity;
    }

    /**
     * @desc    Show main data for all resources
     *
     * @return  mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function all()
    {
        // TODO: Implement all() method.
    }

    /**
     * @desc Get a resource
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function show()
    {
        return $this->activity->show();
    }

    /**
     * @desc Get a resource by conditionals
     *
     * @param $fields   array   Fields to get
     * @param $wheres   array   Conditionals
     * @param $first    boolean Get first row
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function findBy($fields = array(), $wheres = array(), $first)
    {
        // TODO: Implement findBy() method.
    }

    /**
     * @desc    Create a new resource
     *
     * @return  mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function create()
    {
        try {
            $this->activity->code = $this->activity->code ?? uniqid();
            $this->activity->id_status = 1;

            if (empty($this->activity->id_order_detail) && empty($this->activity->id_service)) {
                Session::push('error', 'La actividad no ha podido ser creada porque debe estar asociada a una orden o servicio.');
                throw new Exception();
            }

            return $this->activity->insert();
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @desc Update data for a resource
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function update()
    {
        $this->activity->id_status = $this->getIdStatusByExecPercent($this->activity->execution_percent);

        if ($this->activity->execution_percent == 100 ) {
            $this->activity->end = Carbon::now();
        }

        return $this->activity->edit();
    }

    /**
     * @desc
     *
     * @return mixed
     * @see     2017/11/06
     * @author  Alex Millán
     */
    public function delete()
    {
        try {
            if (!empty($this->activity->getOrder())) {
                Session::push('error', 'La actividad puede ser eliminada porque está asociada a una orden.');
                throw new Exception();
            }

            if ($this->activity->execution_percent > 0) {
                Session::push('error', 'La actividad puede ser eliminada ya que ha sido ejecutada anteriormente.');
                throw new Exception();
            }

            return $this->activity->remove();
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Obtener las actividades organizadas por categorias para el board
     *
     * @return mixed
     */
    public static function getAllBoard()
    {
        try {
            return (new Activity())->getAllBoard();
        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Obtener todas las actividades por estado
     *
     * @param int|string $status
     * @return Collection
     */
    public function getActivitiesByStatus($status)
    {
        try {
            return $this->activity->getByStatus($status);

        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Obtener el porcentaje por defecto de acuerdo a la lista o board
     *
     * @param string $board
     *
     * @return int
     */
    public static function getPorcentExecByBoard(string $board) : int
    {
        switch ($board)
        {
            case 'completed': return 100;
            case 'inprogress' : return 50;
            default: return 0;
        }
    }



    /**
     * Obtener el id del estado de acuerdo al porcentaje de ejecución
     *
     * @param int $execution_percent
     * @return int
     */
    public static function getIdStatusByExecPercent(int $execution_percent) : int
    {
        if ($execution_percent == 100) {
            return 3;
        } elseif ($execution_percent > 10 && $execution_percent <= 99) {
            return 2;
        } else {
            return 1;
        }
    }


    /**
     * actualizar porcentaje de ejecución
     *
     * @return Activity
     */
    public function updateExecPercent()
    {
        $this->activity->id_status = $this->getIdStatusByExecPercent($this->activity->execution_percent);

        if ($this->activity->execution_percent === 100) {
            $this->activity->end = Carbon::now();
        }

        if (auth()->user()->isEmployee()) {
            $this->activity->id_employee = auth()->user()->id_employee;
        }

        $id_order = $this->activity->getOrder();
        if (!empty($id_order))
        {
            $order = Order::find($id_order);
            $count_other_activities = $order->getCountOtherActivities($this->activity->id);

            if ($count_other_activities == 0 && $this->activity->execution_percent === 100) {
                $order->id_status = Order::CERRADA;
            } else {
                $order->id_status = Order::PROCESO;
            }

            $order->edit();
        }


        return $this->activity->updateExecPercent();
    }


    /**
     * Obtener la cantidad de actividades pendientes y en proceso, para las notificaciones
     *
     * @return int
     */
    public static function getCountActivitiesNotification()
    {
        try {
            if (!auth()->user()->isAdministrator()) {
                $activity = new Activity();

                if (auth()->user()->isEmployee()) {
                    return $activity->getCountActivitiesByEmployee(auth()->user()->id_employee);
                }
            }

            return Activity::getCountAllPendientes();

        } catch (Exception $e) {
            logger()->warning('Ocurrió un error al obtener la actividades pendientes', [ $e->getMessage(), $e->getTraceAsString() ]);
            return 0;
        }
    }




}