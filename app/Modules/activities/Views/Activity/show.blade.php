@extends('layouts.account')

@section('external_files_header')
  @parent
@endsection


@section('content')

  <form id="activity_form" role="form">

    <div class="ibox float-e-margins">

      <div class="ibox-title">
        <a href="{{ url('account/activities/edit', $activity->id) }}" id="btn_edit" class="btn btn-primary btn-sm">Editar</a>
        <a href="{{ url('account/activities/destroy', $activity->id) }}" id="btn_delete" class="btn btn-primary btn-sm">Eliminar</a>
        <a href="{{ url('account/activities/create') }}" id="btn_create" class="btn btn-primary btn-sm">Nuevo</a>
      </div>


      <div class="ibox-content">

        <div class="row">

          {{-- code --}}
          <div id="div-codigo" class="col-md-2 col-sm-6">
            <div class="form-group">
              <label for="code" class="control-label col-xs-12">Código</label>
              <input id="code" name="code" type="text" class="form-control" value="{{ $activity->code }}" disabled>
            </div>
          </div>

            {{-- estado --}}
            <div id class="col-md-2 col-sm-6 ">
              <div class="form-group">
                <label for="expected_created" class="control-label col-xs-12">Estado</label>
                <button class="btn btn-primary btn-outline btn-rounded btn-block">
                  <strong>{{ strtoupper($activity->status) }}</strong></button>
              </div>
            </div>

             {{--porcentaje --}}
            <div class="col-md-2 col-sm-6 outspace-percent-exec text-center">
              <h2>{{ $activity->execution_percent }}%</h2>
              <div class="progress progress-mini">
                <div style="width: {{ $activity->execution_percent }}%;" class="progress-bar"></div>
              </div>
            </div>


          {{-- fecha fin --}}
          <div id="div-creacion" class="col-md-2 col-md-push-4 col-sm-6">
            <div class="form-group">
              <label for="expected_created" class="control-label col-xs-12">Fecha creación</label>
              <input id="expected_created" name="expected_date" type="text" class="form-control"
                     value="{{ $activity->created_at }}" disabled>
            </div>
          </div>

        </div>






        <div class="hr-line-dashed"></div>


        <div class="row">

          {{-- fecha inicio --}}
          <div class="col-md-2 col-sm-6">
            <div class="form-group">
              <label for="start" class="control-label col-xs-12">Fecha inicio</label>
              <input id="start" name="start" type="text" class="form-control" value="{{ $activity->start }}" disabled>
            </div>
          </div>

          {{-- fecha fin --}}
          <div class="col-md-2 col-sm-6">
            <div class="form-group">
              <label for="end" class="col-md-10 control-label">Fecha Fin</label>
              <input id="end" name="end" type="text" class="form-control" value="{{ $activity->end }}" disabled>
            </div>
          </div>


            <div class="col-md-2 col-md-push-4 col-sm-6 col-xs-12">
              <div class="form-group">
                <label for="execution_percent" class="control-label col-xs-12">Tiempo estimado</label>
                <input id="execution_percent" name="execution_percent" type="text" class="form-control"
                       value="{{ $activity->estimated_time or 0 }} minutos" disabled>
              </div>
            </div>

            {{-- fecha esperada --}}
            <div class="col-md-2 col-md-push-4 col-sm-6">
              <div class="form-group">
                <label for="start" class="control-label col-xs-12">F. esperada</label>
                <input id="start" name="start" type="text" class="form-control" value="{{ $activity->expected_date }}"
                       disabled>
              </div>
            </div>
          </div>



        <div class="hr-line-dashed"></div>

        <div class="row">

          {{-- service --}}
          <div class="col-md-4 col-sm-6">
            <div class="form-group">
              <label for="service" class="control-label col-xs-12">Servicio:</label>
              <a class="btn btn-primary btn-outline btn-rounded"
                 href="{{ url('account/services/show', $activity->id_service) }}">{{ $activity->service }}</a>
            </div>
          </div>

          {{-- employee --}}
          @if($activity->id_employee)
            <div class="col-md-4 col-sm-6">
              <div class="form-group">
                <label for="employee" class="control-label col-xs-12">Ejecutor:</label>
                <a class="btn btn-primary btn-outline btn-rounded"
                   href="{{ url('account/employees/show', $activity->id_employee) }}">{{ $activity->employee }}</a>
              </div>
            </div>
          @endif

          {{-- order --}}
          @if($activity->id_order)
            <div class="col-md-4 col-sm-6">
              <div class="form-group">
                <label for="order" class="control-label col-xs-12">Orden</label>
                <a class="btn btn-primary btn-outline btn-rounded"
                   href="{{ url('account/orders/show', $activity->id_order) }}">{{ $activity->order_code }}</a>
              </div>
            </div>
          @endif

        </div>


      </div>


    </div>
  </form>

@section('external_files_footer')
  @parent

  {{-- Select2 --}}
  <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

@endsection

@endsection
