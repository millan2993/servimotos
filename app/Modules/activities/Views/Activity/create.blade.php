@extends('layouts.account')

@section('external_files_header')
    @parent

    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endsection


@section('content')

    <form id="frmCreateActivity" role="form" method="post">
        {{ csrf_field() }}

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button id="send" class="btn btn-primary btn-sm" onclick="$('#frmCreateActivity').submit();">Guardar cambios</button>
            </div>

            <div class="ibox-content">

                <div class="row">

                    {{-- fecha inicio --}}

                    <div class="col-md-5">

                        {{-- Date --}}
                        <div class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('start_date')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Fecha inicio</label>
                            <input id="start_date" name="start_date" type="date" class="form-control" value="{{ old('start_date') }}">

                            @include('layouts.components.error_input', ['input' => 'start_date'])
                        </div>

                        {{-- Time --}}
                        <div class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('start_time')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Hora Inicio</label>
                            <div class="input-group clockpicker">
                                <input id="start_time" name="start_time" type="text" readonly class="form-control" value="{{ old('start_time') }}" >

                                {{-- icono--}}
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                            @include('layouts.components.error_input', ['input' => 'start_time'])
                        </div>

                    </div>


                    {{-- expected datetime --}}
                    <div class="col-md-5 col-md-push-2">

                        {{-- Date --}}
                        <div class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('expected_date')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Fecha esperada</label>
                            <input id="expected_date" name="expected_date" type="date" class="form-control" value="{{ old('expected_date') }}">

                            @include('layouts.components.error_input', ['input' => 'expected_date'])
                        </div>

                        {{-- Time --}}
                        <div class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('expected_time')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Hora esperada</label>
                            <div class="input-group clockpicker">
                                <input id="expected_time" name="expected_time" type="text" readonly class="form-control" value="{{ old('expected_time') }}" >

                                {{-- icono--}}
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                            @include('layouts.components.error_input', ['input' => 'expected_time'])
                        </div>

                    </div>

                </div>


                <div class="hr-line-dashed"></div>


                <div class="row">

                    {{-- service --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label for="service" class="control-label col-xs-10">Servicio:</label>
                            <select name="service" id="service" class="form-control">
                                <option></option>
                                @foreach($services as $service)
                                    <option value="{{ $service->id }}" {{ $service->id != old('service') ?: 'selected'}}>
                                        {{ $service->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    {{-- employee --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label for="employee" class="control-label col-xs-10">Ejecutor:</label>
                            <select name="employee" id="employee" class="form-control">
                                <option></option>
                                @foreach($employees as $employee)
                                    <option value="{{ $employee->id }}" {{ $employee->id != old('employee') ?: 'selected' }}>
                                        {{ $employee->first_name }} {{ $employee->last_name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div>


                </div>
        </div>
    </form>

@section('external_files_footer')
    @parent

    {{-- Select2 --}}
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

    <!-- Clock picker -->
    <script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>


    <script>

        $("#service").select2({
            placeholder: "Seleccione un servicio",
            allowClear: true
        });

        $("#employee").select2({
            placeholder: "Seleccione un empleado",
            allowClear: true
        });

        $('.clockpicker').clockpicker({
            default: 'now',
            donetext: 'Listo'
        });

    </script>

@endsection

@endsection
