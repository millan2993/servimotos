@extends('layouts.account')

@section('external_files_header')
  @parent

  <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endsection


@section('content')

  <form id="activity_form" role="form" method="post">
    {{ csrf_field() }}

    <input id="execution_percent" name="execution_percent" type="hidden"
           value="{{ $activity->execution_percent or 0 }}">

    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <button class="btn btn-primary btn-xs" type="submit">Guardar cambios</button>
      </div>

      <div class="ibox-content">


        <div class="row">

          {{-- code --}}
          <div class="col-md-4 form-group">
            <label for="code" class="control-label">Código: <h3>{{ $activity->code }}</h3></label>
          </div>

          <div class="col-md-4 col-md-push-4 form-group">
            <h2 id="execution_percent_edit_activity" class="text-center"
                contenteditable="true">{{ $activity->execution_percent or 0 }} %</h2>
            <div class="progress progress-mini">
              <div style="width: {{ $activity->execution_percent or 0 }}%;" class="progress-bar"></div>
            </div>
          </div>
        </div>


        @if(!$activity->id_order)
          <div class="hr-line-dashed"></div>

          <div class="row">

            {{-- fecha inicio --}}
            <div class="col-md-5">

              {{-- Date --}}
              <div class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('start_date')) ? 'has-error' : '' }}">
                <label class="col-md-12 control-label">Fecha inicio</label>
                <input id="start_date" name="start_date" type="date" class="form-control"
                       value="{{ date("Y-m-d", strtotime($activity->start)) }}">

                @include('layouts.components.error_input', ['input' => 'start_date'])
              </div>

              {{-- Time --}}
              <div class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('start_time')) ? 'has-error' : '' }}">
                <label class="col-md-12 control-label">Hora Inicio</label>
                <div class="input-group clockpicker">
                  <input id="start_time" name="start_time" type="text" readonly class="form-control"
                         value="{{ date("H:iA", strtotime($activity->start)) }}">

                  {{-- icono--}}
                  <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                </div>
                @include('layouts.components.error_input', ['input' => 'start_time'])
              </div>

            </div>


            {{-- expected datetime --}}
            <div class="col-md-5 col-md-push-2">

              {{-- Date --}}
              <div
                  class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('expected_date')) ? 'has-error' : '' }}">
                <label class="col-md-12 control-label">Fecha esperada</label>
                <input id="expected_date" name="expected_date" type="date" class="form-control"
                       value="{{ date("Y-m-d", strtotime($activity->expected_date)) }}">

                @include('layouts.components.error_input', ['input' => 'expected_date'])
              </div>

              {{-- Time --}}
              <div
                  class="col-md-6 col-sm-6 form-group {{ ($errors->errores->has('expected_time')) ? 'has-error' : '' }}">
                <label class="col-md-12 control-label">Hora esperada</label>
                <div class="input-group clockpicker">
                  <input id="expected_time" name="expected_time" type="text" readonly class="form-control"
                         value="{{ date("H:iA", strtotime($activity->expected_date)) }}">

                  {{-- icono--}}
                  <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                </div>
                @include('layouts.components.error_input', ['input' => 'expected_time'])
              </div>

            </div>

          </div>
         @endif


          <div class="hr-line-dashed"></div>


          <div class="row">

            @if(!$activity->id_order)
              {{-- service --}}
              <div class="col-md-4 col-sm-6">
                <div class="form-group">
                  <label for="service" class="control-label col-xs-10">Servicio:</label>
                  <select name="service" id="service" class="form-control">
                    <option></option>
                    @foreach($services as $service)
                      <option value="{{ $service->id }}" {{ $service->id == $activity->id_service  ?: 'selected'}}>
                        {{ $service->name }}
                      </option>
                    @endforeach
                  </select>
                </div>
              </div>
            @endif


            {{-- employee --}}
            <div class="col-md-4 col-sm-6">
              <div class="form-group">
                <label for="employee" class="control-label col-xs-10">Ejecutor:</label>
                <select name="employee" id="employee" class="form-control">
                  <option></option>
                  @foreach($employees as $employee)
                    <option value="{{ $employee->id }}" {{ ($activity->id_employee != $employee->id ) ?: 'selected' }} >
                      {{ $employee->first_name }} {{ $employee->last_name }}
                    </option>
                  @endforeach
                </select>
              </div>
            </div>

          </div>


      </div>
    </div>
  </form>

@section('external_files_footer')
  @parent

  {{-- Select2 --}}
  <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

  <!-- Clock picker -->
  <script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>

  <script>

    $("#service").select2({
      placeholder: "Seleccione un servicio",
      allowClear: true
    });

    $("#employee").select2({
      placeholder: "Seleccione un empleado",
      allowClear: true
    });

    $('.clockpicker').clockpicker({
      default: 'now',
      donetext: 'Listo'
    });

  </script>

@endsection

@endsection
