@extends('layouts.account')

@section('external_files_header')
  @parent

  <link href="{{ asset('css/plugins/footable/footable.core.css') }}" rel="stylesheet">
@endsection


@section('content')

  <div class="row">
    <div class="col-sm-12">
      <div class="ibox">
        <div class="ibox-content">
          <h2>Actividades</h2>


          <div class="clients-list">

            <div class="row">
              <ul class="nav nav-tabs">

                  <li class="active">
                    <a data-toggle="tab" href="#tab-1">
                      <i class="fa fa-user hidden-xs"></i>Pendientes
                    </a>
                  </li>

                  <li>
                    <a data-toggle="tab" href="#tab-2">
                      <i class="fa fa-briefcase hidden-xs"></i>En proceso
                    </a>
                  </li>

                  <li>
                    <a data-toggle="tab" href="#tab-3">
                      <i class="fa fa-briefcase hidden-xs"></i>Terminadas
                    </a>
                  </li>

              </ul>
            </div>

            <div class="row outspace-top outspace-bottom">
              <div class="col-xs-8">
                <input type="text" class="input-sm m-b-xs form-control search" id="filter" placeholder="Buscar">
              </div>
              <div class="col-xs-4">
                <a class="btn btn-primary pull-right" href="{{ url('account/activities/create') }}">Crear Nuevo</a>
              </div>
            </div>

            <div class="tab-content">

              {{--  Pendientes  --}}
              <div id="tab-1" class="tab-pane active">

                <div class="table-responsive">
                  <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                    <thead>
                    <tr>
                      <th>Actividad</th>
                      <th>Servicio</th>
                      <th class="hidden-xs">Orden de compra</th>
                      <th class="hidden-xs">Fecha inicio</th>
                      <th>Fecha fin</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($todo as $activity)
                      <tr>
                        <td>
                          <a href="{{ url('account/activities/show', $activity->id) }}">{{ $activity->code }}</a>
                          <br/>
                          <small>Creado: {{ $activity->created_at }}</small>
                        </td>
                        <td class="project-title">
                          <a href="{{ url('account/services/show', $activity->id_service) }}">{{ $activity->service }}</a>
                          <br/>
                          <small>Cantidad: {{ $activity->amount }}</small>
                        </td>
                        <td class="hidden-xs">
                          <a href="{{ url('account/orders/show', $activity->id_order) }}">{{ $activity->order }}</a>
                        </td>
                        <td class="hidden-xs">
                          {{ $activity->start }}
                        </td>
                        <td>
                          {{ $activity->end }}
                        </td>
                      </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                      <td colspan="8">
                        <ul class="pagination pull-right"></ul>
                      </td>
                    </tr>
                    </tfoot>
                  </table>

                </div>
              </div>


              {{--  En proceso  --}}
              <div id="tab-2" class="tab-pane">

                <div class="table-responsive">
                  <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>
                    <thead>
                      <tr>
                        <th>Actividad</th>
                        <th class="hidden-xs">Servicio</th>
                        <th class="hidden-xs">Orden</th>
                        <th class="hidden-xs">Asignado a:</th>
                        <th>Ejecución</th>
                        <th>Fecha fin</th>
                      </tr>
                    </thead>
                    <tbody>

                    @foreach($inProgress as $activity)
                      <tr>
                        <td>
                          <a href="{{ url('account/activities/show', $activity->id) }}">{{ $activity->code }}</a>
                          <br/>
                          <small>Creado: {{ $activity->created_at }}</small>
                        </td>
                        <td class="project-title hidden-xs">
                          <a href="{{ url('account/services/show', $activity->id_service) }}">{{ $activity->service }}</a>
                          <br/>
                          <small>Cantidad: {{ $activity->amount }}</small>
                        </td>
                        <td class="hidden-xs">
                          <a href="{{ url('account/orders/show', $activity->id_order) }}">{{ $activity->code }}</a>
                        </td>
                        <td class="hidden-xs">
                          <a href="{{ url('account/employees/show', $activity->id_employee) }}">{{ $activity->employee }}</a>
                        </td>
                        <td class="project-completion">
                          <small class="center text-center">
                            {{ $activity->execution_percent or 0 }} %
                          </small>
                          <div class="progress progress-mini">
                            <div style="width: {{ $activity->execution_percent or 0 }}%;"
                                 class="progress-bar"></div>
                          </div>
                        </td>
                        <td> {{ $activity->end }} </td>
                      </tr>
                    @endforeach

                    </tbody>

                    <tfoot>
                      <tr>
                        <td colspan="8">
                          <ul class="pagination pull-right"></ul>
                        </td>
                      </tr>
                    </tfoot>
                  </table>
                </div>

              </div>


              {{--  Terminadas  --}}
              <div id="tab-3" class="tab-pane">

                <div class="table-responsive">
                  <table class="footable table table-stripped" data-page-size="8" data-filter=#filter>

                    <thead>
                    <tr>
                      <th>Actividad</th>
                      <th class="hidden-xs">Servicio</th>
                      <th class="hidden-xs">Orden</th>
                      <th class="hidden-xs">Asignado a:</th>
                      <th>Fecha fin</th>
                    </tr>
                    </thead>
                    <tbody>

                    @foreach($terminadas as $activity)
                      <tr>
                        <td>
                          <a href="{{ url('account/activities/show', $activity->id) }}">{{ $activity->code }}</a>
                          <br/>
                          <small>Creado: {{ $activity->created_at }}</small>
                        </td>
                        <td class="project-title hidden-xs">
                          <a href="{{ url('account/services/show', $activity->id_service) }}">{{ $activity->service }}</a>
                          <br/>
                          <small>Cantidad: {{ $activity->amount }}</small>
                        </td>
                        <td class="hidden-xs">
                          <a href="{{ url('account/orders/show', $activity->id_order) }}">{{ $activity->code }}</a>
                        </td>
                        <td class="hidden-xs">
                          <a href="{{ url('account/employees/show', $activity->id_employee) }}">{{ $activity->employee }}</a>
                        </td>
                        <td>
                          {{ $activity->end }}
                        </td>
                      </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                      <td colspan="8">
                        <ul class="pagination pull-right"></ul>
                      </td>
                    </tr>
                    </tfoot>
                  </table>
                </div>
              </div>

            </div>

          </div>
        </div>
      </div>
    </div>
  </div>




@section('external_files_footer')
  @parent

  <!-- FooTable -->
  <script src="{{ asset('js/plugins/footable/footable.all.min.js') }}"></script>

  <!-- Page-Level Scripts -->
  <script>
    $(document).ready(function () {
      $('.footable').footable();
    });

  </script>
@endsection

@endsection
