<?php

namespace Servimotos\Modules\activities\Policies;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\activities\Models\Activity;

use Illuminate\Auth\Access\HandlesAuthorization;

class ActivityPolicy
{
    use HandlesAuthorization;


    /**
     * Si es administrador tiene todos los permisos
     *
     * @param $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isAdministrator()) {
            return true;
        }
    }


    /**
     * Determine whether the user can view the activity.
     *
     * @param  User  $user
     * @param  Activity  $activity
     * @return mixed
     */
    public function view(User $user, Activity $activity)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_actividades');
    }

    /**
     * Determine whether the user can create activities.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'crear_actividades');
    }

    /**
     * Determine whether the user can update the activity.
     *
     * @param  User  $user
     * @param  Activity  $activity
     * @return mixed
     */
    public function update(User $user, Activity $activity)
    {
        return (new Role())->hasPermission($user->id_role, 'editar_actividades');
    }


    /**
     * Determine whether the user can delete the activity.
     *
     * @param  User  $user
     * @param  Activity  $activity
     * @return mixed
     */
    public function delete(User $user, Activity $activity)
    {
        return (new Role())->hasPermission($user->id_role, 'eliminar_actividades');
    }


    /**
     * Determine whether the user can view all activities.
     *
     * @param  User  $user
     * @return mixed
     */
    public function viewAll(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_todos_actividades');
    }
}
