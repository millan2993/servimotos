<?php
/**
 * Created by PhpStorm.
 * User: Alex Millán
 * Date: 28/04/18
 * Time: 21:34
 */


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {


    /*
     * vehicles
     */
    Route::group(['prefix' => '/activities' ], function () {

        Route::get('/', 'ActivityController@index');

        Route::get('/show/{id}', 'ActivityController@show');

        Route::get('/create', 'ActivityController@create');
        Route::post('/create', 'ActivityController@store');

        Route::get('/edit/{id}', 'ActivityController@edit');
        Route::post('/edit/{id}', 'ActivityController@update');

        Route::get('/destroy/{id}', 'ActivityController@destroy');

        Route::post('/changestatus', 'ActivityController@changestatus');

    });



});




