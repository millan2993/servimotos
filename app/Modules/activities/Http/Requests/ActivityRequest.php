<?php

namespace Servimotos\Modules\activities\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ActivityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'execution_percent' => 'nullable|integer',
            'start_date'        => 'nullable|date',
            'start_time'        => 'nullable',
            'expected_date'     => 'nullable|date|required_with:start_date|after_or_equal:start_date',
            'expected_time'     => 'nullable|required_with:expected_date',
            'employee'          => 'nullable|integer',
            'service'           => 'nullable|integer'
        ];
    }
}
