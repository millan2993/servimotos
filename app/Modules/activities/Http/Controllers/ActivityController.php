<?php

namespace Servimotos\Modules\activities\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\activities\Http\Requests\ActivityRequest;
use Servimotos\Modules\activities\Models\Activity;
use Servimotos\Modules\activities\Repositories\ActivityRepository;
use Servimotos\Modules\rh\Models\Employee;
use Servimotos\Modules\services\Models\Service;

class ActivityController extends Controller
{

    /**
     * @var ActivityRepository
     */
    private $activityRepo;


    /**
     * ActivityController constructor.
     */
    public function __construct()
    {
        $this->activityRepo = new ActivityRepository(new Activity());
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAll', Activity::class);

        // actividades
        $todo       = $this->activityRepo->getActivitiesByStatus(1);
        $inProgress = $this->activityRepo->getActivitiesByStatus(2);
        $terminadas = $this->activityRepo->getActivitiesByStatus(3);


        return view('activities::Activity.index', [
            'todo'       => $todo,
            'inProgress' => $inProgress,
            'terminadas'  => $terminadas
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Activity::class);

        try {
            $services  = Service::findBy( [ 'id', 'name' ], [ 'status' => true ] );
            $employees = Employee::findBy( ['id', 'first_name', 'last_name' ] );

            return view('activities::Activity.create', [
                'services'  => $services,
                'employees' => $employees
            ]);

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al mostrar una actividad', [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withErrors('Ocurrió un error al consultar la actividad.');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param ActivityRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(ActivityRequest $request)
    {
        $this->authorize('create', Activity::class);

        try {

            $validator = Validator::make($request->all(), [
                'start_date' => 'required|date',
                'start_time' => 'required',
                'service'    => 'required|integer'
            ]);

            if ($validator->fails()) {
                return back()
                    ->withErrors($validator)
                    ->withInput();
            }

            // fecha inicio
            $start_date = $request->get('start_date');
            $start_time = $request->get('start_time');
            $start = Carbon::createFromFormat('Y-m-d H:iA', $start_date .' '. $start_time);

            /*  end datetime */
            $expected_date = $request->get('expected_date');
            $expected_time = $request->get('expected_time');

            if (!empty($expected_date) || !empty($expected_time)) {
                $end = Carbon::createFromFormat('Y-m-d H:iA', $expected_date . ' ' . $expected_time);

                // validar que la ficha esperada no sea menor a la fecha inicial
                if ($end < $start) {
                    Session::push('warning', 'La fecha de inicio debe ser menor a la fecha esperada.');
                    throw new Exception();
                }

                $diffMinutes = $start->diffInMinutes($end);
                $this->activityRepo->activity->estimated_time = $diffMinutes;
            }


            $this->activityRepo->activity->start = $start;
            $this->activityRepo->activity->id_service = $request->get('service');
            $this->activityRepo->activity->id_employee = $request->get('employee');

            $this->activityRepo->create();

            return redirect('account/activities/show/' . $this->activityRepo->activity->id)
                ->with('success', 'La actividad fue creada satisfactoriamente.');

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al crear la actividad.';
            Session::push('error', $msg);
            logger()->error($msg, [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
//            validar permisos
            $this->activityRepo->activity = Activity::find($id);
            $this->authorize('view', $this->activityRepo->activity);

            $activity = $this->activityRepo->activity->show();

            return view('activities::Activity.show', [
                'activity' => $activity,
            ]);

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al mostrar una actividad', [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withErrors('Ocurrió un error al consultar la actividad.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
//            validar permisos
            $this->activityRepo->activity = Activity::find($id);
            $this->authorize('update', $this->activityRepo->activity);

            $activity = $this->activityRepo->activity->show();

            $services  = Service::findBy( [ 'id', 'name' ], [ 'status' => true ] );
            $employees = Employee::findBy( ['id', 'first_name', 'last_name' ] );

            return view('activities::Activity.edit', [
                'activity' => $activity,
                'services' => $services,
                'employees' => $employees
            ]);

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al mostrar una actividad', [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withErrors('Ocurrió un error al consultar la actividad.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|ActivityRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ActivityRequest $request, $id)
    {
        try {
//            validar permisos
            $this->activityRepo->activity = Activity::find($id);
            $this->authorize('update', $this->activityRepo->activity);

            // validaciones adicionales si no tiene una orden asociada
            $order = $this->activityRepo->activity->getOrder();

            if (empty($order)) {

                $validator = Validator::make($request->all(), [
                    'start_date' => 'required|date',
                    'start_time' => 'required',
                    'service'    => 'required|integer'
                ]);

                if ($validator->fails()) {
                    return back()
                        ->withErrors($validator)
                        ->withInput();
                }

                // fecha inicio
                $start_date = $request->get('start_date');
                $start_time = $request->get('start_time');
                $start = Carbon::createFromFormat('Y-m-d H:iA', $start_date . ' ' . $start_time);


                /**************** obtener el tiempo estimado ******************* */
                /*  end datetime */
                $expected_date = $request->get('expected_date');
                $expected_time = $request->get('expected_time');

                if (!empty($expected_date) || !empty($expected_time)) {
                    $end = Carbon::createFromFormat('Y-m-d H:iA', $expected_date . ' ' . $expected_time);

                    // validar que la ficha esperada no sea menor a la fecha inicial
                    if ($end < $start) {
                        Session::push('warning', 'La fecha de inicio debe ser menor a la fecha esperada.');
                        throw new Exception();
                    }

                    $diffMinutes = $start->diffInMinutes($end);
                    $this->activityRepo->activity->estimated_time = $diffMinutes;
                }

                $this->activityRepo->activity->start = $start;
                $this->activityRepo->activity->id_service = $request->get('service');
            }

            $this->activityRepo->activity->id = $id;
            $this->activityRepo->activity->id_employee = $request->get('employee');
            $this->activityRepo->activity->execution_percent = $request->get('execution_percent');


            $this->activityRepo->update();

            return redirect('account/activities/show/'. $id);

        } catch (Exception $e) {
            logger()->error('Ocurrio un error al editar la actividad.', [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withErrors('Ocurrió un error al editar la actividad.')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
//            validar permisos
            $this->activityRepo->activity = Activity::find($id);
            $this->authorize('delete', $this->activityRepo->activity);

            $this->activityRepo->delete();

            return redirect('account/activities')
                ->with('success', 'La actividad se ha eliminado correctamente.');

        } catch (Exception $e) {
            logger()->error('Ocurrio un error al eliminar la actividad.', [$e->getMessage(), $e->getTrace()]);
            Session::push('error', 'Ocurrió un error al eliminar la actividad.');
            return back()
                ->withInput();
        }
    }


    public function changeStatus(Request $request)
    {
        $id    = $request->get('id');
        $board = $request->get('board');

        if (empty($id) || empty($board)) {
            return response()->json([
                'status'    => 'error',
                'message'   => 'Error al capturar la actividad en acción'
            ]);
        }

        $this->activityRepo->activity = Activity::find($id);
        $this->activityRepo->activity->execution_percent = ActivityRepository::getPorcentExecByBoard($board);


        $this->activityRepo->updateExecPercent();

        return response()->json([
            'status' => true
        ]);
    }
}
