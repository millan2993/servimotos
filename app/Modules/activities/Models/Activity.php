<?php

namespace Servimotos\Modules\activities\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * Class Activity
 *
*@package Servimotos\Modules\activities\Models
 *
 * @property int    id
 * @property string code
 * @property int    estimated_time
 * @property int    id_status
 * @property string start
 * @property string end
 * @property int    id_order_detail
 * @property int    id_employee
 * @property float  execution_percent
 * @property int    id_service
 *
 * @property Carbon created_at
 * @property Carbon updated_at
 */
class Activity extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'estimed_time', 'id_status', 'start', 'end', 'id_order_detail', 'id_employee', 'execution_percent'
    ];



    /**
     * obtener datos para el ver
     *
     * @return Collection
     */
    public function show()
    {
        return DB::table('activities AS act')
            ->select('act.id', 'act.code', 'act.estimated_time', 'act.id_status', 'st.name as status', 'act.start', 'act.end', 'act.created_at',
                'act.id_employee', 'act.execution_percent', 'od.id_order', 'o.code as order_code', 's.id as id_service', 's.name as service',
                DB::raw('concat(emp.first_name, " ", emp.last_name) as employee'),
                DB::raw('DATE_ADD(act.start, INTERVAL act.estimated_time MINUTE) as expected_date')
                )
            ->leftjoin('activity_status AS st', 'act.id_status', '=', 'st.id')
            ->leftjoin('employees AS emp', 'act.id_employee', '=', 'emp.id')
            ->leftjoin('order_details AS od', 'act.id_order_detail', '=', 'od.id')
            ->leftjoin('orders AS o', 'od.id_order', '=', 'o.id')
            ->leftjoin('services AS s', function ($join) {
                $join->on('s.id', '=', 'act.id_service');
                $join->orOn('s.id', '=', 'od.id');
            })
            ->where('act.id', $this->id)
            ->first();
    }


    /**
     * Crear una nueva actividad
     *
     * @return $this
     * @throws \Exception
     */
    public function insert()
    {
        try {
            DB::beginTransaction();

            $this->saveOrFail();

            DB::commit();
            return $this;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al crear una actividad.', [$e->getMessage()]);
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al crear una actividad.', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Actualizar una actividad
     *
     * @return $this
     */
    public function edit()
    {
        try {
            $this->updated_at = Carbon::now();

            DB::beginTransaction();

            DB::table('activities')
                ->where('id', $this->id)
                ->update([
                    'start'     => $this->start,
                    'end'       => $this->end,
                    'id_status' => $this->id_status,
                    'id_employee'       => $this->id_employee,
                    'estimated_time'    => $this->estimated_time,
                    'execution_percent' => $this->execution_percent,
                    'id_service'        => $this->id_service,
                    'updated_at'        => $this->updated_at
                ]);

            DB::commit();
            return $this;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar una actividad.', [$e->getMessage()]);
            throw $e;

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al actualizar una actividad.', [$e->getMessage()]);
            throw $e;

        }
    }



    /**
     * Obtener actividades por estado para el board
     * retorna un collection con 3 collections internos
     *
     * @return Collection
     */
    public function getAllBoard()
    {
        $todo = DB::table('activities AS a')
            ->select('a.id', 'a.end', 'a.execution_percent', 'st.name as status', 'od.observation', 'od.id_order',
                    'o.code as order_code', 's.name as service', 'o.id_client', 'a.id_employee', 's.id AS id_service',
                DB::Raw('concat(cl.first_name, " ", cl.last_name) as client'),
                DB::Raw('concat(emp.first_name, " ", emp.last_name) as employee'),
                DB::Raw('coalesce(od.amount, 1) as amount')
            )
            ->leftjoin('activity_status AS st', 'st.id', '=', 'a.id_status')
            ->leftjoin('order_details AS od', 'od.id', '=', 'a.id_order_detail')
            ->leftjoin('orders AS o', 'od.id_order', '=', 'o.id')
            ->leftjoin('clients AS cl', 'cl.id', '=', 'o.id_client')
            ->leftjoin('services AS s', function ($join) {
                $join->on('s.id', '=', 'a.id_service');
                $join->orOn('s.id', '=', 'od.id_service');
            })
            ->leftjoin('employees AS emp', 'emp.id', '=', 'a.id_employee')
            ->where('a.id_status', 1)
            ->get();

        $doing = DB::table('activities AS a')
            ->select('a.id', 'a.end', 'a.execution_percent', 'st.name as status', 'od.observation', 'od.id_order',
                    'o.code as order_code', 's.name as service', 'o.id_client', 'a.id_employee', 's.id AS id_service',
                DB::Raw('concat(cl.first_name, " ", cl.last_name) as client'),
                DB::Raw('concat(emp.first_name, " ", emp.last_name) as employee'),
                DB::Raw('coalesce(od.amount, 1) as amount')
            )
            ->leftjoin('activity_status AS st', 'st.id', '=', 'a.id_status')
            ->leftjoin('order_details AS od', 'od.id', '=', 'a.id_order_detail')
            ->leftjoin('orders AS o', 'od.id_order', '=', 'o.id')
            ->leftjoin('clients AS cl', 'cl.id', '=', 'o.id_client')
            ->leftjoin('services AS s', function ($join) {
                $join->on('s.id', '=', 'a.id_service');
                $join->orOn('s.id', '=', 'od.id_service');
            })
            ->leftjoin('employees AS emp', 'emp.id', '=', 'a.id_employee')
            ->where('a.id_status', 2)
            ->get();


        $deliver = DB::table('activities AS a')
            ->select('a.id', 'a.end', 'a.execution_percent', 'st.name as status', 'od.observation', 'od.id_order',
                    'o.code as order_code', 's.name as service', 'o.id_client', 'a.id_employee', 's.id AS id_service',
                DB::Raw('concat(cl.first_name, " ", cl.last_name) as client'),
                DB::Raw('concat(emp.first_name, " ", emp.last_name) as employee'),
                DB::Raw('coalesce(od.amount, 1) as amount')
            )
            ->leftjoin('activity_status AS st', 'st.id', '=', 'a.id_status')
            ->leftjoin('order_details AS od', 'od.id', '=', 'a.id_order_detail')
            ->leftjoin('orders AS o', 'od.id_order', '=', 'o.id')
            ->leftjoin('clients AS cl', 'cl.id', '=', 'o.id_client')
            ->leftjoin('services AS s', function ($join) {
                $join->on('s.id', '=', 'a.id_service');
                $join->orOn('s.id', '=', 'od.id_service');
            })
            ->leftjoin('employees AS emp', 'emp.id', '=', 'a.id_employee')
            ->where('a.id_status', 3)
            ->get();


        $items = new Collection();
        $items->put('todo', $todo);
        $items->put('doing', $doing);
        $items->put('deliver', $deliver);

        return $items;
    }


    /**
     * Obtener la actividad asociada con la order detail
     *
     * @param int $id
     * return int
     */
    public static function getIdByOrderDetail(int $id)
    {
        return DB::table('activities')
            ->where('id_order_detail', $id)
            ->value('id');
    }


    /**
     * Obtener las actividades por su estado
     *
     * @param int $id_status
     * @return Collection
     * @throws Exception
     */
    public function getByStatus(int $id_status) : Collection
    {
        try {
            return DB::table('activities AS act')
                ->select('act.id', 'act.code', 'st.name as status', 'act.start', 'act.created_at',
                    'act.id_employee', 'act.execution_percent', 's.id as id_service', 's.name as service', 'od.amount', 'od.id_order',
                    'o.code as order',
                    DB::Raw('concat(emp.first_name, " ", emp.last_name) as employee'),
                    DB::Raw('coalesce(act.end, DATE_ADD(act.start, INTERVAL act.estimated_time MINUTE)) AS end')
                )
                ->leftjoin('activity_status AS st', 'st.id', '=', 'act.id_status')
                ->leftjoin('employees AS emp', 'act.id_employee', '=', 'emp.id')
                ->leftjoin('order_details AS od', 'od.id', '=', 'act.id_order_detail')
                ->leftjoin('orders AS o', 'o.id', '=', 'od.id_order')
                ->leftjoin('services AS s', function ($join) {
                    $join->on('s.id', '=', 'act.id_service');
                    $join->orOn('s.id', '=', 'od.id');
                })
                ->where('act.id_status', $id_status)
                ->get();

        } catch (QueryException $e) {
            logger()->error('Falló al consultar actividades por estado.');
            throw $e;
        }
    }


    /**
     * Obtener el id del estado por el nombre
     *
     * @param string $status
     * @return int
     * @throws Exception
     */
    public static function getIdStatusByName(string $status)
    {
        try {
            return DB::table('activity_status')
                ->where('name', 'like', $status)
                ->value('id');

        } catch (Exception $e) {
            logger()->error('Falló al consultar el Id del estado por nombre.');
            throw $e;
        }
    }


    /**
     * Eliminar una actividad
     *
     * @return mixed
     */
    public function remove()
    {
        DB::beginTransaction();
        try {
            $deleted = DB::table('activities')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

            return $deleted;
        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }



    /**
     * actualizar el estado de ejecución
     *
     * @return $this
     */
    public function updateExecPercent()
    {
        try {
            $this->updated_at = Carbon::now();

            DB::beginTransaction();

            DB::table('activities')
                ->where('id', $this->id)
                ->update([
                    'id_status'         => $this->id_status,
                    'execution_percent' => $this->execution_percent,
                    'updated_at'        => $this->updated_at,
                    'id_employee'       => $this->id_employee,
                    'end'               => $this->end
                ]);

            DB::commit();
            return $this;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar una actividad.', [$e->getMessage()]);
            throw $e;

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al actualizar una actividad.', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Obtener la cantidad de actividades pendientes y en proceso
     *
     * return int
     */
    public static function getCountAllPendientes()
    {
        return DB::table('activities')
            ->where('id_status', '<>', 3 )
            ->count();
    }


    /**
     * Obtener la cantidad de actividades por empleadas
     *
     * @param int $id_employee
     *
     * @return int
     */
    public function getCountActivitiesByEmployee(int $id_employee)
    {
        return DB::table('activities')
            ->where('id_status', '<>', 3)
            ->where('id_employee', $id_employee)
            ->count();
    }


    /**
     * Obtener la orden asociada a la actividad
     *
     * @return Collection
     */
    public function getOrder()
    {
        return DB::table('activities AS act')
            ->join('order_details AS od', 'od.id', '=', 'act.id_order_detail')
            ->where('act.id', $this->id)
            ->value('od.id_order');
    }
}
