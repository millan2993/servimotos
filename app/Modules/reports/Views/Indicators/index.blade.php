@extends('layouts.account')

@section('external_files_header')
    @parent

    <!-- amCharts javascript sources -->
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/serial.js"></script>

    <!-- amCharts plugins exports -->
    <script type="text/javascript" src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css">

@endsection

@section('content')

    <div class="row">

        <div class="col-md-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Cumplimiento de entrega de solicitudes de servicio</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div id="OSCumplimiento" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                    </div>
                </div>
            </div>
        </div>



        <div class="col-md-6">
            <div class="ibox ">
                <div class="ibox-title">
                    <h5>Ventas por mes en el año actual</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="close-link">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div id="saleByMonth" style="width: 100%; height: 400px; background-color: #FFFFFF;" ></div>
                    </div>
                </div>
            </div>
        </div>

    </div>




    @section('external_files_footer')
        @parent


        <!-- amCharts javascript code -->
        <script type="text/javascript">

            /**
             * Promedio de cumplimiento de ordenes de servicio
             */
            AmCharts.makeChart("OSCumplimiento", {
                "type": "pie",
                "angle": 19.8,
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "depth3D": 12,
                "innerRadius": "40%",
                "labelRadius": -20,
                "baseColor": "#B12F2C",
                "hoverAlpha": 0.95,
                "labelTickAlpha": 0,
                "marginBottom": 0,
                "marginTop": 0,
                "outlineAlpha": 0.61,
                "outlineThickness": 2,
                "titleField": "category",
                "valueField": "column-1",
                "visibleInLegendField": "column-1",
                "accessibleTitle": "",
                "handDrawScatter": 4,
                "percentPrecision": 0,
                "tapToActivate": false,
                "fontSize": 20,
                "creditsPosition": "bottom-right",
                "allLabels": [],
                "balloon": {},
                "export": {
                    "enabled": true
                },
                "dataProvider": JSON.parse(@json($avgCumplimientoEntregaOS))
            });


            /**
             * ventas por mes
             */
            AmCharts.makeChart("saleByMonth",
            {
                "creditsPosition": "bottom-right",
                "type": "serial",
                "categoryField": "category",
                "columnSpacing": 31,
                "columnWidth": 0.68,
                "dataDateFormat": "mm",
                "autoMarginOffset": 17,
                "depth3D": 4,
                "marginBottom": 0,
                "marginLeft": 0,
                "marginRight": 70,
                "marginTop": 5,
                "minMarginBottom": 0,
                "minMarginLeft": 0,
                "minMarginRight": 5,
                "minMarginTop": 5,
                "startDuration": 1,
                "accessibleTitle": "",
                "percentPrecision": 0,
                "categoryAxis": {
                    "gridPosition": "start"
                },
                "chartCursor": {
                    "enabled": true,
                    "animationDuration": 0.4
                },
                "chartScrollbar": {
                    "enabled": true
                },
                "trendLines": [],
                "graphs": [
                    {
                        "fillAlphas": 1,
                        "id": "AmGraph-1",
                        "title": "graph 1",
                        "type": "column",
                        "valueField": "column-1"
                    }
                ],
                "guides": [],
                "valueAxes": [],
                "allLabels": [],
                "balloon": {},
                "export": {
                    "enabled": true
                },
                "titles": [],
                "dataProvider": JSON.parse(@json($amountSales))
            }
        );
    </script>


    @endsection

@endsection
