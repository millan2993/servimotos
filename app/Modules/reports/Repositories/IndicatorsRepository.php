<?php

namespace Servimotos\Modules\reports\Repositories;

use Servimotos\Modules\activities\Models\Activity;
use Servimotos\Modules\order\Models\Order;

class IndicatorsRepository
{

    /**
     * Obtener el promedio de ordenes de servicio
     */
    public function getAvgPromedioCumplimientoOS()
    {
        $order = new Order();
        return $order->getAvgCumplimiento();
    }


    /**
     * Obtener el promedio de cumplimiento de actividades
     *
     * @return array
     *
     * @todo Enviar el idioma para obtener el nombre de los meses
     */
    public function getAmountSalesByMonth()
    {
        $order = new Order();
        return $order->getAmountSalesByMonth();
    }




}