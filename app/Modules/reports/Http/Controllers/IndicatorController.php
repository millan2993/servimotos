<?php

namespace Servimotos\Modules\reports\Http\Controllers;

use Illuminate\Http\Request;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\reports\Repositories\IndicatorsRepository;


class IndicatorController extends Controller
{
    /**
     * @var IndicatorsRepository
     */
    private $indicatorRepo;

    /**
     * IndicatorController constructor.
     *
     */
    public function __construct()
    {
        $this->indicatorRepo = new IndicatorsRepository();
    }


    public function index()
    {
        $avg_cumplumiento = $this->indicatorRepo->getAvgPromedioCumplimientoOS() ?? 100;
        $avg_incumplimiento = 100 - $avg_cumplumiento;

        $avgs = [
            [
                'category' => 'Cumplimiento',
                'column-1' => $avg_cumplumiento
            ],
            [
                'category' => 'Incumplimiento',
                'column-1' => $avg_incumplimiento
            ]
        ];

        $avgCumplimientoEntregaOS = json_encode($avgs);

        $amountSales = $this->getSalesByMonth();

        return view('reports::Indicators.index', [
            'avgCumplimientoEntregaOS' => $avgCumplimientoEntregaOS,
            'amountSales' => $amountSales
        ]);
    }


    /**
     * obtener la cantidad de ventas del año actual
     */
    private function getSalesByMonth()
    {
        $sales = $this->indicatorRepo->getAmountSalesByMonth();

        $salesJSON = [];
        foreach ($sales as $sale) {
            $salesJSON[] = [
                'category' => ucfirst($sale->month),
                'column-1' => $sale->amount
            ];
        }

        return json_encode($salesJSON);
    }

}
