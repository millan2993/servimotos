<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AmountsalesByMonth extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $driver = DB::connection()->getConfig('driver');

        switch ($driver) {
            case 'mysql':
                DB::unprepared('
                    CREATE PROCEDURE getAmountSalesByMonth(lang VARCHAR(10))
                      BEGIN
                        SET lc_time_names = lang;
                        SELECT count(created_at) as amount, MONTHNAME(created_at) as month
                        FROM orders
                        WHERE YEAR(created_at) = year(NOW())
                        GROUP BY MONTH(created_at), MONTHNAME(created_at);
                      END;
                ');
                break;
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP PROCEDURE getAmountSalesByMonth');
    }
}
