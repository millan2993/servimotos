<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/01/18
 * Time: 4:16
 */

Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {

    Route::group(['prefix' => '/indicators' ], function () {

        Route::get('/', 'IndicatorController@index');

    });

});
