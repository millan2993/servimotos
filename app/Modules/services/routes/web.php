<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/01/18
 * Time: 4:16
 */


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {


    /*
     * Servicios
     */
    Route::group(['prefix' => '/services' ], function () {
        Route::get('/', 'ServiceController@index');
        Route::get('/show/{id}', 'ServiceController@show');

        Route::get('/create', 'ServiceController@create');
        Route::post('/create', 'ServiceController@store');

        Route::get('/edit/{id}', 'ServiceController@edit');
        Route::post('/edit/{id}', 'ServiceController@update');

        Route::get('/destroy/{id}', 'ServiceController@destroy');


        /**
         * Ajax
         */

        Route::post('/getprice', 'ServiceController@getPrice');
    });


    /**
     * type services
     */
    Route::group( ['prefix' => '/typeservices' ], function () {
        Route::get('/', 'TypeServiceController@index');
        Route::post('/create', 'TypeServiceController@store');
        Route::post('/edit/{id}', 'TypeServiceController@update');
        Route::get('/destroy/{id}', 'TypeServiceController@destroy');

        Route::post('/ajaxcreate', 'TypeServiceController@ajaxCreate');
        Route::post('/ajaxedit/{id}', 'TypeServiceController@ajaxEdit');
        Route::get('/ajaxdelete/{id}', 'TypeServiceController@ajaxDelete');

    });




});




