<?php

namespace Servimotos\Modules\services\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesProviders extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $base = config('app.name') . '\Modules\services\Repositories';

        $this->app->bind(
            $base . '\Services\ServiceRepositoryInterface',
            $base . '\Services\ServiceRepository'
        );

        $this->app->bind(
            $base . '\Typeservices\TypeservicesRepositoryInterface',
            $base . '\Typeservices\TypeservicesRepository'
        );

    }
}
