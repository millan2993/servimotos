<?php

namespace Servimotos\Modules\Services\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Servimotos\Modules\services\Models\Service;
use Servimotos\Modules\services\Policies\ServicePolicy;

class AuthServiceProvicer extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Service::class => ServicePolicy::class
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }

}
