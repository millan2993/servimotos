<?php

namespace Servimotos\Modules\services\Http\Controllers;

use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\services\Models\Service;
use Servimotos\Modules\services\Repositories\Typeservices\TypeservicesRepositoryInterface;

class TypeServiceController extends Controller
{

    private $typeServiceRepo;

    /**
     * TypeServiceController constructor.
     * @param $typeservicesRepository
     */
    public function __construct(TypeservicesRepositoryInterface $typeservicesRepository)
    {
        $this->typeServiceRepo = $typeservicesRepository;
    }


    /**
     * Obtener todos los tipos de servicios
     */
    public function index()
    {
        $this->authorize('view', Service::class);

        $typeServices = $this->typeServiceRepo->all();

        return view('services::Typeservices.index', [
            'typeServices' => $typeServices
        ]);
    }


    /**
     * Crear un tipo de servicio por ajax
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxCreate(Request $request)
    {
        $this->authorize('create', Service::class);

        try {
            $id = $this->typeServiceRepo->create([
                'name' => $request->get('name'),
                'desc' => $request->get('desc')
            ]);

            return response()->json([
                'id' => $id
            ]);


        } catch (QueryException $e) {
            throw $e;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => 'Ha ocurrido un error al crear el tipo de servicio'
                ]
            ]);
        }

    }


    public function ajaxEdit(Request $request, $id)
    {
        $this->authorize('update', Service::class);

        try {
            $id = $this->typeServiceRepo->update($id, [
                'name' => $request->get('name'),
                'desc' => $request->get('desc')
            ]);

            return response()->json([
                'id' => $id
            ]);


        } catch (QueryException $e) {
            throw $e;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => 'Ha ocurrido un error al editar el tipo de servicio'
                ]
            ]);
        }
    }




    /**
     * Eliminar un tipo de servicio desde ajax
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxDelete($id)
    {
        $this->authorize('delete', Service::class);

        try {
            $deleted = $this->typeServiceRepo->delete($id);

            return response()->json([
                'deleted' => $deleted
            ]);


        } catch (QueryException $e) {
            throw $e;
        } catch (\Exception $e) {
            return response()->json([
                'errors' => [
                    'message' => 'Ha ocurrido un error al crear el tipo de servicio'
                ]
            ]);
        }

    }
}
