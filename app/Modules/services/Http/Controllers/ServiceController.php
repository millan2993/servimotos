<?php

namespace Servimotos\Modules\services\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\services\Http\Requests\ServiceRequest;
use Servimotos\Modules\services\Models\Service;
use Servimotos\Modules\services\Models\TypeService;
use Servimotos\Modules\services\Repositories\Services\ServiceRepositoryInterface;
use Servimotos\User;


class ServiceController extends Controller
{

    /**
     * @var ServiceRepositoryInterface
     */
    private $serviceRepo;



    /**
     * ServiceController constructor.
     * @param ServiceRepositoryInterface $serviceRepository
     */
    public function __construct(ServiceRepositoryInterface $serviceRepository)
    {
        $this->serviceRepo = $serviceRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('view', Service::class);

        $services = $this->serviceRepo->all();

        return view('services::Services.index', [
            'services' => $services
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Service::class);

        $typeServices = TypeService::findBy(['id', 'name']);

        return view('services::Services.create', [
            'typeServices' => $typeServices
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|ServiceRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $this->authorize('create', Service::class);

        try {
            $data = $request->except('_token');
            $idService = $this->serviceRepo->create($data);

            return redirect('account/services/show/' . $idService)
                ->with('success', 'Servicio creado exitosamente.');

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al crear un servicio.', [ $e->getMessage(), $e->getTraceAsString() ]);
            Session::push('error', 'Ocurrió un error al crear el servicio.');
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', Service::class);

        try {
            $service = $this->serviceRepo->show($id);

            if (empty($service)) {
                Session::push('error', 'El servicio solicitado no existe.');
                throw new Exception();
            }

            return view('services::Services.show', [
                'service' => $service
            ]);

        } catch (Exception $e) {
            $message = 'Ocurrió un error al visualizar el servicio.';
            logger()->error($message, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $message);
            return redirect()->back();
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Service::class);

        try {
            $service = $this->serviceRepo->show($id);

            if (empty($service)) {
                Session::push('error', 'El servicio solicitado no existe.');
                throw new Exception();
            }

            $typeServices = TypeService::findBy(['id', 'name']);

            return view('services::Services.edit', [
                'service'      => $service,
                'typeServices' => $typeServices
            ]);

        } catch (Exception $e) {
            $message = 'Ocurrió un error al editar el servicio.';
            logger()->error($message, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $message);
            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|ServiceRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
        $this->authorize('update', Service::class);

        try {
            $data = $request->except('_token');

            $idService = $this->serviceRepo->update($id, $data);

            if (empty($idService)) {
                return back()->withErrors('Ocurrió un error al editar el servicio.')->withInput();
            }

            return redirect('account/services/show/' . $id);

        } catch (Exception $e) {
            $message = 'Ocurrió un error al actualizar el servicio.';
            logger()->error($message, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $message);
            return redirect()->back();
        }
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('delete', Service::class);

            $deleted = $this->serviceRepo->delete($id);

            if ($deleted == false) {
                return redirect('account/services')->withErrors('Ocurrió un error al eliminar el rol.');
            }

            return redirect('account/services');

        } catch (Exception $e) {
            $message = 'Ocurrió un error al eliminar el servicio.';
            logger()->error($message, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $message);
            return redirect()->back();
        }
    }



    /**
     * Obtener datos para el select
     */
    public function select()
    {
        $services = $this->serviceRepo->findBy( [ 'id', 'name' ]);

        return response()->json([
            'services' => $services
        ]);
    }


    /**
     * Obtener el precio de un servicio
     *
     * @param Request $request  id del servicio
     * @return \Illuminate\Http\JsonResponse
     *
     * @date 13/05/2018
     * @author Alex Millán
     */
    public function getPrice(Request $request)
    {
        $wheres = [
            [ 'id', '=', $request->get('id') ]
        ];

        $service = $this->serviceRepo->findBy( ['price'],  $wheres, true);

        return response()->json([
            'price' => $service->price
        ]);
    }

}
