<?php

namespace Servimotos\Modules\services\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ServiceRequest extends FormRequest
{

    /**
     * Definir el tipo de error al formulario
     *
     * @var string
     */
    protected $errorBag = 'errores';


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'      => 'required|min:3',
            'desc'      => 'nullable|min:3|max:500',
            'status'    => 'required',
            'time'      => 'date_format:H:i',
//            'time'      => 'regex:/^\+?[1-9]+:[0-9]{2}$/',
            'type'      => 'required',
            'price'     => 'required|integer'
        ];
    }


    /**
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }
}
