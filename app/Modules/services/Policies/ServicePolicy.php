<?php

namespace Servimotos\Modules\services\Policies;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\services\Models\Service;
use Illuminate\Auth\Access\HandlesAuthorization;

class ServicePolicy
{
    use HandlesAuthorization;

    /**
     * Si es administrador tiene todos los permisos
     *
     * @param $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isAdministrator()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the service.
     *
     * @param  User  $user
     * @param  Service|null  $service
     * @return mixed
     */
    public function view(User $user, Service $service = null)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_servicios');
    }

    /**
     * Determine whether the user can create services.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'crear_servicios');
    }

    /**
     * Determine whether the user can update the service.
     *
     * @param  User  $user
     * @param  Service  $service
     * @return mixed
     */
    public function update(User $user, Service $service = null)
    {
        return (new Role())->hasPermission($user->id_role, 'editar_servicios');
    }

    /**
     * Determine whether the user can delete the service.
     *
     * @param  User  $user
     * @param  Service  $service
     * @return mixed
     */
    public function delete(User $user, Service $service = null)
    {
        return (new Role())->hasPermission($user->id_role, 'eliminar_servicios');
    }
}
