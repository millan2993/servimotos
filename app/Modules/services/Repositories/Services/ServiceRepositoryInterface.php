<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/02/18
 * Time: 5:32
 */

namespace Servimotos\Modules\services\Repositories\Services;

use Servimotos\Repositories\RepositoryDefault;

interface ServiceRepositoryInterface extends RepositoryDefault
{

}