<?php

namespace Servimotos\Modules\services\Repositories\Services;

use Illuminate\Support\Facades\DB;
use Servimotos\Modules\services\Models\Service;

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 10/02/18
 * Time: 5:32
 */
class ServiceRepository implements ServiceRepositoryInterface
{

    /**
     * @var Service
     */
    private $service;

    /**
     * SerivceRepository constructor.
     * @param Service $service
     * @internal param $model
     */
    public function __construct(Service $service)
    {
        $this->service = $service;
    }


    /**
     * @return mixed
     */
    public function all()
    {
        return $this->service->index();
    }


    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $this->service->id = $id;
        return $this->service->show();
    }


    /**
     * @param array $fields
     * @param array $wheres
     * @param bool $first
     * @return mixed
     */
    public function findBy($fields = array(), $wheres = array(), $first)
    {
        return $this->service->findBy($fields, $wheres, $first);
    }


    /**
     * @param array $data
     * @return mixed
     */
    public function create($data)
    {
        try {
            DB::beginTransaction();

            $this->service->name = $data['name'];
            $this->service->desc = $data['desc'];
            $this->service->type = $data['type'];
            $this->service->status = $data['status'];
            $this->service->time_medium = $data['time'];
            $this->service->price = $data['price'];
            $id = $this->service->insert();

            DB::commit();
            return $id;

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $this->service->id = $id;
        $this->service->name = $data['name'];
        $this->service->desc = $data['desc'];
        $this->service->type = $data['type'];
        $this->service->status = $data['status'];
        $this->service->time_medium = $data['time'];
        $this->service->price = $data['price'];

        return $this->service->edit();
    }


    /**
     * @param int $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->service->id = $id;
        return $this->service->remove();
    }


}