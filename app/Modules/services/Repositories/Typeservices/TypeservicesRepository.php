<?php

/**
 * Created by PhpStorm.
 * User: alex
 * Date: 11/02/18
 * Time: 15:16
 */

namespace Servimotos\Modules\services\Repositories\Typeservices;

use Servimotos\Modules\services\Models\TypeService;
use Servimotos\Modules\services\Repositories\Typeservices\TypeservicesRepositoryInterface;

class TypeservicesRepository implements TypeservicesRepositoryInterface
{
    private $typeService;

    /**
     * TypeservicesRepository constructor.
     * @param TypeService $typeService
     */
    public function __construct(TypeService $typeService)
    {
        $this->typeService = $typeService;
    }



    /**
     * @return mixed
     */
    public function all()
    {
        return $this->typeService->index();
    }



    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $this->typeService->id = $id;
        return $this->typeService->show();
    }

    /**
     * @param array $fields
     * @param array $wheres
     * @param bool $first
     * @return mixed
     */
    public function findBy($fields = array(), $wheres = array(), $first)
    {
        return TypeService::findBy($fields, $wheres, $first);
    }



    /**
     * @param array $data
     * @return mixed
     */
    public function create($data)
    {
        $this->typeService->name = $data['name'];
        $this->typeService->desc = $data['desc'];

        return $this->typeService->insert();
    }

    /**
     * @param int $id
     * @param array $data
     * @return mixed
     */
    public function update($id, $data)
    {
        $this->typeService->id = $id;
        $this->typeService->name = $data['name'];
        $this->typeService->name = $data['name'];

        return $this->typeService->edit();
    }



    /**
     * @param int $id
     * @return mixed
     */
    public function delete($id)
    {
        $this->typeService->id = $id;
        return $this->typeService->remove();
    }


}