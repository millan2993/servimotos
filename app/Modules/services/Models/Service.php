<?php

namespace Servimotos\Modules\services\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

/**
 * Class Service
 * @package Servimotos\Modules\services\Models
 *
 * @property int    id
 * @property string name
 * @property string desc
 * @property int    type
 * @property bool   status
 * @property string time_medium
 * @property Carbon updated_at
 * @property Carbon created_at
 * @property float  price
 */
class Service extends Model
{
    protected $fillable = [
        'name', 'desc', 'type', 'status', 'time_medium'
    ];


    /**
     * Obtiene todos los servicios
     *
     * @return mixed
     */
    public function index()
    {
        try {
            return DB::table('services')
                ->select('id', 'name', 'desc', 'type', 'status')
                ->get();
        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * @param string|array $fields
     * @param array     $wheres
     * @param boolean   $first
     * @return Collection|\stdClass
     */
    public static function findBy($fields, $wheres = array(), $first = false)
    {
        try {
            $roles = DB::table('services')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $roles->first() : $roles->get();

        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * @return \stdClass
     */
    public function show()
    {
        try {
            return DB::table('services')
                ->select('services.id', 'services.name', 'services.desc', 'services.type as id_type', 'typeservices.name as type', 'services.status',
                    'services.created_at', 'services.updated_at', 'services.price',
                    DB::raw('DATE_FORMAT(services.time_medium, "%H:%i") as time_medium')
                    )
                ->leftjoin('typeservices', 'typeservices.id', '=', 'services.type')
                ->where('services.id', '=', $this->id)
                ->first();

        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * Crear un servicio
     *
     * @return int
     */
    public function insert()
    {
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();

        DB::beginTransaction();
        try {
            $this->id = DB::table('services')->insertGetId([
                'name'  => $this->name,
                'desc'  => $this->desc,
                'type'  => $this->type,
                'status'  => $this->status,
                'time_medium'  => $this->time_medium,
                'created_at'  => $this->created_at,
                'updated_at'  => $this->updated_at,
                'price'     => $this->price
            ]);

            DB::commit();
            return $this->id;

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Editar un servicio
     *
     * @return mixed
     */
    public function edit()
    {
        $this->updated_at = Carbon::now();

        try {
            DB::beginTransaction();

            $data = [
                'name' => $this->name,
                'desc' => $this->desc,
                'type' => $this->type,
                'status' => $this->status,
                'time_medium'  => $this->time_medium,
                'updated_at' => $this->updated_at,
                'price'     => $this->price
            ];

            $updated = DB::table('services')
                ->where('id', '=', $this->id)
                ->update($data);

            DB::commit();

            return $updated;
        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Eliminar un servicio
     *
     * @return mixed
     */
    public function remove()
    {
        DB::beginTransaction();
        try {
            /*
            DB::table('documentacion')
                ->where('id_service', '=', $this->id)
                ->delete();
            */

            $deleted = DB::table('services')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

            return $deleted;
        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


}
