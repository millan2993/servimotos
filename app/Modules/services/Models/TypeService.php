<?php

namespace Servimotos\Modules\services\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

/**
 * Class Service
 * @package Servimotos\Modules\services\Models
 *
 * @property int    id
 * @property string name
 * @property string desc
 * @property Carbon updated_at
 * @property Carbon created_at
 */
class TypeService extends Model
{
    protected $fillable = [
        'name', 'desc'
    ];


    /**
     * Obtiene todos los tipos de servicios
     *
     * @return mixed
     */
    public function index()
    {
        try {
            return DB::table('typeservices')
                ->select('id', 'name', 'desc')
                ->get();
        } catch (QueryException $e) {
            throw $e;
        }
    }



    /**
     * @param string|array $fields
     * @param array     $wheres
     * @param boolean   $first
     * @return Collection|\stdClass
     */
    public static function findBy($fields, $wheres = array(), $first = false)
    {
        try {
            $roles = DB::table('typeservices')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $roles->first() : $roles->get();

        } catch (QueryException $e) {
            return array();
        }
    }




    /**
     * @return \stdClass
     */
    public function show()
    {
        try {
            return DB::table('typeservices')
                ->select('id', 'name', 'desc', 'created_at', 'updated_at')
                ->where('id', '=', $this->id)
                ->first();

        } catch (QueryException $e) {
            throw $e;
        }
    }



    /**
     * Crear un tipo de servicio
     *
     * @return int
     */
    public function insert()
    {
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();

        DB::beginTransaction();
        try {
            $this->id = DB::table('typeservices')->insertGetId([
                'name'  => $this->name,
                'desc'  => $this->desc,
                'created_at'  => $this->created_at,
                'updated_at'  => $this->updated_at,
            ]);

            DB::commit();
            return $this->id;

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }



    /**
     * Editar un tipo de servicio
     *
     * @return mixed
     */
    public function edit()
    {
        $this->updated_at = Carbon::now();

        try {
            DB::beginTransaction();

            $data = [
                'name' => $this->name,
                'desc' => $this->desc,
                'updated_at' => $this->updated_at
            ];

            $updated = DB::table('typeservices')
                ->where('id', '=', $this->id)
                ->update($data);

            DB::commit();

            return $updated;
        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }



    /**
     * Eliminar un tipo de servicio
     *
     * @return mixed
     */
    public function remove()
    {
        DB::beginTransaction();
        try {
            $deleted = DB::table('typeservices')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

            return $deleted;
        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


}
