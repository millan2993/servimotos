@extends('layouts.account')

@section('external_files_header')
    @parent

    <!-- Toastr style -->
    <link href="{{ asset('css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">

@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <button id="btn-new-typeservice" class="btn btn-primary btn-sm">Crear Nuevo</button>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table id="services" class="table table-striped table-bordered table-hover" >
                                        <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Descripción</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            @foreach($typeServices as $typeService)
                                            <tr id="row-{{ $typeService->id }}" class="animated">
                                                <td class="col-md-3">{{ $typeService->name }}</td>
                                                <td>
                                                    <p>{{ $typeService->desc }}</p>
                                                </td>
                                                <td class="col-md-1 text-navy">
                                                    <i class="fa fa-edit iHandLink" title="Editar" onclick="editTypeService(this)"></i>
                                                    <i class="fa fa-trash-o iHandLink" title="Eliminar" onclick="deleteTypeService(this)"></i>
                                                </td>
                                            </tr>
                                            @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>


@section('external_files_footer')
    @parent

    <!-- Toastr -->
    <script src=" {{ asset('js/plugins/toastr/toastr.min.js') }}"></script>

@endsection

@endsection
