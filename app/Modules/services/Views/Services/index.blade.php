@extends('layouts.account')

@section('external_files_header')
    @parent

    <link href={{ asset('css/plugins/dataTables/datatables.min.css') }} rel="stylesheet">
@endsection


@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ url('account/services/create') }}" class="btn btn-primary btn-sm">Crear Nuevo</a>
            </div>

            <div class="panel-body">

                <div class="row">
                    <div class="col-lg-12">
                        <div class="ibox float-e-margins">
                            <div class="ibox-content">

                                <div class="table-responsive">
                                    <table id="services" class="table table-striped table-bordered table-hover" >
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th class="hidden-xs">Descripción</th>
                                            <th>Estado</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($services as $service)
                                        <tr>
                                            <td class="col-md-2">{{ $service->name }}</td>
                                            <td class="hidden-xs">
                                                <p>{{ $service->desc }}</p>
                                            </td>
                                            <td>
                                                <label class="badge  {{ ($service->status) ? 'badge-primary' : 'badge-danger' }}">{{ ($service->status) ? 'Activo' : 'Inactivo' }}</label>
                                            </td>
                                            <td class="text-navy">
                                                <a href="{{ url('account/services/show/'.$service->id) }}" class="btn btn-primary btn-xs">Ver más</a>
                                            </td>
                                        </tr>
                                        @endforeach

                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>
</div>



@section('external_files_footer')
    @parent

    <script src= {{ asset('js/plugins/dataTables/datatables.min.js') }}></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            createTable('#services');
        });

    </script>

@endsection

@endsection
