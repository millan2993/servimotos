@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection


@section('content')

<form role="form">
    {{ csrf_field() }}

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a class="btn btn-primary btn-sm" href="{{ url('account/services/edit/'.$service->id) }}">Editar</a>
            <a class="btn btn-primary btn-sm" href="{{ url('account/services/destroy/'.$service->id) }}">Eliminar</a>

            <div class="pull-right">
                <a class="btn btn-primary btn-sm" href="{{ url('account/services/create') }}">Crear Nuevo</a>
            </div>
        </div>

        <div class="ibox-content">

            <div class="row">
                <div class="col-md-12">

                    {{-- name --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Nombre *</label>
                            <input id="name" name="name" type="text" class="form-control" disabled
                                   value="{{ $service->name }}">
                        </div>
                    </div>


                    {{-- status --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <div>
                                <label class="i-checks col-md-6">
                                    <input name="status" type="radio" disabled {{ $service->status ? 'checked' : '' }} > Activo
                                </label>
                                <label class="i-checks col-md-6">
                                    <input name="status" type="radio" disabled {{ !$service->status ? 'checked' : '' }}> Inactivo
                                </label>
                            </div>
                        </div>
                    </div>


                    {{-- time --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label">Tiempo promedio</label>
                            <input id="time" name="time" type="text" class="form-control" disabled
                                   value="{{ $service->time_medium }}">
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    {{-- type --}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="role" class="control-label col-xs-10">Tipo</label>

                            <select id="type" name="type" class="form-control" disabled>
                                <option></option>
                                <option selected>{{ $service->type }}</option>
                            </select>
                        </div>
                    </div>

                    {{--price--}}
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Precio</label>
                            <input id="price" name="price" type="text" class="form-control" disabled
                                   value="{{ $service->price or old('price') }}">
                        </div>
                    </div>

                    {{-- desc --}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('desc')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Descripción</label>
                            <textarea name="desc" id="desc" class="form-control" cols="50"
                                      rows="2" disabled>{{ $service->desc }}</textarea>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</form>





@section('external_files_footer')
    @parent

    <!-- iCheck -->
    <script src=" {{ asset('js/plugins/iCheck/icheck.min.js') }} "></script>

    <!-- Select2 -->
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>


    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $("#type").select2({
                placeholder: "Seleccione un tipo",
                allowClear: true
            });
        });
    </script>
@endsection


@endsection