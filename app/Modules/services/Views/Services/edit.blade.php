@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection


@section('content')

<form id="edit" role="form" method="post">
    {{ csrf_field() }}

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <button class="btn btn-primary btn-sm" onclick="$('#edit').submit();">Guardar cambios</button>
        </div>
        <div class="ibox-content">

            <div class="row">
                <div class="col-md-12">

                    {{-- name --}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('name')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Nombre *</label>
                            <input id="name" name="name" type="text" class="form-control"
                                   value="{{ $service->name }}">

                            @include('layouts.components.error_input', ['input' => 'name'])
                        </div>
                    </div>


                    {{-- status --}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('status')) ? 'has-error' : '' }}">
                            <label class="control-label">Estado</label>
                            <div>
                                <label class="i-checks col-md-6">
                                    <input name="status" type="radio" value="1" {{ $service->status ? 'checked' : '' }} > Activo
                                </label>
                                <label class="i-checks col-md-6">
                                    <input name="status" type="radio" value="0" {{ !$service->status ? 'checked' : '' }}> Inactivo
                                </label>
                            </div>

                            @include('layouts.components.error_input', ['input' => 'status'])
                        </div>
                    </div>


                    {{-- time --}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('time')) ? 'has-error' : '' }}">
                            <label class="control-label">Tiempo promedio</label>
                            <input id="time" name="time" type="text" class="form-control" placeholder="00:00"
                                   value="{{ $service->time_medium }}">
                            @include('layouts.components.error_input', ['input' => 'time'])
                        </div>
                    </div>

                </div>
            </div>


            <div class="row">
                <div class="col-md-12">

                    {{-- type --}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('type')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">Tipo</label>

                            <select id="type" name="type" class="form-control">
                                <option></option>

                                @foreach($typeServices as $typeService)
                                    <option value="{{ $typeService->id }}"
                                            {{ (old('type', $service->id_type) == $typeService->id) ? 'selected' : '' }} >
                                            {{ $typeService->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'type'])
                        </div>
                    </div>


                    {{--price--}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('price')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Precio</label>
                            <input id="price" name="price" type="text" class="form-control"
                                   value="{{ $service->price or old('price') }}">

                            @include('layouts.components.error_input', ['input' => 'price'])
                        </div>
                    </div>

                    {{-- desc --}}
                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('desc')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Descripción</label>
                            <textarea name="desc" id="desc" class="form-control" cols="50"
                                      rows="2">{{ $service->desc }}</textarea>
                            @include('layouts.components.error_input', ['input' => 'desc'])
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

</form>


@section('external_files_footer')
    @parent

    <!-- iCheck -->
    <script src=" {{ asset('js/plugins/iCheck/icheck.min.js') }} "></script>

    <!-- Select2 -->
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>


    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });

            $("#type").select2({
                placeholder: "Seleccione un tipo",
                allowClear: true
            });
        });
    </script>
@endsection


@endsection