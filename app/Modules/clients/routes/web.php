<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/01/18
 * Time: 4:16
 */


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {


    /*
     * employees
     */
    Route::group(['prefix' => '/clients' ], function () {


        Route::get('/', 'ClientsController@index');

        Route::get('/show/{id}', 'ClientsController@show');

        Route::get('/create', 'ClientsController@create');
        Route::post('/create', 'ClientsController@store');

        Route::get('/destroy/{id}', 'ClientsController@destroy');

        Route::get('/edit/{id}', 'ClientsController@edit');
        Route::post('/edit/{id}', 'ClientsController@update');


        Route::post('/loadToSelect/', 'ClientsController@loadToSelect');
    });



});




