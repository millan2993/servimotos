@extends('layouts.account')

@section('external_files_header')
    @parent

    <link href={{ asset('css/plugins/dataTables/datatables.min.css') }} rel="stylesheet">

@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ url('account/clients/create') }}" class="btn btn-primary btn-xs">Crear Nuevo</a>
                </div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">

                                <div class="ibox-content">

                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th class="hidden-xs">Teléfono</th>
                                                <th>Celular</th>
                                                <th class="hidden-xs hidden-sm">correo</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($clients as $client)
                                                <tr>
                                                    <td class="col-md-3">{{ $client->first_name }}</td>
                                                    <td class="col-md-2 hidden-xs">{{ $client->phone }}</td>
                                                    <td class="col-md-2">{{ $client->cellphone }}</td>
                                                    <td class="col-md-3 hidden-xs hidden-sm">{{ $client->email }}</td>
                                                    <td class="text-navy col-md-3">
                                                        <a href="{{ url('account/clients/show/'.$client->id) }}" class="btn btn-primary btn-xs">Ver</a>
                                                        <a href="{{ url('account/clients/edit/'.$client->id) }}" class="btn btn-primary btn-xs hidden-xs">Editar</a>
                                                        <a href="{{ url('account/clients/destroy/'.$client->id) }}" class="btn btn-primary btn-xs hidden-xs">Eliminar</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>

@section('external_files_footer')
    @parent

    <script src= {{ asset('js/plugins/dataTables/datatables.min.js') }}></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Clientes'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ]

            });


        });



    </script>

@endsection

@endsection
