<?php

namespace Servimotos\Modules\clients\models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

/**
 * @property int id
 * @property string first_name
 * @property string last_name
 * @property int type_document
 * @property int document
 * @property string email
 * @property string phone
 * @property string cellphone
 * @property int sex
 * @property int id_address
 * @property string|Carbon updated_at
 * @property string|Carbon created_at
 */
class Clients extends Model
{

    protected $fillable = [
        'id', 'type_document', 'document', 'first_name', 'last_name', 'phone', 'cellphone', 'email', 'id_address'
    ];


    /*
     * Crear un nuevo cliente
     */
    public function insert()
    {
        DB::beginTransaction();
        try {
            $this->id = DB::table('clients')->insertGetId([
                'first_name' => $this->first_name,
                'last_name'  => $this->last_name,
                'sex'        => $this->sex,
                'type_document' => $this->type_document,
                'document'   => $this->document,
                'email'      => $this->email,
                'phone'      => $this->phone,
                'cellphone'  => $this->cellphone,
                'id_address' => $this->id_address,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            DB::commit();
            return $this->id;

        } catch (QueryException $e) {
            logger()->error('Error al crear un cliente', [ $e->getMessage()] );
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Obtener clientes según condición
     *
     * @param string|array $fields
     * @param array $wheres
     * @param boolean $first
     * @return mixed
     *
     * @author Alex Millán
     * @see 29/04/2018
     */
    public static function findBy($fields, array $wheres = array(), bool $first = false)
    {
        try {
            $clients = DB::table('clients')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $clients->first() : $clients->get();

        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * Obtener clientes según condición para los select de la aplicación
     *
     * @param array $wheres
     * @return Collection
     *
     * @author Alex Millán
     * @see 29/04/2018
     */
    public function select(array $wheres = array())
    {
        try {
            return DB::table('clients')
                ->select('id',
                    DB::raw('CONCAT(first_name, " ", last_name) as name')
                )
                ->where($wheres)
                ->get();
        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * Validar si un cliente tiene cuenta de usuario
     *
     * @return boolean
     */
    public function hasUser()
    {
        try {
            return DB::table('users')
                ->where('id_client', $this->id)
                ->exists();

        } catch (QueryException $e) {
            throw $e;
        }
    }



    /**
     * Actualizar el correo del usuario asociado
     *
     * @return void
     */
    public function updateEmailUser()
    {
        try {
            DB::table('users')
                ->where('id_client', $this->id)
                ->update([
                    'email' => $this->email
                ]);

        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * Actualizar la información del cliente
     */
    public function edit()
    {
        $this->updated_at = Carbon::now();

        try {
            DB::beginTransaction();

            $data = [
                'first_name' => $this->first_name,
                'last_name'  => $this->last_name,
                'sex'        => $this->sex,
                'type_document' => $this->type_document,
                'document'   => $this->document,
                'email'      => $this->email,
                'phone'      => $this->phone,
                'cellphone'  => $this->cellphone,
                'id_address' => $this->id_address,
                'updated_at' => $this->updated_at
            ];

            DB::table('clients')
                ->where('id', '=', $this->id)
                ->update($data);

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }



    /**
     * Eliminar un cliente
     *
     * @return void
     */
    public function remove()
    {
        try {
            DB::beginTransaction();

            DB::table('clients')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Eliminar los vehiculos asociados al cliente
     */
    public function removeVehicles()
    {
        try {
            DB::beginTransaction();

            DB::table('vehicles')
                ->where('id_client', $this->id)
                ->delete();

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Verificar si un cliente tiene ordenes asociadas
     *
     * @return mixed
     */
    public function hasAssociatedOrders()
    {
        return DB::table('orders')
            ->where('id_client', $this->id)
            ->exists();
    }


    public function show()
    {
        try {
            return DB::table('clients AS c')
                ->select( 'c.id', 'c.first_name', 'c.last_name', 'c.type_document as id_type_document', 'c.document', 'c.sex', 'c.email',
                    'c.phone', 'c.cellphone', 'c.created_at', 'c.updated_at', 'addr.primary as address',
                    'addr.id_city', 'cities.name as city',
                    'cities.id_state', 'states.name as state',
                    'states.id_country', 'countries.name as country' ,
                    DB::raw('CASE c.type_document 
                                WHEN 1 THEN "Cédula" 
                                WHEN 2 THEN "Documento de identidad" 
                                WHEN 3 THEN "Cédula extranjera" 
                            END AS type_document')
                )
                ->leftjoin('address as addr', 'addr.id', '=', 'c.id_address')
                ->leftjoin('cities', 'cities.id', '=', 'addr.id_city' )
                ->leftjoin('states', 'states.id', '=', 'cities.id_state' )
                ->leftjoin('countries', 'countries.id', '=', 'states.id_country' )
                ->where('c.id', '=', $this->id)
                ->first();

        } catch (QueryException $e) {
            throw $e;
        }
    }



}
