<?php

namespace Servimotos\Modules\clients\Policies;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\clients\models\Clients;

use Illuminate\Auth\Access\HandlesAuthorization;

class ClientsPolicy
{
    use HandlesAuthorization;



    /**
     * Si es administrador tiene todos los permisos
     *
     * @param $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isAdministrator() && $ability != 'delete') {
            return true;
        }
    }


    /**
     * Permiso para ver todos los usuarios
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAll(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_todos_clientes');
    }


    /**
     * Determine whether the user can view the client.
     *
     * @param  User  $user
     * @param  int $id_client
     * @return mixed
     */
    public function view(User $user, int $id_client)
    {
        if ($user->id_client == $id_client) {
            return true;
        }

        if ($this->viewAll($user)) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'ver_clientes');
    }

    /**
     * Determine whether the user can create clients.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'crear_clientes');
    }

    /**
     * Determine whether the user can update the client.
     *
     * @param  User  $user
     * @param  int $id_client
     * @return mixed
     */
    public function update(User $user, int $id_client)
    {
        if ($user->id_client == $id_client) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'editar_clientes');
    }

    /**
     * Determine whether the user can delete the client.
     *
     * @param  User  $user
     * @param  int $id_client
     * @return mixed
     */
    public function delete(User $user, int $id_client)
    {
        if ($user->id_client == $id_client) {
            return false;
        }

        if ($user->isAdministrator()) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'eliminar_clientes');
    }
}
