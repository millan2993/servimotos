<?php

namespace Servimotos\Modules\clients\Providers;

use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

use Servimotos\Modules\clients\models\Clients;
use Servimotos\Modules\clients\Policies\ClientsPolicy;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Enlace del modelo con las politicas (permisos)
     *
     * @var array
     */
    protected $policies = [
        Clients::class => ClientsPolicy::class
    ];

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
