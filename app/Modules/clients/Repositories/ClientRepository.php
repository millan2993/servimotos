<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 29/04/18
 * Time: 11:31
 */

namespace Servimotos\Modules\clients\Repositories;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\acl\Repositories\Users\UserRepository;
use Servimotos\Modules\clients\models\Clients;
use Servimotos\Region;

class ClientRepository
{
    /**
     * Model Order
     *
     * @var Clients
     */
    public $client;


    /**
     * OrderRepository constructor.
     *
     * @param Clients $client
     */
    public function __construct(Clients $client)
    {
        $this->client = $client;
    }


    /**
     * Obtener todas las ordenes de compra
     *
     * @return mixed
     */
    public function all()
    {
        return $this->client->all();
    }

    /**
     * @return object
     */
    public function show()
    {
        try {
            return $this->client->show();

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $fields
     * @param array $wheres
     * @param bool $first
     * @return mixed
     */
    public function findBy($fields = array(), $wheres = array(), $first)
    {
        return $this->client->findBy($fields, $wheres, $first);
    }

    /**
     * Crear un nuevo cliente
     *
     * @return int
     */
    public function create()
    {
        try {
            DB::beginTransaction();

            if (!empty($this->client->city) || !empty($this->client->address)) {
                $this->client->id_address = Region::saveAddress($this->client->city, $this->client->address);
            }

            $clientExists = self::byDocument($this->client->document);
            if (!empty($clientExists)) {
                Session::push('error', 'Ya existe un cliente registrado con el mismo documento de identidad.');
                throw new Exception();
            }

            $inserted = $this->client->insert();

            DB::commit();
            return $inserted;

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }



    /**
     * Actualizar el cliente
     */
    public function update()
    {
        try {
            DB::beginTransaction();

            if (!empty($this->client->city) || !empty($this->client->address)) {
                $this->client->id_address = Region::saveAddress($this->client->city, $this->client->address);
            }

            if (!empty($this->client->email)) {
                $this->updateEmailUser();
            }

            $this->client->edit();

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Eliminar un cliente
     */
    public function delete()
    {
        try {
            DB::beginTransaction();

            $hasHistorical = $this->hasHistorical();
            if ($hasHistorical) {
                Session::push('error', 'El cliente no puede ser eliminado porque tiene información histórica en la aplicación.');
                throw new Exception();
            }

            $client = Clients::findBy( ['id_address'], ['id' => $this->client->id], true);

            $this->client->removeVehicles();
            $this->client->remove();

//            eliminar la dirección
            if (!empty($client->id_address)) {
                Region::deleteAddress($client->id_address);
            }

//           eliminar el usuario
            if (self::hasUser($this->client->id)) {
                UserRepository::deleteByEntity($this->client);
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * @return mixed
     */
    public static function select(array $wheres = array())
    {
        return (new Clients())->select($wheres);
    }


    /**
     * Actualizar el correo de la cuenta de usuario asociada
     */
    private function updateEmailUser()
    {
        try {
            if (self::hasUser($this->client->id)) {
                $this->client->updateEmailUser();
            }
        } catch (Exception $e) {
            Session::push('error', 'Ocurrió un error al actualizar el correo de la cuenta de usuario asociada.');
            throw $e;
        }
    }


    /**
     * Validar si un cliente tiene cuenta de usuario
     *
     * @param int $id
     *
     * @return boolean
     */
    private static function hasUser($id)
    {
        try {
            $client = new Clients();
            $client->id = $id;
            return $client->hasUser();

        } catch (Exception $e) {
            logger()->log('Ocurrió un error al validar si un empleado tiene usuarios.', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }


    /**
     * Obtener el cliente por documento de identidad
     *
     * @param  int $document
     * @return array
     */
    private static function byDocument($document)
    {
        return Clients::findBy(['id'], ['document' => $document], true);
    }


    /**
     * Validar si un cliente tiene información histórica (orders)
     *
     * @return boolean
     */
    private function hasHistorical()
    {
        try {
//            orderdes asociadas
            $hasHistorical = $this->client->hasAssociatedOrders();
            if ($hasHistorical) {
                return true;
            }

            return false;

        } catch (Exception $e) {
            return true;
        }
    }


}