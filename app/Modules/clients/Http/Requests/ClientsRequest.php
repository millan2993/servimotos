<?php

namespace Servimotos\Modules\clients\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClientsRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'first_name'     => 'required|min:3|max:45',
            'last_name'      => 'required|min:3|max:45',
            'sex'            => 'required|integer|max:255',
            'type_document'  => 'required|integer|max:255',
            'document'       => 'required|integer|digits_between:5,15',
            'email'          => 'nullable|email',
            'cellphone'      => 'nullable|integer|min:7|min:15',
            'phone'          => 'nullable|integer|min:7|min:15',
        ];

        if (!empty($this->request->get('address')) ) {
            $rules = ['city' => 'required'];
        }

        return $rules;
    }


    /**
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }
}
