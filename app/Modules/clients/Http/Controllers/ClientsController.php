<?php

namespace Servimotos\Modules\clients\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\clients\Http\Requests\ClientsRequest;
use Servimotos\Modules\clients\models\Clients;
use Servimotos\Modules\clients\Repositories\ClientRepository;
use Servimotos\Region;

class ClientsController extends Controller
{

    /**
     * @var ClientRepository ClientRepository
     */
    private $clientRepo;

    /**
     * ClientsController constructor.
     */
    public function __construct()
    {
        $this->clientRepo = new ClientRepository(new Clients());
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAll', Clients::class);

        // Traer de la base de datos
        $Client = new Clients();
        $clients = $Client->all();

        return view('clients::clients.index', [
            'clients' => $clients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Clients::class);

        $countries = Region::getCountries();
        $states = Region::getStates(82);
        $cities = Region::getCities(1700);

        return view('clients::clients.create', [
            'countries' => $countries,
            'states'    => $states,
            'cities'    => $cities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|ClientsRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClientsRequest $request)
    {
        $this->authorize('create', Clients::class);

        $data = $request->all();

        try {
            $this->clientRepo->client->first_name     = $data['first_name'];
            $this->clientRepo->client->last_name      = $data['last_name'];
            $this->clientRepo->client->type_document  = $data['type_document'];
            $this->clientRepo->client->document       = $data['document'];
            $this->clientRepo->client->email          = $data['email'];
            $this->clientRepo->client->phone          = $data['phone'];
            $this->clientRepo->client->cellphone      = $data['cellphone'];
            $this->clientRepo->client->sex            = $data['sex'];

            $this->clientRepo->client->address  = $request->get('address');
            $this->clientRepo->client->city     = $request->get('city');

            $id = $this->clientRepo->create();

            return redirect('account/clients/show/'.$id)
                ->with('success', 'El cliente fue creado satisfactoriamente.');

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al crear el cliente';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);

            return back()->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', [Clients::class, $id]);

        try {
            $this->clientRepo->client->id = $id;
            $client = $this->clientRepo->show();

            if (empty($client)) {
                Session::push('El cliente indicado no existe.');
                throw new Exception();
            }

            return view('clients::clients.show', [
                'client' => $client
            ]);

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al mostrar el cliente.';
            logger()->error($msg, [ $e->getMessage(), $e->getTraceAsString() ]);

            return back()
                ->with('error', $msg);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', [Clients::class, $id]);

        try {
            $this->clientRepo->client->id = $id;
            $client = $this->clientRepo->show();

            if (empty($client)) {
                Session::push('info', 'El cliente indicado no existe.');
            }

            $countries = Region::getCountries();
            $states = Region::getStates(82);
            $cities = Region::getCities(1700);

            return view('clients::clients.edit', [
                'client'    => $client,
                'countries' => $countries,
                'states'    => $states,
                'cities'    => $cities
            ]);

        } catch (Exception $e) {
            Session::push('error', 'Error al editar el cliente.');
            back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|ClientsRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClientsRequest $request, $id)
    {
        $this->authorize('update', [Clients::class, $id ]);

        $data = $request->all();

        try {

            $this->clientRepo->client->id = $id;
            $this->clientRepo->client->first_name = $data['first_name'];
            $this->clientRepo->client->last_name = $data['last_name'];
            $this->clientRepo->client->type_document = $data['type_document'];
            $this->clientRepo->client->document = $data['document'];
            $this->clientRepo->client->email = $data['email'];
            $this->clientRepo->client->phone = $data['phone'];
            $this->clientRepo->client->cellphone = $data['cellphone'];
            $this->clientRepo->client->sex =    $data['sex'];

            $this->clientRepo->client->address  = $request->get('address');
            $this->clientRepo->client->city     = $request->get('city');

            $this->clientRepo->update();

            return redirect('account/clients/show/'.$id)
                ->with('success', 'El cliente fue actualizado satisfactoriamente.');

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al actualizar el cliente';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);

            return back()->withInput();
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', [Clients::class, $id ]);

        try {
            $this->clientRepo->client->id = $id;
            $this->clientRepo->delete();

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al eliminar el cliente.';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);
            return back();
        }

        return redirect('account/clients')
            ->with('success', 'El cliente fue eliminado satisfactoriamente.');
    }



    /**
     * Cargar los datos para un select
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadToSelect()
    {
        try {
            return response()->json([
                'status' => 'ok',
                'data'   => ClientRepository::select()
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status'  => 'error',
                'data'    => [],
                'message' => 'Error al capturar la actividad en acción',
            ]);
        }
    }

}
