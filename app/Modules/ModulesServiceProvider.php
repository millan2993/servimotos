<?php

namespace Servimotos\modules;

use Illuminate\Support\ServiceProvider;
use Servimotos\Models\Module;

class ModulesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // For each of the registered modules, include their routes and Views
//        $modules = config("modules.Modules");

        $wheres = array(
            ['status', '=', 1]
        );

        $module = new Module();
        $modules = $module->findBy('folder', $wheres, false);

        if (!empty($modules)) {

            foreach ($modules as $module) {

                /*
                    // Load the routes for each of the modules
                    if(file_exists(__DIR__.'/'.$module.'/routes/routes.php')) {
                        include __DIR__.'/'.$module.'/routes/routes.php';
                    }
                */

                // Load the views
                if (is_dir(__DIR__ . '/' . $module->folder . '/Views')) {
                    $this->loadViewsFrom(__DIR__ . '/' . $module->folder . '/Views', $module->folder);
                }


                /*
                 * Load the providers for all modules
                 */
                $dir = app_path() . '/Modules/' . $module->folder . '/Providers';

                if (is_dir($dir)) {
                    $files = scandir($dir);
                    $providers = array_diff($files, ['.', '..']);

                    foreach ($providers as $provider) {
                        $nameFile = str_replace('.php', '', $provider);
                        $this->app->register(config('app.name') . '\Modules\\' . $module->folder . '\Providers\\' . $nameFile);
                    }
                }
            }

        }


    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
