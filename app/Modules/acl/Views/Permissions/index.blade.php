@extends('layouts.account')

@section('external_files_header')
    @parent

    <link href={{ asset('css/plugins/dataTables/datatables.min.css') }} rel="stylesheet">

@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ url('account/permissions/create') }}" class="btn btn-primary btn-xs">Crear Nuevo</a>
                </div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">

                                <div class="ibox-content">

                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th>Estado</th>
                                                <th>Descripción</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($permissions as $permission)
                                                <tr>
                                                    <td class="col-md-2">{{ $permission->name }}</td>
                                                    <td class="col-md-1 text-navy"><i class="fa {{ ($permission->status) ? 'fa-check' : 'fa-times'}}"></i></td>
                                                    <td class="col-md-4">
                                                        <p>{{ $permission->desc }}</p>
                                                    </td>
                                                    <td class="text-navy col-md-3">
                                                        <a href="{{ url('account/permissions/show/'.$permission->id) }}" class="btn btn-primary btn-xs">Ver</a>
                                                        <a href="{{ url('account/permissions/edit/'.$permission->id) }}" class="btn btn-primary btn-xs">Editar</a>
                                                        <a href="{{ url('account/permissions/destroy/'.$permission->id) }}" class="btn btn-primary btn-xs">Eliminar</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                        </div>


                </div>

            </div>
        </div>
    </div>

    @section('external_files_footer')
        @parent

        <script src= {{ asset('js/plugins/dataTables/datatables.min.js') }}></script>

        <!-- Page-Level Scripts -->
            <script>
                $(document).ready(function(){
                    $('.dataTables-example').DataTable({
                        dom: '<"html5buttons"B>lTfgitp',
                        buttons: [
                            {extend: 'excel', title: 'ExampleFile'},
                            {extend: 'pdf', title: 'ExampleFile'},
                            {extend: 'print',
                                customize: function (win){
                                    $(win.document.body).addClass('white-bg');
                                    $(win.document.body).css('font-size', '10px');

                                    $(win.document.body).find('table')
                                            .addClass('compact')
                                            .css('font-size', 'inherit');
                                }
                            }
                        ]

                    });


                });



            </script>

    @endsection

@endsection
