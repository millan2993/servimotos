@extends('layouts.account')

@section('content')


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a href="{{ url('account/permissions/edit/'.$permission->id) }}" class="btn btn-primary btn-xs">Editar</a>
            <a href="{{ url('account/permissions/destroy/'.$permission->id) }}" class="btn btn-primary btn-xs">Eliminar</a>

            <div class="pull-right">
                <a href="{{ url('account/permissions/create') }}" class="btn btn-primary btn-xs">Crear Nuevo</a>
            </div>
        </div>
        <div class="ibox-content">


            <form role="form">


                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="name" class="col-md-10 control-label">Nombre</label>
                            <input id="name" name="name" type="text" disabled
                                   class="form-control" value="{{ $permission->name }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="status" class="col-md-12 control-label">Estado</label>
                            <input id="status" name="status" type="text" disabled
                                   class="form-control" value="{{ ($permission->status) ? 'Activo' : 'Inactivo' }}">
                        </div>
                    </div>


                </div>


                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="type" class="control-label col-xs-10">Tipo</label>

                            <select id="type" name="type" class="form-control" disabled>
                                <option>{{ $permission->type }}</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Valores</label>
                            <input id="name" name="name" type="text" disabled
                                   class="form-control" value="{{ $permission->values }}">
                        </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Fecha creación</label>
                            <input id="created" name="created" type="text" disabled=""
                                   class="form-control" value="{{ $permission->created_at }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Última modificación</label>
                            <input id="updated" name="updated" type="text" disabled=""
                                   class="form-control" value="{{ $permission->updated_at }}">
                        </div>
                    </div>

                </div>



                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="desc" class="col-md-10 control-label">Descripción</label>

                            <textarea name="desc" id="desc" class="form-control" cols="50"
                                      rows="2" disabled>{{ $permission->desc }}</textarea>

                        </div>
                    </div>

                </div>


            </form>


        </div>
    </div>


@endsection