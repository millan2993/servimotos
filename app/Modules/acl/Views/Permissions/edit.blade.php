@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <button class="btn btn-primary btn-xs" type="submit" onclick="$('#permission-edit').submit();">Guardar cambios</button>
        </div>
        <div class="ibox-content">

            @if($permission)
                <form id="permission-edit" role="form" method="post">
                    {{ csrf_field() }}


                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->errores->has('name')) ? 'has-error' : '' }}">
                                <label class="col-md-10 control-label">Nombre *</label>

                                <input id="name" name="name" type="text" class="form-control"
                                       value="{{ $permission->name or old('name') }}">

                                @include('layouts.components.error_input', ['input' => 'name'])
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->errores->has('status')) ? 'has-error' : '' }}">
                                <label class="control-label">Estado</label>

                                <div class="switch">
                                    <div class="onoffswitch">
                                        <input id="status" name="status" type="checkbox"
                                               class="onoffswitch-checkbox" {{ ($permission->status) ? 'checked' : '' }}>
                                        <label class="onoffswitch-label" for="status">
                                            <span class="onoffswitch-inner"></span>
                                            <span class="onoffswitch-switch"></span>
                                        </label>
                                    </div>
                                </div>

                                @include('layouts.components.error_input', ['input' => 'status'])
                            </div>
                        </div>

                    </div>





                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->errores->has('type')) ? 'has-error' : '' }}">
                                <label for="type" class="control-label col-xs-10">Tipo *</label>

                                <select id="type" name="type" class="form-control">
                                    <option></option>
                                    <option value="1" {{ ($permission->type == 'Acceso' ) ? 'selected' : '' }}>Acceso</option>
                                    <option value="2" {{ ($permission->type == 'Parametro') ? 'selected' : '' }}>Parametro</option>
                                </select>

                                @include('layouts.components.error_input', ['input' => 'type'])
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="values" class="col-md-10 control-label">Valores</label>

                                <input id="values" name="values" type="text" class="form-control"
                                       value="{{ $permission->values or old('values') }}">
                            </div>
                        </div>


                    </div>





                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-group {{ ($errors->errores->has('desc')) ? 'has-error' : '' }}">
                                <label for="desc" class="col-md-10 control-label">Descripción *</label>

                                <textarea name="desc" id="desc" class="form-control" cols="50"
                                          rows="2">{{ $permission->desc or old('desc') }}</textarea>

                                @include('layouts.components.error_input', ['input' => 'desc'])
                            </div>
                        </div>

                    </div>



                </form>


            @else
                <h2 class="text-center">El módulo no existe</h2>
            @endif

        </div>
    </div>





@section('external_files_footer')
    @parent

    <!-- Select2 -->
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

    <script>
        $(document).ready(function () {

            $("#module").select2({
                placeholder: "Seleccione un módulo",
                allowClear: false
            });
        });
    </script>

    <script>
        $(document).ready(function () {

            $("#type").select2({
                placeholder: "Seleccione el tipo",
                allowClear: false
            });
        });
    </script>
@endsection





@endsection