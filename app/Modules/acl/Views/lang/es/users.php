<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 19/08/18
 * Time: 10:36 PM
 */

return [
    'employee' => 'empleado',
    'employees' => 'empleados',

    'client'   => 'cliente',
    'clients'   => 'clientes'
];