@extends('layouts.account')

@section('external_files_header')
    @parent
@endsection


@section('content')


    <div class="row">

        <div class="ibox float-e-margins">

            <div class="ibox-title">
                <a class="btn btn-primary btn-sm" href="{{ url('account/users/create') }}">Crear Usuario</a>
            </div>

            <div class="ibox-content">

                <div class="row">
                    @foreach($users as $user)

                        <div class="col-lg-3">
                            <div class="contact-box center-version" >

                                <a href="{{ url('account/users/show', [ $user->id ]) }}" >
                                    <img alt="image" class="img-circle"
                                         src="{{ ( empty($user->avatar) || !Storage::disk('public')->exists($user->avatar) ) ? asset('img/profile_small.jpg') : asset($user->avatar)}}">
                                    <h3 class="m-b-xs"><strong>{{ $user->name }}</strong></h3>
                                    <span class="font-bold"> {{ $user->role }} </span>

                                    <address class="m-t-md">
                                        <p>{{ $user->email }}</p>
                                        <label class="badge {{ ($user->status) ? 'badge-primary' : 'badge-danger' }}">
                                            {{ ($user->status) ? 'Activo' : 'Inactivo' }}
                                        </label>
                                    </address>

                                </a>
{{--

                                <div class="contact-box-footer">
                                    <div class="m-t-xs btn-group">
                                        <a class="btn btn-xs btn-white"><i class="fa fa-phone"></i> Call </a>
                                        <a class="btn btn-xs btn-white"><i class="fa fa-envelope"></i> Email</a>
                                        <a class="btn btn-xs btn-white"><i class="fa fa-user-plus"></i> Follow</a>
                                    </div>
                                </div>
--}}

                            </div>
                        </div>

                    @endforeach

                </div>
            </div>

        </div>

    </div>


@section('external_files_footer')
    @parent
@endsection

@endsection
