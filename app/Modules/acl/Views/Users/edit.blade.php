@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href=" {{ asset('css/plugins/iCheck/custom.css') }} " rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection

@section('content')

    <div class="ibox float-e-margins">

        <div class="ibox-title">
            <button class="btn btn-primary btn-sm" onclick="$('#userEdit').submit();">Guardar</button>

            <div class="pull-right">
                <a class="btn btn-primary btn-sm" href="{{ url('account/users/create') }}">Crear Nuevo</a>
            </div>
        </div>

        <div class="ibox-content">

            <form id="userEdit" role="form" method="post">
                {{ csrf_field() }}

                <div class="row">

                    <div class="profile">
                        <div class="profile-image">
                            <img class="img-circle circle-border center-block" src="{{ asset('img/' . $user->avatar ) }}" >
                        </div>

                        <div class="profile-info">

                            <div class="col-sm-12 {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">@</span>
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Nombre" value="{{ $user->name }}">
                                </div>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
                                <label class="control-label col-xs-10">Estado *</label>

                                <label class="i-checks col-md-6 col-sm-6 col-xs-6">
                                    <input name="status" type="radio" value="1" {{ ($user->status) ? 'checked' : '' }}> Activo
                                </label>
                                <label class="i-checks col-md-6 col-sm-6 col-xs-6">
                                    <input name="status" type="radio" value="0" {{ (!$user->status) ? 'checked' : '' }}> Inactivo
                                </label>
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('role')) ? 'has-error' : '' }}">
                                <label for="role" class="control-label col-xs-10">Rol *</label>

                                <select id="role" name="role" class="form-control">
                                    <option></option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" {{ ($role->id == $user->id_role) ? 'selected' : '' }} >
                                            {{ $role->name }}
                                        </option>
                                    @endforeach
                                </select>

                                @include('layouts.components.error_input', ['input' => 'role'])
                            </div>

                        </div>

                    </div>

                </div>
            </form>

        </div>
    </div>



    {{-- ---------------   ACTUALIZAR CONTRASEÑA  -------------------- --}}
    <div class="row">

        <div class="col-md-6">

            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Actualizar contraseña</h5>
                </div>
                <div class="ibox-content">

                    <form id="updatePass" role="form" method="post" class="form-horizontal">

                        <div class="form-group {{ ($errors->has('current_password')) ? 'has-error' : '' }}">
                            <label class="col-md-5 control-label">Contraseña actual *</label>
                            <div class="col-md-7">
                                <input id="current_password" name="current_password" type="password" class="form-control">
                            </div>
                            @include('layouts.components.error_input', ['input' => 'current_password'])
                        </div>

                        <div class="form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                            <label class="col-md-5 control-label">Nueva Contraseña *</label>
                            <div class="col-md-7">
                                <input id="password" name="password" type="password" class="form-control">
                            </div>
                            @include('layouts.components.error_input', ['input' => 'password'])
                        </div>

                        <div class="form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                            <label class="col-md-5 control-label">Confirmar contraseña *</label>
                            <div class="col-md-7">
                                <input id="password_confirmation" name="password_confirmation" type="password" class="form-control">
                            </div>
                            @include('layouts.components.error_input', ['input' => 'password_confirmation'])
                        </div>

                        <button type="submit" class="btn btn-primary btn-block">Guardar</button>

                    </form>

                </div>
            </div>

        </div>



    </div>


@section('external_files_footer')
    @parent

    <!-- iCheck -->
    <script src=" {{ asset('js/plugins/iCheck/icheck.min.js') }} "></script>

    <!-- Select2 -->
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>


    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });

        $(document).ready(function () {
            $("#role").select2({
                placeholder: "Seleccione un rol",
                allowClear: false
            });
        });
    </script>
@endsection

@endsection
