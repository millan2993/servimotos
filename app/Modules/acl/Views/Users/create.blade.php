@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href=" {{ asset('css/plugins/iCheck/custom.css') }} " rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection


@section('content')


    <div class="ibox float-e-margins">

        <div class="ibox-title">
            <button class="btn btn-primary btn-sm" onclick="$('#userCreate').submit();">Guardar</button>
        </div>

        <div class="ibox-content">

            <form id="userCreate" role="form" method="post">
                {{ csrf_field() }}

                <div class="row">

                    <div class="profile">

                        <div class="profile-image">
                            <img class="img-circle circle-border center-block" src="{{ asset('img/profile_small.jpg') }}" >
                        </div>

                        <div class="profile-info">

                            <div class="col-sm-12 {{ ($errors->has('name')) ? 'has-error' : '' }}">
                                <div class="input-group m-b">
                                    <span class="input-group-addon">@</span>
                                    <input id="name" name="name" type="text" class="form-control" placeholder="Nombre" value="{{ old('name') }}">
                                </div>
                            </div>


                            {{-- status --}}
                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('status')) ? 'has-error' : '' }}">
                                <label class="control-label col-xs-10">Estado *</label>

                                <label class="i-checks col-md-6 col-sm-6 col-xs-6">
                                    <input name="status" type="radio" value="1" {{ old('status') ? 'checked' : '' }} > Activo
                                </label>
                                <label class="i-checks col-md-6 col-sm-6 col-xs-6">
                                    <input name="status" type="radio" value="0" {{ ( old('status') !== null && !old('status') ) ? 'checked' : '' }}>Inactivo
                                </label>
                            </div>


                            {{-- role --}}
                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('role')) ? 'has-error' : '' }}">
                                <label for="role" class="control-label col-xs-10">Rol *</label>
                                <select id="role" name="role" class="form-control">
                                    <option></option>
                                    @foreach($roles as $role)
                                        <option value="{{ $role->id }}" {{ ($role->id == old('role')) ? 'selected' : '' }}>
                                            {{ $role->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @include('layouts.components.error_input', ['input' => 'role'])
                            </div>


                            {{-- type users --}}
                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('typeuser')) ? 'has-error' : '' }}">
                                <label for="typeuser" class="control-label col-xs-10">Tipo de usuario *</label>
                                <select id="typeuser" name="typeuser" class="form-control">
                                    <option></option>
                                    <option value="1">Empleado</option>
                                    <option value="2">Cliente</option>
                                </select>
                                @include('layouts.components.error_input', ['input' => 'typeuser'])
                            </div>

                            {{-- owner--}}
                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('owner')) ? 'has-error' : '' }}">
                                <label for="owner" class="control-label col-xs-10">Propietario *</label>
                                <select id="owner" name="owner" class="form-control"></select>
                                @include('layouts.components.error_input', ['input' => 'owner'])
                            </div>


                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('password')) ? 'has-error' : '' }}">
                                <label class="control-label col-xs-10">Contraseña *</label>
                                <input id="password" name="password" type="password" class="form-control">
                                @include('layouts.components.error_input', ['input' => 'password'])
                            </div>

                            <div class="col-xs-12 col-sm-6 col-md-4 form-group {{ ($errors->has('password_confirmation')) ? 'has-error' : '' }}">
                                <label class="control-label col-xs-10">Confirmar contraseña *</label>
                                <input id="password_confirmation" name="password_confirmation" type="password" class="form-control">
                                @include('layouts.components.error_input', ['input' => 'password_confirmation'])
                            </div>


                        </div>
                    </div>
                </div>


            </form>

        </div>
    </div>



@section('external_files_footer')
    @parent
    <script src=" {{ asset('js/plugins/iCheck/icheck.min.js') }} "></script>
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green'
            });

            $("#role").select2({
                placeholder: "Seleccione un rol",
                allowClear: false
            });

            $("#typeuser").select2({
                placeholder: "Seleccione el tipo de usuario",
                allowClear: false,
                minimumResultsForSearch: -1
            });

            $("#owner").select2({
                placeholder: "Seleccione el propietario de la cuenta",
                allowClear: false
            });

        });
    </script>
@endsection

@endsection
