@extends('layouts.account')

@section('external_files_header')
    @parent
@endsection


@section('content')

    <div class="ibox float-e-margins">

        <div class="ibox-title">
            <a class="btn btn-primary btn-sm" href="{{ url('account/users/edit/'.$user->id) }}">Editar</a>
            <a class="btn btn-primary btn-sm" href="{{ url('account/users/destroy/'.$user->id) }}">Eliminar</a>

            <div class="pull-right">
                <a class="btn btn-primary btn-sm" href="{{ url('account/users/create') }}">Crear Nuevo</a>
            </div>
        </div>

        <div class="ibox-content">

            <form role="form">
                <div class="row">

                    <div class="profile">

                        <div class="profile-image">
                            <img class="img-circle circle-border center-block"
                                 src="{{ ( empty($user->avatar) || !Storage::disk('public')->exists($user->avatar) ) ? asset('img/profile_small.jpg') : asset($user->avatar)}}">
                        </div>

                        <div class="profile-info">
                            <h2 class="profile-title">
                                <a href="{{ url("account/{$user->module}/show", $user->entity) }}">{{ $user->name }}</a>
                            </h2>

                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label">Correo</label>
                                    <input id="email" name="email" type="email" disabled=""
                                           class="form-control" value="{{ $user->email }}">
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="status">Estado</label>
                                    <input id="status" name="status" type="text" disabled="" class="form-control"
                                           value="{{ ($user->status) ? 'Activo' : 'Inactivo' }}">
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="role">Rol</label>
                                    <a class="btn btn-primary btn-block" href="{{ url('account/roles/show/'. $user->id_role) }}">
                                        {{ $user->role }}
                                    </a>
                                </div>
                            </div>

                            @if($user->module)
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label for="type" class="col-md-10 control-label">Tipo</label>
                                        <input id="type" name="type" type="text" class="form-control" value="{{ ucfirst(__('acl::users.'.str_singular($user->module))) }}" disabled>
                                    </div>
                                </div>
                            @endif

                            @if(Auth::user()->hasRole(array('Administrador', 'superadmin'), Auth::user()->id) )
                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label" for="status">Creado</label>
                                        <input id="status" name="status" type="text" disabled=""
                                               class="form-control" value="{{ $user->created_at }}">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <div class="form-group">
                                        <label class="control-label">Actualizado</label>
                                        <input id="email" name="email" type="email" disabled=""
                                               class="form-control" value="{{ $user->updated_at }}">
                                    </div>
                                </div>

                            @endif

                        </div>

                    </div>

                </div>
            </form>


        </div>
    </div>


    <!--
        <div class="row">

            <div class="col-md-6">
                <div class="ibox">
                    <div class="ibox-content">
                        <h3>Mensaje privado</h3>

                        <p class="small">
                            Enviar mensaje privado a {{ $user->name }}
                        </p>

                        <div class="form-group">
                            <label>Asunto</label>
                            <input type="text" class="form-control" placeholder="Asunto del mensaje">
                        </div>
                        <div class="form-group">
                            <label>Mensaje</label>
                            <textarea class="form-control" placeholder="Su mensaje" rows="3"></textarea>
                        </div>
                        <button class="btn btn-primary btn-block">Enviar</button>

                    </div>
                </div>
            </div>
        </div>
    --!>


@section('external_files_footer')
    @parent
@endsection

@endsection
