@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href=" {{ asset('css/plugins/checkbox/checkbox.css') }} " rel="stylesheet">
@endsection

@section('content')


    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <a href="{{ url('account/roles/edit/'.$role->id) }}" class="btn btn-primary btn-sm">Editar</a>
            <a href="{{ url('account/roles/destroy/'.$role->id) }}" class="btn btn-primary btn-sm">Eliminar</a>

            <div class="pull-right">
                <a href="{{ url('account/roles/create') }}" class="btn btn-primary btn-sm">Crear Nuevo</a>
            </div>
        </div>
        <div class="ibox-content">

            <form role="form" class="form-inline">
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-10 control-label">Nombre</label>
                                <div class="col-md-12">
                                    <input id="name" name="name" type="text" disabled=""
                                           class="form-control" value="{{ $role->name }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="col-md-10 control-label">Estado</label>
                                <div class="col-md-12">
                                    <input id="status" name="status" type="text" disabled=""
                                           class="form-control" value="{{ ($role->status) ? 'Activo' : 'Inactivo' }}">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-10 control-label">Descripción</label>
                                <div class="col-md-12">
                                <textarea name="desc" id="desc" class="form-control" disabled="" cols="50"
                                          rows="2">{{  $role->desc }}</textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                </form>


        </div>
    </div>



    {{-- ------------------- PERMISOS  --------------------- --}}
    <div class="row">

        <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Permisos</h5>
                </div>
                <div class="ibox-content">


                    @if( in_array($role->name, ['Administrador', 'superadmin']))
                        <h4 class="text-center">Este rol tiene todos los permisos</h4>
                    @elseif(!$modules)
                        <h4 class="text-center">No tiene permisos asignados</h4>
                    @else


                        <div class="panel-body">

                            @foreach($modules as $module)

                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>{{ ucfirst($module->name) }}</h5>
                                    </div>

                                    <div class="ibox-content">

                                        @foreach($module->components as $component)
                                            <div class="row">
                                                <h3>{{ ucfirst($component->name) }}</h3>

                                                @foreach($component->permissions as $permission)
                                                    <div class="col-md-3 col-sm-6 col-xs-12 table-permissions">
                                                        <div class="checkbox checkbox-inline checkbox-success full-width">
                                                            <input type="checkbox" id="{{ $permission->name }}" name="{{ $permission->name }}"
                                                                    {{ ($permission->status) ? 'checked' : '' }} disabled>
                                                            <label for="{{ $permission->name }}"> {{ $permission->name }} </label>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                            @if(!$loop->last)
                                                <div class="hr-line-dashed"></div>
                                            @endif

                                        @endforeach

                                    </div>
                                </div>
                            @endforeach

                        </div>


                    @endif
                </div>
            </div>

    </div>



@endsection
