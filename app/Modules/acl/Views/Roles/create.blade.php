@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href=" {{ asset('css/plugins/checkbox/checkbox.css') }} " rel="stylesheet">
@endsection


@section('content')


<form id="create" role="form" method="post">
    {{ csrf_field() }}

    <div class="ibox float-e-margins">
        <div class="ibox-title">
            <button class="btn btn-primary btn-sm" onclick="$('#create').submit();">Guardar cambios</button>
        </div>
        <div class="ibox-content">



                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group {{ ($errors->errores->has('name')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Nombre</label>
                            <input id="name" name="name" type="text" class="form-control"
                                   value="{{ old('name') }}">

                            @include('layouts.components.error_input', ['input' => 'name'])
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group {{ $errors->errores->has('status') ? 'has-error' : '' }}">
                            <label class="control-label">Estado</label><br>
                            <div class="radio radio-inline radio-primary">
                                <input type="radio" id="enabled" name="status" value="1">
                                <label for="enabled">Activo</label>
                            </div>
                            <div class="radio radio-inline radio-primary">
                                <input type="radio" id="disabled" name="status" value="0">
                                <label for="disabled">Inactivo</label>
                            </div>
                            @include('layouts.components.error_input', ['input' => 'status'])
                        </div>
                    </div>


                    <div class="col-md-5">
                        <div class="form-group {{ ($errors->errores->has('desc')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Descripción</label>
                            <textarea name="desc" id="desc" class="form-control" cols="50"
                                      rows="2">{{ old('desc') }}</textarea>
                            @include('layouts.components.error_input', ['input' => 'desc'])
                        </div>
                    </div>

                </div>

        </div>
    </div>




    <div class="row">

        <div class="col-md-12">
            <div class="ibox float-e-margins">

                <div class="ibox-title">
                    <h5>Permisos</h5>
                </div>


                <div class="ibox-content">

                    <div class="panel-body">
                        <p>Seleccione los permisos asignados para este rol.</p>

                        @foreach($modules as $module)

                            <div class="ibox float-e-margins">
                                <div class="ibox-title">
                                    <h5>{{ ucfirst($module->name) }}</h5>
                                </div>

                                <div class="ibox-content">

                                    @foreach($module->components as $component)
                                        <div class="row">
                                            <h3>{{ ucfirst($component->name) }}</h3>

                                            @foreach($component->permissions as $permission)
                                                <div class="col-md-3 col-sm-6 col-xs-12 table-permissions">
                                                    <div class="checkbox checkbox-inline checkbox-success full-width">
                                                        <input type="checkbox" id="{{ $permission->name }}" name="permissions[]"
                                                               value="{{ $permission->id }}">
                                                        <label for="{{ $permission->name }}"> {{ $permission->name }} </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                        @if(!$loop->last)
                                            <div class="hr-line-dashed"></div>
                                        @endif

                                    @endforeach

                                </div>
                            </div>
                        @endforeach

                    </div>

                </div>


            </div>
        </div>
    </div>


</form>

@endsection