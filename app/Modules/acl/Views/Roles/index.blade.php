@extends('layouts.account')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ url('account/roles/create') }}" class="btn btn-primary btn-sm">Crear Nuevo</a>
                </div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">
                                <div class="ibox-content">

                                    <table class="table table-hover">
                                        <thead>
                                        <tr>
                                            <th>Nombre</th>
                                            <th>Estado</th>
                                            <th class="hidden-xs">Descripción</th>
                                            <th>Acciones</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @foreach($roles as $role)
                                            <tr>
                                                <td class="col-md-2">{{ $role->name }}</td>
                                                <td class="col-md-1 text-navy"><i class="fa {{ ($role->status) ? 'fa-check' : 'fa-times'}}"></i></td>
                                                <td class="col-md-6 hidden-xs">
                                                    <p>{{ $role->desc }}</p>
                                                </td>
                                                <td class="text-navy col-md-3">
                                                    <a href="{{ url('account/roles/show/'.$role->id) }}" class="btn btn-primary btn-sm">Ver</a>
                                                    <a href="{{ url('account/roles/edit/'.$role->id) }}" class="btn btn-primary btn-sm hidden-xs">Editar</a>
                                                    <a href="{{ url('account/roles/destroy/'.$role->id) }}" class="btn btn-primary btn-sm hidden-xs">Eliminar</a>
                                                </td>
                                            </tr>
                                        @endforeach



                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection
