<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/01/18
 * Time: 4:16
 */


Route::get('users', 'UserController@index');


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {

    /**
     * Permissions
     */
    Route::group([ 'prefix' => '/permissions' ], function () {
        Route::get('/', 'PermissionController@index');
        Route::get('/show/{id}', 'PermissionController@show');

        Route::get('/create', 'PermissionController@create');
        Route::post('/create', 'PermissionController@store');

        Route::get('/edit/{id}', 'PermissionController@edit');
        Route::post('/edit/{id}', 'PermissionController@update');

        Route::get('/destroy/{id}', 'PermissionController@destroy');
    });



    /*
     * Roles
     */
    Route::group(['prefix' => '/roles' ], function () {
        Route::get('/', 'RoleController@index');
        Route::get('/show/{id}', 'roleController@show');

        Route::get('/create', 'roleController@create');
        Route::post('/create', 'roleController@store');

        Route::get('/edit/{id}', 'roleController@edit');
        Route::post('/edit/{id}', 'roleController@update');

        Route::get('/destroy/{id}', 'roleController@destroy');
    });


    /**
     * Users
     */
    Route::group( ['prefix' => '/users' ], function () {
        Route::get('/', 'UserController@index');
        Route::get('/show/{id}', 'UserController@show');

        Route::get('/create', 'UserController@create');
        Route::post('/create', 'UserController@store');

        Route::get('/edit/{id}', 'UserController@edit');
        Route::post('/edit/{id}', 'UserController@update');

        Route::get('/destroy/{id}', 'UserController@destroy');

    });




});




