<?php

namespace Servimotos\Modules\acl\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesProviders extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $base = config('app.name') . '\Modules\acl\Repositories';

        $this->app->bind(
            $base . '\Roles\RoleRepositoryInterface',
            $base . '\Roles\RoleRepository'
        );

        $this->app->bind(
            $base . '\Users\UserRepositoryInterface',
            $base . '\Users\UserRepository');

        $this->app->bind(
            $base . '\Permissions\PermissionRepositoryInterface',
            $base . '\Permissions\PermissionRepository');

    }
}
