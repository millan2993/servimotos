<?php

namespace Servimotos\Modules\acl\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Servimotos\Modules\acl\Models\Permission;
use Servimotos\Modules\acl\Models\Role;
use Servimotos\Modules\acl\Policies\PermissionsPolicy;
use Servimotos\Modules\acl\Policies\RolePolicy;
use Servimotos\Modules\acl\Policies\UserPolicy;
use Servimotos\User;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
//        'Servimotos\Model' => 'Servimotos\Policies\ModelPolicy',

        Permission::class => PermissionsPolicy::class,
        Role::class       => RolePolicy::class,
        User::class       => UserPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->loadTranslationsFrom(__DIR__ . '/../Views/lang', 'acl');
    }
}
