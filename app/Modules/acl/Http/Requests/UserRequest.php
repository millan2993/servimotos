<?php

namespace Servimotos\Modules\acl\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name'      => 'required|min:3',
            'status'    => 'required|boolean',
            'role'      => 'required|integer'
        ];

        if ($this->is('*/users/create')) {
            $rules['password'] = 'required|min:6|confirmed';
            $rules['typeuser'] = 'required|integer';
            $rules['owner']    = 'required|integer';
        }

        return $rules;
    }


    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [

        ];
    }
}
