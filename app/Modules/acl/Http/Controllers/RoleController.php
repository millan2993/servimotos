<?php

namespace Servimotos\Modules\acl\Http\Controllers;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Models\Module;
use Servimotos\Modules\acl\Models\Role;
use Servimotos\Modules\acl\Repositories\Permissions\PermissionRepositoryInterface;
use Servimotos\Modules\acl\Repositories\Roles\RoleRepositoryInterface;
use Servimotos\Repositories\ComponentRepositoryInterface;
use Servimotos\Repositories\ModuleRepositoryInterface;

class roleController extends Controller
{

    private $roleRepo;

    private $permissionRepo;

    private $moduleRepo;

    private $componentRepo;

    /**
     * roleController constructor.
     *
     * @param RoleRepositoryInterface $roleRepository
     * @param PermissionRepositoryInterface $permissionRepository
     * @param ModuleRepositoryInterface $moduleRepository
     * @param ComponentRepositoryInterface $componentRepository
     * @internal param $roleRepo
     *
     * @todo Reconstruir módulo Roles con pattern repository
     */
    public function __construct(
        RoleRepositoryInterface $roleRepository,
        PermissionRepositoryInterface $permissionRepository,
        ModuleRepositoryInterface $moduleRepository,
        ComponentRepositoryInterface $componentRepository
    )
    {
        $this->roleRepo         = $roleRepository;
        $this->permissionRepo   = $permissionRepository;
        $this->moduleRepo       = $moduleRepository;
        $this->componentRepo    = $componentRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Role::class);

        $roles = $this->roleRepo->all();

        // Verificar módulos existentes.
        if (empty($roles)) {
            return redirect('account')->withErrors('Ha ocurrido un error al consultar los roles');
        }

        return view('acl::Roles.index', ['roles' => $roles]);

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Role::class);


//        Consultar modulos activos
        $wheres = [ [ 'status', '=', 1 ] ];
        $modules = $this->moduleRepo->findBy( [ 'id', 'name' ], $wheres);

        // Consultar components
        foreach ($modules as $module) {
            $module->components = $this->moduleRepo->getComponents($module->id, true, true);

            // Consultar los permisos
            foreach ($module->components as $component) {
                $component->permissions  = $this->componentRepo->getPermissions($component->id, 1);
            }
        }


        return view('acl::Roles.create', [
            'modules' => $modules
        ]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Role::class);

        $data = $request->all( [ 'name', 'status', 'desc', 'permissions' ] );

        $validator = $this->validation($data);

        if($validator->fails()) {
            return back()
                ->withErrors( 'alert-danger', 'errorsClass' )
                ->withErrors( ['msg' => 'Por favor, verifique la información ingresada.'] )
                ->withErrors($validator, 'errores')
                ->withInput();
        } else {

            $role = $this->roleRepo->create($data);

            if (empty($role)) {
                return back()
                    ->withErrors('Ocurrio un error al actualizar el rol.')
                    ->withInput();
            }
        }

        return redirect('account/roles/show/'.$role);

    }



    /**
     * Display the specified resource.
     *
     * @param   int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', [Role::class, $id]);

        $role = $this->roleRepo->show($id);

        // Verificar que el modulo exista
        if (empty($role)) {
            return redirect('account/roles')->withErrors('El rol indicado no existe o ha ocurrido un problema al consultarlo.');
        }


//        Consultar modulos activos
        $wheres = [ [ 'status', '=', 1 ] ];
        $modules = $this->moduleRepo->findBy( [ 'id', 'name' ], $wheres);

        // Consultar components
        foreach ($modules as $module) {
            $module->components = $this->moduleRepo->getComponents($module->id, true, true);

            // Consultar los permisos
            foreach ($module->components as $component) {
                $component->permissions  = $this->componentRepo->getPermissionsByRole($component->id, $id);
            }
        }


        return view('acl::Roles.show', [
            'role'  => $role,
            'modules'   => $modules
        ]);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', [Role::class, $id]);

        $role = $this->roleRepo->show($id);

        if (empty($role)) {
            return redirect('account/roles')->withErrors('El rol indicado no existe.');
        }

//        Consultar modulos activos
        $wheres = [ [ 'status', '=', 1 ] ];
        $modules = $this->moduleRepo->findBy( [ 'id', 'name' ], $wheres);

        // Consultar components
        foreach ($modules as $module) {
            $module->components = $this->moduleRepo->getComponents($module->id, true, true);

            // Consultar los permisos
            foreach ($module->components as $component) {
                $component->permissions  = $this->componentRepo->getPermissionsByRole($component->id, $id);
            }
        }


        return view('acl::Roles.edit', [
            'role' => $role,
            'modules' => $modules
        ]);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', [Role::class, $id]);

        $data = $request->all( ['name', 'status', 'desc', 'permissions' ] );

        $validator = $this->validation($data);

        if($validator->fails()) {
            return back()
                ->withErrors( 'Por favor, verifique la información ingresada.' )
                ->withErrors($validator, 'errores')
                ->withInput();
        } else {
            $updated = $this->roleRepo->update($id, $data);

            if (empty($updated)) {
                return back()
                    ->withErrors('Ocurrio un error al actualizar el rol.')
                    ->withInput();
            }
        }

        return redirect('account/roles/show/'.$id);

    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->authorize('delete', [Role::class, $id]);

            $deleted = $this->roleRepo->delete($id);
            if ($deleted == false) {
                return redirect('account/roles')
                    ->with('error', 'Ocurrió un error al eliminar el rol.');
            }

            return redirect('account/roles');

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al eliminar el rol.');
            return back();
        }

    }




    /**
     * @desc    Validar los datos request
     *
     * @param   array   $params
     *
     * @return  mixed>
     *
     * @see     17/10/2017
     * @author  Alex Millán
     */
    private function validation ($params) {
        return Validator::make($params, [
            'name'      =>  'required',
            'desc'      =>  'required',
            'status'    =>  'required'
        ], [
            'name.required' => 'El nombre es obligatorio.',
            'desc.required' => 'La descripción es obligatoria.',
            'status.required'  => 'El estado debe ser obligatorio.'
        ]);
    }

}
