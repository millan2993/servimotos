<?php

namespace Servimotos\Modules\Acl\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\acl\Models\Permission;
use Servimotos\Modules\acl\Repositories\Permissions\PermissionRepositoryInterface;


class PermissionController extends Controller
{

    /**
     * @var PermissionRepositoryInterface
     */
    private $permissionRepo;


    /**
     * permissionController constructor.
     *
     * @param PermissionRepositoryInterface $permissionRepository
     *
     * @internal param $permissionRepo
     */
    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepo = $permissionRepository;
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Permission::class);

        if (Auth::user()->can('index', Permission::class) == false ){
            return redirect('account/permissions')->withErrors('No tienes permisos para acceder al recurso indicado.');
        }

        $permissions = $this->permissionRepo->all();

        return view('acl::Permissions.index', [
            'permissions' => $permissions
        ]);
    }




    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', [Permission::class, $id]);

        $permission = $this->permissionRepo->show($id);

        // Verificar que el permiso exista
        if (empty($permission)) {
            return redirect('account/permissions')->withErrors('El permiso indicado no existe.');
        }
/*
        // Verificar permisos
        if (Auth::user()->can('view', [Permission::class, $id]) == false ){
            return redirect('account/permissions')->withErrors('No tienes permisos para acceder al recurso indicado.');
        }
*/

        return view('acl::Permissions.show', [
            'permission' => $permission
        ]);

    }




    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', [Permission::class, $id]);

        $permission = $this->permissionRepo->show($id);

        // Verificar que el modulo exista
        if (empty($permission)) {
            return redirect('account/permissions')->withErrors('El permiso indicado no existe.');
        }
/*
        // Verificar permisos
        if (Auth::user()->can('view', [Permission::class, $id]) == false ){
            return redirect('account/permissions')->withErrors('No tienes permisos para acceder al recurso indicado.');
        }
*/

        return view('acl::Permissions.edit', [
            'permission' => $permission
        ]);

    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', [Permission::class, $id]);

        // Verificar permisos
        /*if (Auth::user()->can('view', [Permission::class, $id]) == false ){
            return redirect('account/permissions')->withErrors('No tienes permisos para acceder al recurso indicado.');
        }
        */

        $data = $request->except('_token');
        $data['status'] = (!empty($data['status'])) ? true : false;

        $validator = $this->validation($data);

        if($validator->fails()) {
            return back()
                ->withErrors( 'alert-danger', 'errorsClass' )
                ->withErrors( ['msg' => 'Por favor, verifique la información ingresada.'] )
                ->withErrors($validator, 'errores')
                ->withInput();
        } else {
            $save = $this->permissionRepo->update($id, $data);

            if (empty($save)) {
                return back()
                    ->withErrors('Ha ocurrido un error al actualizar el permiso.')
                    ->withInput();
            }
        }


        return redirect('account/permissions/show/'.$id);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Permission::class);

        return view('acl::Permissions.create');
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', Permission::class);

        $data = $request->except('_token');
        $data['status'] = (!empty($data['status'])) ? true : false;

        $validator = $this->validation($data);

        if($validator->fails()) {
            return back()
                ->withErrors( 'alert-danger', 'errorsClass' )
                ->withErrors( ['msg' => 'Por favor, verifique la información ingresada.'] )
                ->withErrors($validator, 'errores')
                ->withInput();

        } else {
            //$module = Permission::insert($data);
            $permission = $this->permissionRepo->create($data);

            if (empty($permission)) {
                return back()
                    ->withErrors('Ha ocurrido un error al crear el permiso.')
                    ->withInput();
            }
        }

        return redirect('account/permissions/show/'.$permission);

    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
/*        // Verificar permisos
        if (Auth::user()->can('delete', [Permission::class, $id]) == false ){
            return redirect('account/permissions')->withErrors('No tienes permisos para acceder al recurso indicado.');
        }*/

        $this->authorize('delete', [Permission::class, $id]);

        $deleted = $this->permissionRepo->delete($id);
        if(empty($deleted)) {
            return redirect('account/permissions')->withErrors('Ocurrió un error al eliminar el permiso.');
        }

        return redirect('account/permissions');
    }





    /**
     * @desc Validar datos antes de guardar
     *
     * @param $data
     *
     *
     * @see     01/10/2017
     * @author  Alex Millán
     */
    private function validation($data)
    {
        return Validator::make($data, [
            'name'      =>  'required',
            'desc'      =>  'required',
            'status'    =>  'required',
            'type'      =>  'required|numeric',
        ], [
            'name.required'    => 'El nombre es obligatorio.',
            'desc.required'    => 'La descripción es obligatoria.',
            'status.required'  => 'El estado debe ser obligatorio.',
            'type.required'    => 'Seleccione el tipo de permiso.',
            'type.numeric'     => 'Tipo seleccionado incorrectamente.'
        ]);

    }

}
