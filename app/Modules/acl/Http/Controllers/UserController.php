<?php

namespace Servimotos\Modules\acl\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\acl\Http\Requests\UserRequest;
use Servimotos\Modules\acl\Repositories\Roles\RoleRepositoryInterface;
use Servimotos\Modules\acl\Repositories\Users\UserRepository;
use Servimotos\User;

class userController extends Controller
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;


    /**
     * @var RoleRepositoryInterface
     */
    private $roleRepo;


    /**
     * userController constructor.
     *
     * @param UserRepository $userRepo
     * @param RoleRepositoryInterface $roleRepository
     *
     * @todo Reconstruir módulo users con pattern repository
     */
    public function __construct(UserRepository $userRepo, RoleRepositoryInterface $roleRepository)
    {
        $this->userRepo = $userRepo;
        $this->roleRepo = $roleRepository;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', User::class);

        $users = $this->userRepo->all();

        return view('acl::Users.index', [
            'users' => $users
        ]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', User::class);

        // Consultar los roles
        $roles = $this->roleRepo->findBy( ['id', 'name'], [ [ 'status', '=', 1 ] ], false);

        return view('acl::Users.create', [
            'roles' => $roles
        ]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param Request|UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        try {
            $this->authorize('create', User::class);

            $this->userRepo->user->type     = $request->get('typeuser');
            $this->userRepo->user->id_owner = $request->get('owner');
            $this->userRepo->user->id_role  = $request->get('role');
            $this->userRepo->user->name     = $request->get('name');
            $this->userRepo->user->password = $request->get('password');
            $this->userRepo->user->status   = $request->get('status');

            $idUser = $this->userRepo->create();
            return redirect('account/users/show/'.$idUser);

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al crear un usuario.', [$e->getMessage()]);
            Session::push('error', 'Ocurrió un error al crear el usuario.');
            return back()->withInput();
        }

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->authorize('view', [User::class, $id]);

        try {
            $user = $this->userRepo->show($id);

            // Verificar que el usuario exista
            if (empty($user)) {
                Session::push('error', 'El usuario indicado no existe.');
                throw new Exception();
            }

            return view('acl::Users.show', [
                'user' => $user
            ]);


        } catch (Exception $e) {
            $msg = 'Ocurrió un error al consultar el usuario.';

            logger()->error($msg, [$e->getMessage()]);
            return back()
                ->with('error', $msg);
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', [User::class, $id]);

        $user = $this->userRepo->show($id);

        // Verificar que el usuario exista
        if (empty($user)) {
            return redirect('account/users')->withErrors('El usuario indicado no existe.');
        }

        // Asignar imagen avatar
        if (empty($user->avatar) || !Storage::disk('public')->exists($user->avatar) ) {
            $user->avatar = 'profile_small.jpg';
            //$user->avatar = (!$user->sex) ? 'img/women.jpg' : 'img/avatar_man.jpg';
        }

        // Consultar todos los roles
        $roles = $this->roleRepo->findBy( ['id', 'name'], [ [ 'status', '=', 1 ] ], false);

        return view('acl::Users.edit', [
            'user' => $user,
            'roles'=> $roles
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $this->authorize('update', [User::class, $id]);

        // Validar que el correo no se repita
        $wheres = array(
            [ 'email', '=', $request['email'] ],
            [ 'id', '<>', $id ]
        );

        $emailUnique = $this->userRepo->findBy('id', $wheres, false);

        if (!$emailUnique->isEmpty()) {
            return back()->withErrors('El correo ya está registrado. Intente con otro correo.')->withInput();
        }


        // Obtener los datos del request
        $data = $request->except(['_token', 'avatar', 'password_confirmation']);


        // Cambiar el campo role por id_role
        $data['id_role'] = $data['role'];
        unset($data['role']);

        // Se guarda la imagen avatar
        if ($request->hasFile('avatar')) {
            $data['avatar'] = Storage::disk('public')->put('avatars', $request->file('avatar'));
        }


        // Guardar el usuario
        $idUser = $this->userRepo->update($id, $data);


        if (empty($idUser)) {
            return back()->withErrors('Ocurrió un error al crear el usuario.')->withInput();
        }


        return redirect('account/users/show/'.$id);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', [User::class, $id]);

        $this->userRepo->user->id = $id;
        $deleted = $this->userRepo->delete();

        if ($deleted == false) {
            return back()->withErrors('Ocurrió un error al eliminar el usuario.');
        }

        return redirect('account/users');
    }



}
