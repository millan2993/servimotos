<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 7/01/18
 * Time: 7:48
 */

namespace Servimotos\Modules\acl\Repositories\Users;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\clients\models\Clients;
use Servimotos\Modules\rh\Models\Employee;
use Servimotos\User;

class UserRepository
{

    public $user;

    /**
     * UserRepository constructor.
     * @param $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }


    public function all()
    {
        return $this->user->index();
    }


    /**
     * Consultar la información de un usuario
     *
     * @param $id
     *
     * @return array
     */
    public function show($id)
    {
        try {
            return $this->user->show($id);

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al consultar un usuario.', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }

    public function findBy($fields = array(), $wheres = array(), $first = false)
    {
        return $this->user->findBy($fields, $wheres, $first);
    }

    public function create()
    {
        try {
            $owner_relation = $this->getRelationsByNumber($this->user->type);
            $this->user->{$owner_relation['column']} = $this->user->id_owner;

            $this->user->email = $this->getEmailByType($owner_relation['table'], $this->user->id_owner);
            if (empty($this->user->email)) {
                Session::push('error', 'No se ha podido crear el usuario porque el propietario no tiene correo electrónico.');
                throw new Exception('');
            }

            $exists = $this->userExistsByEmail($this->user->email);
            if ($exists) {
                Session::push('error', 'El usuario ya existe.');
                throw new Exception();
            }

            return $this->user->insert();

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al crear un usuario.', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }

    public function update($id, $user)
    {
        return $this->user->edit($id, $user);
    }


    /**
     * Eliminar el usuario actual
     *
     * @return bool|null
     */
    public function delete()
    {
        try {
            return $this->user->remove($this->user->id);

        } catch (Exception $e) {
            throw $e;
        }
    }


    public function hasRole($role, $id = null)
    {
        $id = (is_null($id)) ? Auth::user()->id : $id;

        return $this->user->hasRoles($role, $id);
    }


    /**
     * Obtener el nombre de la tabla de la cual está asociado el usuario
     *
     * @param int $identifier
     * @return array
     */
    public function getRelationsByNumber(int $identifier)
    {
        switch ($identifier) {
            case 2:
                return ['table' => 'clients', 'column' => 'id_client'];
                break;
            default:
                return ['table' => 'employees', 'column' => 'id_employee'];
                break;
        }
    }


    /**
     * Obtener el correo según la entidad
     *
     * @param string $table
     * @param int    $id
     *
     * @return string
     */
    public function getEmailByType(string $table, int $id)
    {
        return $this->user->getEmailByType($table, $id);
    }


    /**
     * Verificar si un usuario existe por medio del correo
     *
     * @param string $email
     * @return boolean
     */
    public static function userExistsByEmail(string $email)
    {
        return User::userExistsByEmail($email);
    }


    /**
     * Eliminar el usuario asociado al empleado dado
     *
     * @param Employee $employee
     */
    public static function deleteByEmployee(Employee $employee)
    {
        try {
            $id_user = User::getByEmployee($employee->id);
            $user = new User();
            $user->id = $id_user;

            $userRepo = new UserRepository($user);
            $userRepo->delete();

        } catch (Exception $e) {
            throw $e;
        }
    }


    /**
     * Eliminar el usuario asociado a la entidad
     *
     * @param Employee|Clients $entity debe ser una instancia de empleados o clientes
     */
    public static function deleteByEntity($entity)
    {
        try {
            if ($entity instanceof Employee) {
                self::deleteByEmployee($entity);

            } else if ($entity instanceof Clients) {
                self::deleteByClient($entity);
            }

        } catch (Exception $e) {
            Session::push('error', 'No se ha podido eliminar la cuenta de usuario.');
            throw $e;
        }
    }

    /**
     * Eliminar el usuario asociado al cliente dado
     *
     * @param Clients $client
     */
    public static function deleteByClient(Clients $client)
    {
        try {
            $id_user = User::getByClient($client->id);
            $user = new User();
            $user->id = $id_user;

            $userRepo = new UserRepository($user);
            $userRepo->delete();

        } catch (Exception $e) {
            throw $e;
        }
    }

}