<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/11/17
 * Time: 11:46 AM
 */

namespace Servimotos\Modules\acl\Repositories\Roles;

use Exception;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\acl\Models\Role;

class RoleRepository implements RoleRepositoryInterface
{
    private $model;

    /**
     * RoleRepository constructor.
     *
     * @param Role $role
     *
     * @internal param $model
     */
    public function __construct(Role $role)
    {
        $this->model = $role;
    }

    public function all()
    {
        return $this->model->index();
    }

    public function show($id)
    {
        return $this->model->show($id);
    }

    public function findBy($fields = array(), $wheres = array(), $first = false)
    {
        return $this->model->findBy($fields, $wheres, $first);
    }

    public function create($role)
    {
        $this->model->name = $role['name'];
        $this->model->desc = $role['desc'];
        $this->model->status = $role['status'];

        $this->model->setPermissions( $role['permissions'] );

        return $this->model->insert($role);
    }


    public function update($id, $role)
    {
        $this->model->id = $id;
        $this->model->name = $role['name'];
        $this->model->desc = $role['desc'];
        $this->model->status = $role['status'];

        $this->model->setPermissions( $role['permissions']);

        return $this->model->edit($id);
    }


    /**
     * Eliminar un role
     *
     * @param int $id
     *
     * @return array
     */
    public function delete($id)
    {
        try {
            $this->model->id = $id;

            $this->model->deletePermissions();

            if (Role::hasUsers($id)) {
                Session::push('error', 'El role no puede ser eliminado porque tiene usuarios asociados.');
                throw new Exception();
            }

            return $this->model->remove($id);

        } catch (Exception $e) {
            Session::push('Ocurrió un error al eliminar el rol.');
            throw $e;
        }
    }


    public function hasPermission($role, $permission)
    {
        return $this->model->hasPermission($role, $permission);
    }

    public function getPermissions($type)
    {
        return $this->model->getPermissions($type);
    }


}