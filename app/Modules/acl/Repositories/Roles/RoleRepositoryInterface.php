<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/11/17
 * Time: 11:42 AM
 */

namespace Servimotos\Modules\acl\Repositories\Roles;


use Servimotos\Repositories\RepositoryDefault;

interface RoleRepositoryInterface extends RepositoryDefault
{
    /**
     * Obtener los permisos asociados a un rol
     *
     * @param int $type
     * @return mixed
     */
    public function getPermissions($type);

    /**
     * Consultar si un rol tiene un permiso especifico, y éste se encuentra activo
     *
     * @param int    $role
     * @param string|int $permission
     * @return boolean
     */
    public function hasPermission($role, $permission);
}