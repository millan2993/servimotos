<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 4/11/17
 * Time: 10:47 PM
 */

namespace Servimotos\Modules\acl\Repositories\Permissions;


use Servimotos\Modules\acl\Models\Permission;

class PermissionRepository implements PermissionRepositoryInterface

{

    private $model;

    /**
     * PermissionRepository constructor.
     *
     * @param Permission $permission
     *
     * @internal param $model
     */
    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }


    public function all()
    {
        return $this->model->index();
        //return (is_null($permissions) || is_array($permissions)) ? $permissions : $this->model->newCollection($permissions->toArray());
    }

    public function show($id)
    {
        return $this->model->show($id);
    }

    public function findBy($fields = null, $wheres = null, $first = false)
    {
        return $this->model->findBy($fields, $wheres, $first);
    }

    public function create($module)
    {
        return $this->model->insert($module);
    }

    public function update($id, $module)
    {
        return $this->model->edit($id, $module);
    }

    public function delete($id)
    {
        return $this->model->remove($id);
    }


}