<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 4/11/17
 * Time: 10:46 PM
 */

namespace Servimotos\Modules\acl\Repositories\Permissions;


use Servimotos\Repositories\RepositoryDefault;

interface PermissionRepositoryInterface extends RepositoryDefault
{

}