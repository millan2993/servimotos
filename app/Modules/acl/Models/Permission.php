<?php

namespace Servimotos\Modules\acl\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class Permission extends Model
{
    protected $fillable = [
        'name', 'desc', 'status', 'values', 'type'
    ];


    /**
     * @desc Obtener todos los permissos
     * @return mixed
     *
     * @see     01/10/2017
     * @author  Alex Millán
     */
    public static function index()
    {
        try {
            return DB::table('permissions')
                ->select('id', 'name', 'desc', 'status')
                ->orderBy('permissions.name')
                ->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los permisos.', [$e->getMessage()]);
            return false;
        }
    }


    /**
     * @desc    Obtener todos los permisos según los campos y condicionales
     *
     * @param   array $fields
     * @param   array $wheres
     * @param   bool  $first
     *
     * @return mixed
     * @see     2017/11/05
     * @author  Alex Millán
     */
    public static function findBy($fields = array(), $wheres = array(), $first = false)
    {
        if (empty($fields)) {
            $fields = [ 'id', 'name' ];
        }

        try {
            $permissions = DB::table('permissions')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $permissions->first() : $permissions->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar permisos.' [$e->getMessage()]);
            return null;
        }
    }


    /**
     * @desc     Ver un permisos
     *
     * @param   $id   int
     *
     * @return  mixed
     * @see     01/10/2017
     * @author  Alex Millán
     */
    public static function show($id)
    {
        try {
            return DB::table('permissions')
                ->select('id', 'name', 'desc', 'status', 'values', 'created_at', 'updated_at',
                    DB::raw('CASE type WHEN 1 THEN "Acceso" WHEN 2 THEN "Parametro" END AS type')
                )
                ->where('permissions.id', $id)
                ->first();

        } catch (QueryException $e) {
            logger()->error('Error al seleccionar un módulo.', [$e->getMessage()]);
            return null;
        }
    }



    /**
     * @desc Insertar un permiso
     *
     * @param array $permission
     *
     * @return mixed
     *
     * @see     01/10/2017
     * @author  Alex Millán
     */
    public static function insert($permission)
    {
        $permission['created_at'] = Carbon::now();
        $permission['updated_at'] = Carbon::now();

        DB::beginTransaction();
        try {
            $id = DB::table('permissions')->insertGetId($permission);
            DB::commit();

            return $id;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al Crear un permiso.', [$e->getMessage()]);
            return null;
        }
    }



    /**
     * @desc Actualizar un permiso
     *
     * @param int   $id
     * @param array $permission
     *
     * @return mixed
     *
     * @see     01/10/2017
     * @author  Alex Millán
     */
    public static function edit($id, $permission)
    {
        $permission['updated_at'] = Carbon::now();
        $updated = null;

        DB::beginTransaction();
        try {
            $updated = DB::table('permissions')
                ->where('id', '=', $id)
                ->update($permission);

            DB::commit();
            return $updated;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar el permiso: ' .$id, [$e->getMessage()]);
            return null;
        }
    }



    /**
     * @desc Eliminar un permiso
     *
     * @param int $id
     *
     * @return mixed
     *
     * @see     01/10/2017
     * @author  Alex Millán
     */
    public static function remove($id)
    {
        $deleted = null;

        DB::beginTransaction();
        try {
            $deleted = DB::table('permissions')
                ->where('id', $id)
                ->delete();

            DB::commit();
            return $deleted;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar el permiso: ' .$id, [$e->getMessage()]);
            return null;
        }
    }


    public static function getBy($columns = array('id', 'name'), $wheres = array())
    {
        return DB::table('permissions')
            ->select($columns)
            ->where($wheres)
            ->get();
    }


}
