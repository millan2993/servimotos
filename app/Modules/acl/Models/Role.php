<?php

namespace Servimotos\Modules\acl\Models;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

/**
 * @property int id
 * @property string name
 * @property string desc
 * @property boolean status
 * @property mixed created_at
 * @property mixed updated_at
 */
class Role extends Model
{
    protected $fillable = [
        'name', 'desc', 'status'
    ];


    /**
     * @var array
     */
    private $permissions;


    /**
     * @param array $permissions
     */
    public function setPermissions($permissions)
    {
        $this->permissions = empty($permissions) ? array() : $permissions;
    }



    /**
     * @desc    Ver todos los roles
     * @return  mixed
     *
     * @see     17/10/2017
     * @author  Alex Millán
     */
    public function index()
    {
        try {
            return DB::table('roles')
                ->select('id', 'name', 'desc', 'status')
                ->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar roles filtrados.', [$e->getMessage()]);
            return array();
        }

    }



    /**
     * @param array $fields
     * @param array $wheres
     * @param boolean $first
     * @return array|mixed
     */
    public static function findBy($fields, $wheres = array(), $first)
    {
        try {
            $roles = DB::table('roles')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $roles->first() : $roles->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar roles filtrados.', [$e->getMessage()]);
            return array();
        }
    }
    
    

    /**
     * @desc    Mostrar detalles del rol especificado
     *
     * @param   int $id
     *
     * @return  mixed
     *
     * @see     17/10/2017
     * @author  Alex Millán
     */
    public function show($id)
    {
        try {
            return DB::table('roles')
                ->select('id', 'name', 'desc', 'status', 'created_at', 'updated_at')
                ->where('id', '=', $id)
                ->first();

        } catch (QueryException $e) {
            logger()->error('Error al seleccionar un rol.', [$e->getMessage()]);
            return null;
        }
    }



    /**
     * @desc    Crear un role
     *
     * @return  mixed
     *
     * @see     17/10/2017
     * @author  Alex Millán
     */
    public function insert()
    {
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();

        $data = array(
            'name' => $this->name,
            'desc' => $this->desc,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        );

        DB::beginTransaction();
        try {
            $this->id = DB::table('roles')->insertGetId($data);

            // Guardar los permisos para ese role
            $this->savePermissions();

            DB::commit();
            return $this->id;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al Crear un módulo.', [$e->getMessage()]);
            throw $e;
        }
    }



    /**
     * @desc    Editar un rol
     *
     * @return  mixed
     *
     * @see     17/10/2017
     * @author  Alex Millán
     */
    public function edit()
    {
        $this->updated_at = Carbon::now();

        $data = array(
            'name' => $this->name,
            'desc' => $this->desc,
            'status' => $this->status,
            'updated_at' => $this->updated_at
        );

        try {
            DB::beginTransaction();

            $this->savePermissions();

            $updated = DB::table('roles')
                ->where('id', '=', $this->id)
                ->update($data);

            DB::commit();

            return $updated;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar el rol. ', [$e->getMessage()]);
            throw $e;
        }

    }



    /**
     * @desc    Eliminar un role
     *
     * @see     17/10/2017
     * @author  Alex Millán
     * @return array
     */
    public function remove()
    {
        DB::beginTransaction();
        try {

            $deleted = DB::table('roles')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

            return $deleted;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un rol: ', [$e->getMessage()]);
            throw $e;
        }

    }



    /**
     * Consultar si un rol tiene un permiso especifico, y éste se encuentra activo
     *
     * @param int        $role
     * @param string|int $permission
     * @return boolean
     */
    public function hasPermission($role, $permission)
    {
        $role = (empty($role)) ? $this->id : $role;

        try {
            $filter = ['permissions.name', 'LIKE', $permission ];

            $wheres = array(
                ['roles.id', '=', $role],
                ['roles.status', '=', true],
                ['permissions.status', '=', true ],
                $filter
            );

            return DB::table('permissions')
                ->leftjoin('roles_permissions', 'permissions.id', '=', 'roles_permissions.id_permission')
                ->leftjoin('roles', 'roles.id', '=', 'roles_permissions.id_role')
                ->where($wheres)
                ->exists();

        } catch (QueryException $e) {
            logger()->error('Error al consultar un permiso para un rol.', [$e->getMessage()]);
            return null;
        }

    }



    /**
     * Obtener los permisos de un rol
     *
     * @param bool $status
     * @return mixed
     */
    public function getPermissions($type = null, $status = null)
    {
        $permissions = DB::table('permissions')
            ->select('permissions.id', 'permissions.name', 'permissions.status')
            ->join('roles_permissions', 'roles_permissions.id_permission', '=', 'permissions.id')
            ->where('roles_permissions.id_role', '=', $this->id);

        if (!is_null($type)) {
            $permissions = $permissions->where('permissions.type', '=', $type);
        }

        if (!is_null($status)) {
            $permissions = $permissions->where('permissions.status', '=', $status);
        }

        return $permissions->get();
    }



    /**
     * Guardar permisos para un rol
     */
    public function savePermissions()
    {
        try {
            DB::beginTransaction();
            $actuales = array();

            $permissions = DB::table('roles_permissions')
                ->select('id_permission')
                ->where('id_role', '=', $this->id)
                ->get();

            foreach ($permissions as $permission) {
                $actuales[] = $permission->id_permission;
            }


            // Obtener los permisos para eliminar
            $delete = array_diff($actuales, $this->permissions);

            DB::table('roles_permissions')
                ->where('id_role', '=', $this->id)
                ->whereIn('id_permission', $delete)
                ->delete();


            // Obtener los nuevos permisos para ser insertados
            $inserts = array_diff($this->permissions, $actuales);

            foreach ($inserts as $insert) {
                DB::table('roles_permissions')->insert( [
                    'id_role' => $this->id,
                    'id_permission' => $insert
                ]);
            }

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al guardar permisos a un rol.' . $e->getMessage());
            throw $e;
        }

    }

    /**
     * Eliminar los permisos asociados a un role
     *
     * @return void
     */
    public function deletePermissions()
    {
        try {
            DB::beginTransaction();

            DB::table('roles_permissions')
                ->where('id_role', '=', $this->id)
                ->delete();

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al guardar permisos los permisos asociados a un role.' . $e->getMessage());
            throw $e;
        }
    }


    /**
     * @param int $id_role
     *
     * @return boolean
     */
    public static function hasUsers(int $id_role)
    {
        return DB::table('users')
            ->where('id_role', $id_role)
            ->exists();
    }


}
