<?php

namespace Servimotos\Modules\acl\Policies;

use Servimotos\User;
use Servimotos\Modules\acl\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;


    public function index(User $user)
    {
        if (User::hasRole(array('superadmin', 'administrador'), $user->id) ) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'ver_roles');
    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \Servimotos\User  $user
     * @param  int  $id_role
     * @return mixed
     */
    public function view(User $user, $id_role)
    {
        $ver_admin = $this->hasPermission($user->id, $id_role);
        if ($ver_admin) {
            return true;
        }

        $wheres = array(
            [ 'id', '=', $id_role ]
        );
        $role =  Role::findBy(array('name'), $wheres, true);

        if ((new Role())->hasPermission($user->id_role, 'ver_roles') && $role->name <> 'superadmin') {
            return true;
        }


        return false;
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \Servimotos\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        $admin = User::hasRole(array('superadmin', 'administrador'), $user->id);
        if ($admin) {
            return true;
        }

        if ((new Role())->hasPermission($user->id_role, 'crear_roles')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \Servimotos\User  $user
     * @param  int  $id_role
     * @return mixed
     */
    public function update(User $user, $id_role)
    {
        $wheres = array(
            [ 'id', '=', $id_role ]
        );
        $role =  Role::findBy('name', $wheres, true);

//        los roles de administrador no se pueden editar
        if (in_array($role->name, [ 'superadmin', 'Administrador']) ) {
            return false;
        }

//        si es administrador
        $ver_admin = $this->hasPermission($user->id, $id_role);
        if ($ver_admin) {
            return true;
        }

//        si tiene permiso
        if ((new Role())->hasPermission($user->id_role, 'editar_roles')) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \Servimotos\User  $user
     * @param  int $id_role
     * @return mixed
     */
    public function delete(User $user, $id_role)
    {

        $wheres = array(
            [ 'id', '=', $id_role ]
        );
        $role =  Role::findBy('name', $wheres, true);

//        los roles de administrador no se pueden editar
        if (in_array($role->name, [ 'superadmin', 'Administrador']) ) {
            return false;
        }

        return $this->hasPermission($user->id, $id_role);
    }


    /**
     * Validar si es administrador o superadministrador, o desea visualizar esos roles
     *
     * @param $id_user
     * @param $id_role
     *
     * @return bool
     */
    private function hasPermission($id_user, $id_role)
    {
        if (User::hasRole('superadmin', $id_user)) {
            return true;
        }

//        obtener el nombre del role
        $wheres = array(
            [ 'id', '=', $id_role ]
        );
        $role =  Role::findBy(array('name'), $wheres, true);

        if ( User::hasRole('Administrador', $id_user) && $role->name <> 'superadmin' ) {
            return true;
        }

        return false;
    }
}
