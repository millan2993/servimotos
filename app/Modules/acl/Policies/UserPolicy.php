<?php

namespace Servimotos\Modules\acl\Policies;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;


    public function index(User $user)
    {
        return $this->hasPermission($user->id, $user->id_role, 'ver_usuarios');
    }

    /**
     * Determine whether the user can view the role.
     *
     * @param  \Servimotos\User  $user
     * @param  int  $id_user
     * @return mixed
     */
    public function view(User $user, $id_user)
    {
        return $this->hasPermissionsEstandar($user->id, $user->id_role, $id_user, 'ver_usuarios');
    }

    /**
     * Determine whether the user can create roles.
     *
     * @param  \Servimotos\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $this->hasPermission($user->id, $user->id_role, 'crear_usuarios');
    }

    /**
     * Determine whether the user can update the role.
     *
     * @param  \Servimotos\User  $user
     * @param  int  $id_user
     * @return mixed
     */
    public function update(User $user, $id_user)
    {
        $user_target = User::findBy('name', [ ['id', '=', $id_user] ], true);
        if ($user_target->name == 'superadmin' || $user_target->name == 'Administrador') {
            return false;
        }

        return $this->hasPermissionsEstandar($user->id, $user->id_role, $id_user, 'editar_usuarios');
    }

    /**
     * Determine whether the user can delete the role.
     *
     * @param  \Servimotos\User  $user
     * @param  int $id_user
     * @return mixed
     */
    public function delete(User $user, $id_user)
    {
        $user_target = User::findBy('name', [ ['id', '=', $id_user] ], true);
        if ($user_target->name == 'superadmin' || $user_target->name == 'Administrador') {
            return false;
        }

        return $this->hasPermissionsEstandar($user->id, $user->id_role, $id_user, 'eliminar_usuarios');
    }


    /**
     * Validar permisos básicos que no requieran un usuario especifico
     * 
     * @param $id_user
     * @param $id_role
     * @param $permission
     * @return bool
     */
    private function hasPermission($id_user, $id_role, $permission)
    {
        $status = User::hasRole(array('superadmin', 'administrador'), $id_user);

        if ($status) {
            return true;
        }

        return (new Role())->hasPermission($id_role, $permission);
    }


    /**
     * Verificar permisos de un usuario hacia el crud de otros usuarios
     *
     * @param $current_user
     * @param $role_currentUser
     * @param $id_user
     * @param $permission
     * @return bool
     */
    private function hasPermissionsEstandar($current_user, $role_currentUser, $id_user, $permission)
    {
        // Si el usuario actual es superadmin
        $superadmin = User::hasRole( 'superadmin', $current_user );
        if ($superadmin) {
            return true;
        }

        if ($current_user == $id_user) {
            return true;
        }

        $admistrador = User::hasRole( 'Administrador', $current_user );
        $user_target = User::findBy('name', [ ['id', '=', $id_user] ], true);

        if ($admistrador && $user_target->name != 'superadmin') {
            return true;
        }


        // Si el usuario actual tiene permisos y el rol del usuario a ver es diferente a superadmin
        $hasPermission = (new Role())->hasPermission($role_currentUser, $permission);
        $userSuperAdmin = User::hasRole( ['superadmin', 'Administrador'], $id_user);

        if ($hasPermission && !$userSuperAdmin) {
            return true;
        }


        return false;
    }
    
}
