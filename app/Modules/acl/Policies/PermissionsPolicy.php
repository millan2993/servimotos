<?php

namespace Servimotos\Modules\acl\Policies;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\acl\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionsPolicy
{
    use HandlesAuthorization;


    public function before(User $user, $ability)
    {
        if (User::hasRole('superadmin', $user->id)) {
            return true;
        }
    }



    public function index(User $user)
    {
        return User::hasRole('Administrador', $user->id);
    }

    /**
     * Determine whether the user can view the permission.
     *
     * @param  \Servimotos\User  $user
     * @param  int $id_permission
     * @return mixed
     */
    public function view(User $user, $id_permission)
    {
        $hasPermission = Role::hasPermission(Auth::user()->id_role, $id_permission);

        if ($hasPermission && User::hasRole('Administrador', $user->id) ) {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can create permissions.
     *
     * @param  \Servimotos\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return false;
    }

    /**
     * Determine whether the user can update the permission.
     *
     * @param  \Servimotos\User  $user
     * @param  int  $id_permission
     * @return mixed
     */
    public function update(User $user, $id_permission)
    {
        $where = array(
            [ 'id', '=', $id_permission ],
            [ 'type', '<>', 1 ]
        );

        $permissions = Permission::findBy(array('id'), $where, false);

        if ( !$permissions->isEmpty()  &&  User::hasRole('Administrador', $user->id) ) {
            return true;
        }

        return false;

    }

    /**
     * Determine whether the user can delete the permission.
     *
     * @param  \Servimotos\User  $user
     * @param  int  $id_permission
     * @return false
     */
    public function delete(User $user, $id_permission)
    {
        return false;
    }
}
