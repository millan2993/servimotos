<?php

namespace Servimotos\Modules\vehicles\Http\Controllers;

use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\clients\Repositories\ClientRepository;
use Servimotos\Modules\vehicles\Http\Requests\VehiclesRequest;
use Servimotos\Modules\vehicles\models\vehicles;
use Servimotos\Region;

class VehiclesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAll', Vehicles::class);

        // Traer de la base de datos
        $Vehicle = new Vehicles();
        $vehicles = $Vehicle->index();

        return view('vehicles::vehicles.index', [
            'vehicles' => $vehicles
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Vehicles::class);

        $countries = Region::getCountries();
        $states = Region::getStates(82);
        $cities = Region::getCities(1700);
        $clients = ClientRepository::select();

        return view('vehicles::vehicles.create', [
            'countries' => $countries,
            'states'    => $states,
            'cities'    => $cities,
            'clients'   => $clients
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|VehiclesRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(VehiclesRequest $request)
    {
        $this->authorize('create', Vehicles::class);

        $vehicle = new Vehicles();

        try {

            $nextYear = date('Y') + 1;
            if ($request->get('modelo') > $nextYear) {
                return back()
                    ->withErrors( ['msg' => 'El modelo del vehiculo indicado no es vigente.'], 'errores')
                    ->withInput();
            }

            $vehicle->placa     = $request->get('placa');
            $vehicle->marca     = $request->get('marca');
            $vehicle->modelo    = $request->get('modelo');
            $vehicle->color     = $request->get('color');
            $vehicle->id_city   = $request->get('city');
            $vehicle->id_client = $request->get('client');

            $vehicle->insert();

        } catch (QueryException $e ) {
            logger()->error('Error al crear el vehiculo.', [$e->getMessage()]);
            return back()->withErrors('Ocurrió un error al crear un vehiculo.')->withInput();

        } catch (Exception $e) {
            logger()->error('Error al crear vehiculo.', [$e->getMessage()]);
            return back()->withErrors('Ocurrió un error al crear el vehiculo')->withInput();

        }

        return redirect('account/vehicles/show/'.$vehicle->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = new Vehicles();
        $vehicle->id = $id;

        $this->authorize('view', $vehicle);

        $data = $vehicle->show();

        $cities = Region::getCities(1700);

        return view('vehicles::vehicles.show', [
            'vehicle' => $data,
            'cities' => $cities
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicle = new Vehicles();
        $vehicle->id = $id;

        $this->authorize('update', $vehicle);

        $data = $vehicle->show();

        $countries = Region::getCountries();
        $states = Region::getStates(82);
        $cities = Region::getCities(1700);
        $clients = ClientRepository::select();

        return view('vehicles::vehicles.edit', [
            'vehicle' => $data,
            'countries' => $countries,
            'states' => $states,
            'cities' => $cities,
            'clients'=> $clients
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|VehiclesRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(VehiclesRequest $request, $id)
    {
        $data = $request->all();

        $vehicle = Vehicles::find($id);

        $this->authorize('update', $vehicle);

        try {
            $vehicle->placa = $data['placa'];
            $vehicle->marca = $data['marca'];
            $vehicle->modelo = $data['modelo'];
            $vehicle->color = $data['color'];
            $vehicle->id_city = $data['city'];
            
            $vehicle->update();

        } catch (\Exception $e) {
            logger()->error('Error al crear vehiculo.', [$e->getMessage()]);
            return back()->withErrors('Ocurrió un error al actualizar el cliente');

        } catch (QueryException $e ) {
            logger()->error('Error al editar el vehiculo.', [$e->getMessage()]);
            return back()->withErrors('Ocurrió un error al eliminar un vehiculo.');
        }


        return redirect('account/vehicles/show/'.$id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicle = Vehicles::find($id);

        $this->authorize('update', $vehicle);

        try {
            Vehicles::destroy($id);
        } catch (\Exception $e) {
            logger()->error('Error al eliminar el vehiculo.', [$e->getMessage()]);
            return back()->withErrors('Ocurrió un error al eliminar el vehiculo');
        }

        return redirect('account/vehicles');
        
    }


    public function vehiclesByClient(Request $request)
    {
        try {
            $client = $request->get('client');

            if (empty($client)) {
                throw new Exception();
            }

            $vehicles = Vehicles::byClient($client);
            return response()->json([
                'vehicles' => $vehicles
            ]);

        } catch (Exception $e) {

            return response()->json([
                'status'  => 'error',
                'message' => 'Error al capturar la actividad en acción'
            ]);
        }

    }
}
