<?php

namespace Servimotos\Modules\vehicles\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehiclesRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client' => 'required|integer',
            'color'  => 'nullable|min:1|max:45',
            'placa'  => 'required|min:5|max:6',
            'marca'  => 'required|max:255',
            'modelo' => 'required|integer|min:1900',
            'city'   => 'nullable|integer'
        ];
    }


    /**
     * @return array
     */
    public function messages()
    {
        return [
        ];
    }
}
