<?php
/**
 * Created by PhpStorm.
 * User: alex ortiz
 * Date: 6/01/18
 * Time: 4:16
 */


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {


    /*
     * vehicles
     */
    Route::group(['prefix' => '/vehicles' ], function () {

        Route::get('/', 'VehiclesController@index');

        Route::get('/show/{id}', 'VehiclesController@show');

        Route::get('/create', 'VehiclesController@create');
        Route::post('/create', 'VehiclesController@store');

        Route::get('/destroy/{id}', 'VehiclesController@destroy');

        Route::get('/edit/{id}', 'VehiclesController@edit');
        Route::post('/edit/{id}', 'VehiclesController@update');


        Route::post('/vehiclesByClient', 'VehiclesController@vehiclesByClient');

    });



});




