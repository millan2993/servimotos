@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection


@section('content')

    <form id="create" role="form" method="post">
        {{ csrf_field() }}

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button id="send" class="btn btn-primary btn-sm" onclick="$('#create').submit();">Guardar cambios</button>
            </div>
            <div class="ibox-content">

                <div class="row">

                    {{-- clients --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('client')) ? 'has-error' : '' }}">
                            <label for="client" class="control-label col-xs-10">Cliente *</label>
                            <select id="client" name="client" class="form-control">
                                <option></option>
                                @foreach($clients as $client)
                                    <option value="{{ $client->id }}" {{ ($vehicle->id_client == $client->id) ? 'selected' : '' }}>
                                        {{ $client->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'client'])
                        </div>
                    </div>

                    {{-- placa --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Placa</label>
                            <input id="placa" name="placa" type="text" class="form-control" value="{{ $vehicle->placa }}">
                            @include('layouts.components.error_input', ['input' => 'placa'])
                        </div>
                    </div>

                    {{-- color --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Color</label>
                            <input id="color" name="color" type="text" class="form-control" value="{{ $vehicle->color }}">

                            @include('layouts.components.error_input', ['input' => 'color'])
                        </div>
                    </div>


                    {{-- marca --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Marca</label>
                            <input id="marca" name="marca" type="text" class="form-control" value="{{ $vehicle->marca }}">
                            @include('layouts.components.error_input', ['input' => 'marca'])
                        </div>
                    </div>

                     {{-- modelo --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Modelo</label>
                            <input id="modelo" name="modelo" type="tel" class="form-control" value="{{ $vehicle->modelo }}">
                            @include('layouts.components.error_input', ['input' => 'modelo'])
                        </div>
                    </div>

                </div>


                <div class="hr-line-dashed"></div>
                <h2 class="text-navy">Información Detallada</h2>


                <div class="row">

                    {{-- country --}}
                    <div class="col-sm-4">
                        <div class="form-group {{ ($errors->errores->has('country')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">País</label>
                            <select id="country" name="country" class="form-control">
                                <option></option>
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ $vehicle->id_country == $country->id ? 'selected' : '' }}>
                                        {{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'country'])
                        </div>
                    </div>

                    {{-- state --}}
                    <div class="col-sm-4">
                        <div class="form-group {{ ($errors->errores->has('state')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">Departamento</label>
                            <select id="state" name="state" class="form-control">
                                <option></option>
                                @foreach($states  as $state)
                                    <option value="{{ $state->id }}" {{ ($vehicle->id_state == $state->id ) ? 'selected' : '' }}>
                                        {{ $state->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'state'])
                        </div>
                    </div>

                    {{-- city --}}
                    <div class="col-sm-4">
                        <div class="form-group {{ ($errors->errores->has('city')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">Ciudad</label>
                            <select id="city" name="city" class="form-control">
                                <option></option>
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ ($vehicle->id_city == $city->id ) ? 'selected' : '' }}>
                                        {{ $city->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'city'])
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </form>


    @section('external_files_footer')
        @parent


        {{-- Select2 --}}
        <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>


        <script>
            $(document).ready(function () {

                $("#country").select2({
                    placeholder: "Seleccione un país",
                    allowClear: true,
                    width: '100%'
                });

                $("#state").select2({
                    placeholder: "Seleccione un departamento",
                    allowClear: true,
                    width: '100%'
                });

                $("#city").select2({
                    placeholder: "Seleccione una ciudad",
                    allowClear: true,
                    width: '100%'
                });

                $("#client").select2({
                    placeholder: "Seleccione un cliente"
                });

            });
        </script>
    @endsection

@endsection
   

