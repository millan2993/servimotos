@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
@endsection


@section('content')

    <form id="create" role="form" method="post">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <a class="btn btn-primary btn-sm" href="{{ url('account/vehicles/edit/'.$vehicle->id) }}">Editar</a>
                <a class="btn btn-primary btn-sm" href="{{ url('account/vehicles/destroy/'.$vehicle->id) }}">Eliminar</a>

                <div class="pull-right">
                    <a class="btn btn-primary btn-sm" href="{{ url('account/vehicles/create') }}">Crear Nuevo</a>
                </div>
            </div>


            <div class="ibox-content">

                <div class="row">

                    {{-- client  --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label for="client" class="control-label col-xs-10">Cliente</label>
                            <input id="modelo" name="modelo" type="tel" class="form-control" value="{{ $vehicle->client }}" disabled>
                        </div>
                    </div>

                    {{-- placa --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Placa</label>
                            <input id="placa" name="placa" type="text" class="form-control" value="{{ $vehicle->placa }}" disabled>
                        </div>
                    </div>

                    {{-- color --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Color</label>
                            <input id="color" name="color" type="text" class="form-control" value="{{ $vehicle->color }}" disabled>
                        </div>
                    </div>



                    {{-- marca --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Marca</label>
                            <input id="marca" name="marca" type="text" class="form-control" value="{{ $vehicle->marca }}" disabled>
                        </div>
                    </div>

                     {{-- modelo --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Modelo</label>
                            <input id="modelo" name="modelo" type="tel" class="form-control" value="{{ $vehicle->modelo }}" disabled>
                        </div>
                    </div>

                </div>


                <div class="hr-line-dashed"></div>
                <h2 class="text-navy">Información Detallada</h2>


                <div class="row">

                    {{-- country --}}
                    <div class="col-sm-4">
                        <div class="form-group {{ ($errors->has('country')) ? 'has-error' : '' }}" disabled>
                            <label for="country" class="control-label col-xs-10">País</label>
                            <input id="city" name="city" class="form-control" disabled  value="{{ $vehicle->country }}" >
                        </div>
                    </div>

                    {{-- state --}}
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="role" class="control-label col-xs-10">Departamento</label>
                            <input id="city" name="city" class="form-control" disabled  value="{{ $vehicle->state }}" >
                        </div>
                    </div>

                    {{-- city --}}
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="role" class="control-label col-xs-10">Ciudad</label>
                            <input id="city" name="city" class="form-control" disabled  value="{{ $vehicle->city }}" >
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </form>




@section('external_files_footer')
    @parent

    {{-- iCheck --}}
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>


    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

@endsection

@endsection
