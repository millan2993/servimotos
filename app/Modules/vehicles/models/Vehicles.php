<?php

namespace Servimotos\Modules\vehicles\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class vehicles extends Model
{

    protected $fillable = [
        'id', 'descripcion', 'placa', 'marca', 'modelo', 'color', 'id_city'
    ];


    /*
     * Crear un nuevo cliente
     */
    public function insert()
    {
        $id = $this->save();
        return $id;
    }


    /**
     * Obtener la información de un vehiculo
     *
     * @return Collection
     */
    public function show() {

        return DB::table('vehicles AS v')
            ->select('v.id', 'v.color', 'v.placa', 'v.marca', 'v.modelo', 'v.id_client',
                'city.name as city', 'state.name as state', 'country.name as country',
                'v.id_city', 'city.id_state', 'state.id_country',
                DB::raw('concat(c.first_name, " ", c.last_name) as client')
            )
            ->leftjoin('clients AS c', 'c.id', '=', 'v.id_client')
            ->leftjoin('cities AS city', 'city.id', '=', 'v.id_city' )
            ->leftjoin('states AS state', 'state.id', '=', 'city.id_state' )
            ->leftjoin('countries AS country', 'country.id', '=', 'state.id_country' )
            ->where('v.id', $this->id)
            ->first();
    }


    public function index()
    {
        return DB::table('vehicles AS v')
            ->select('v.id', 'v.color', 'v.id_client', 'v.marca', 'v.placa',
                DB::raw('concat(c.first_name, " ", c.last_name) as client')
            )
            ->leftjoin('clients AS c', 'c.id', '=', 'v.id_client')
            ->get();
    }


    /**
     * @param $id_client
     */
    public static function byClient($id_client)
    {
        return DB::table('vehicles AS v')
            ->select( 'v.id', 'v.placa' )
            ->where('v.id_client', $id_client)
            ->get();
    }


}
