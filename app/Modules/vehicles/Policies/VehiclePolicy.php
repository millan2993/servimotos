<?php

namespace Servimotos\Modules\vehicles\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\vehicles\models\vehicles;

class VehiclePolicy
{
    use HandlesAuthorization;


    /**
     * Si es administrador tiene todos los permisos
     *
     * @param $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isAdministrator()) {
            return true;
        }
    }


    /**
     * Tiene permiso de ver todos los vehiculos
     *
     * @param User $user
     *
     * @return bool
     */
    public function viewAll(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_todos_vehiculos');
    }


    /**
     * Determine whether the user can view the vehicles.
     *
     * @param  User  $user
     * @param  vehicles  $vehicles
     * @return mixed
     */
    public function view(User $user, vehicles $vehicles)
    {
        if ($this->viewAll($user)) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'ver_vehiculos');
    }

    /**
     * Determine whether the user can create vehicles.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'crear_vehiculos');
    }

    /**
     * Determine whether the user can update the vehicles.
     *
     * @param  User  $user
     * @param  vehicles  $vehicles
     * @return mixed
     */
    public function update(User $user, vehicles $vehicles)
    {
        return (new Role())->hasPermission($user->id_role, 'editar_vehiculos');
    }

    /**
     * Determine whether the user can delete the vehicles.
     *
     * @param  User  $user
     * @param  vehicles  $vehicles
     * @return mixed
     */
    public function delete(User $user, vehicles $vehicles)
    {
        return (new Role())->hasPermission($user->id_role, 'eliminar_vehiculos');
    }
}
