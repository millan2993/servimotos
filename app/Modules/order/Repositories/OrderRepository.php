<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 29/04/18
 * Time: 11:31
 */

namespace Servimotos\Modules\order\Repositories;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\activities\Models\Activity;
use Servimotos\Modules\activities\Repositories\ActivityRepository;
use Servimotos\Modules\order\Models\Order;


class OrderRepository implements OrderInterface
{
    /**
     * Model Order
     *
     * @var Order
     */
    public $order;


    /**
     * Prefijo para el código de la oc
     * @var string
     */
    const prefix = 'OC-';


    /**
     * OrderRepository constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }



    /**
     * Obtener todas las ordenes de compra
     *
     * @return mixed
     */
    public function index()
    {
        return $this->order->index();
    }

    /**
     * Obtener datos de la orden
     *
     * @return mixed
     */
    public function show()
    {
        try {
            $order = $this->order->show();

            if (!$order) {
                Session::push('info', 'La orden de compra no existe.');
                throw new Exception('');
            }

            return $order;

        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * @param array $fields
     * @param array $wheres
     * @param bool $first
     * @return mixed
     */
    public function findBy($fields = array(), $wheres = array(), $first)
    {

    }



    /**
     * @return self
     * @throws Exception
     */
    public function create()
    {
        $this->order->code = $this->order->code ?? uniqid(self::prefix);
        $this->order->id_status = $this->order->id_status ?? 1;

        try {
            DB::beginTransaction();

            // guardar la orden
            $this->order->insert();

            // guardar los items
            $items = $this->order->getItems();

            for ($i = 0; $i < count($items); $i++) {
                $item['id_order'] = $this->order->id;
                $item['id_service'] = $items[$i]['service'];
                $item['amount']     = $items[$i]['amount'];
                $item['price']      = $items[$i]['price'];
                $item['created_at'] = Carbon::now();
                $item['updated_at'] = Carbon::now();

                $this->addItem($item, $this->order->expected_date);
            }

            DB::commit();
            return $this;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al crear una orden.', [$e->getMessage()]);
            throw (empty($e->getMessage())) ? new Exception('Ha ocurrido un error al crear la Orden de compra') : $e;

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al crear una orden.', [$e->getMessage()]);
            throw (empty($e->getMessage())) ? new Exception('Ha ocurrido un error al crear la Orden de compra') : $e;
        }
    }

    /**
     * Actualizar una orden
     * @return mixed
     * @throws Exception
     */
    public function edit()
    {
        try {
            // editar sólo abiertas
            throw_if($this->order->id_status != Order::ABIERTA, 'Exception', 'Sólo se pueden editar ordenes abiertas.');

            $this->order->edit();

            $itemAdded = [];
            $items = $this->order->getItems();

            // crear o actualizar los items
            for ($i = 0; $i < count($items); $i++) {
                $item['id_order'] = $this->order->id;
                $item['id_service'] = $items[$i]['service'];
                $item['amount']     = $items[$i]['amount'];
                $item['price']      = $items[$i]['price'];
                $item['updated_at'] = Carbon::now();

                $itemAdded[] = $item['id_service'];

                $this->saveItem($item, $this->order->expected_date);
            }

//            Eliminar items
            $id_itemsDelete = $this->order->getItemsToDelete($this->order->id, $itemAdded);
            foreach ($id_itemsDelete as $item) {
                $this->deleteItem($item->id);
            }

            DB::commit();
            return $this;

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un item de compra.', [$e->getMessage()]);
            throw $e;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function remove()
    {
        try {
            $order = Order::find($this->order->id);

            throw_if($order->id_status != 1, 'Exception', 'Sólo se puede eliminar ordenes de compra abiertas');

            $this->order->delete();

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un item de compra.', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Obtener los items de una order
     *
     * @return mixed
     */
    public function getItemsById()
    {
        return (empty($this->order->id)) ? null : $this->order->getItemsByIdOrder($this->order->id);
    }


    /**
     * Crear o actualizar un item
     *
     * @param array     $item
     * @param string    $expected_date  for the activity
     * @throws Exception
     */
    public function saveItem(array $item, string $expected_date)
    {
        try {
            DB::beginTransaction();

//            verificar si ya existe el order_detail
            $id_order_detail = Order::getIdOrderDetails($item['id_order'], $item['id_service']);

//            si existe, actualiza lo actualiza
            if (!empty($id_order_detail)) {
                $this->updateItem($id_order_detail, $item, $expected_date);

            } else {
                $this->addItem($item, $expected_date);
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Ocurrió un error al guardar un item de compra.', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Crear un nuevo item
     *
     * @param array $item
     * @param string $expected_date
     * @throws Exception
     */
    public function addItem(array $item, string $expected_date = null)
    {
        try {
            $item['created_at'] = Carbon::now();
            $idItem = $this->order->insertItem($item);

//            crear la actividad
            $activity = new Activity();
            $activity->id_status = 1;
            $activity->end = $expected_date;
            $activity->code = uniqid();
            $activity->id_order_detail = $idItem;
            $activity->id_service = $item['id_service'];

            $activityRepo = new ActivityRepository($activity);
            $activityRepo->create();

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Ocurrió un error al crear un item de compra.', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Eliminar un item (servicio) de una orden
     *
     * @param int $id_orderDetail
     * @throws Exception
     */
    public function deleteItem(int $id_orderDetail)
    {
//        consultar el estado de la actividad

        try {
            DB::beginTransaction();

            $this->order->deleteItem($id_orderDetail);

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Ocurrió un error al eliminar un item de compra.', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Actualizar un item de compra
     *
     * @param int $id_order_detail
     * @param $item
     * @param $expected_date
     * @throws Exception
     */
    private function updateItem(int $id_order_detail, $item, $expected_date = null)
    {
        try {
            $this->order->updateItem($id_order_detail, $item);

//            actualizar la actividad
            if (!empty($expected_date)) {
                $id_activity = Activity::getIdByOrderDetail($id_order_detail);
                $activity = Activity::find($id_activity);
                $activity->end = $expected_date;
            }

            $activityRepo = new ActivityRepository($activity);
            $activityRepo->update();
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Ocurrió un error al actualizar un item de compra.', [$e->getMessage()]);
            throw $e;
        }
    }




}