<?php

namespace Servimotos\Modules\order\Models;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Servimotos\Mail\OrderFinished;
use Servimotos\Modules\rh\Models\Employee;


/**
 * Class Order
 *
*@package Servimotos\Modules\order\Models
 *
 * @property int    id
 * @property string code
 * @property int    id_client
 * @property string observation
 * @property string expected_date
 * @property string delivered_date
 * @property array  items
 * @property int    id_status
 * @property int    id_vehicle
*/
class Order extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 'id_client', 'observation', 'expected_date', 'delivered_date', 'id_status'
    ];

    const ABIERTA = 1;
    const PROCESO = 2;
    const CERRADA = 3;
    const ANULADA = 4;


    /**
     * @var array items de una oc
     */
    private $items;

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array $items
     * @return self
     */
    public function setItems(array $items)
    {
        $this->items = $items;
        return $this;
    }


    public function index()
    {
        return DB::table('orders as o')
            ->select('o.id', 'o.code', 'o.created_at', 'o.expected_date', 'o_st.name as name_status',
                    DB::Raw('concat(cl.first_name, " ", cl.last_name) as name_client')
                )
            ->leftjoin('clients as cl', 'cl.id', '=', 'o.id_client')
            ->leftjoin('orders_status as o_st', 'o_st.id', '=', 'o.id_status')
            ->get();
    }


    public function show()
    {
        try {
            return DB::table('orders as o')
                ->select('o.id', 'o.observation', 'o.expected_date', 'o.created_at', 'o.code', 'o.delivered_date',
                    'o.id_status', 'o_st.name as name_status',
                    'o.id_client', 'o.id_vehicle', 'v.placa as vehicle',
                    DB::Raw('concat(cl.first_name, " ", cl.last_name) as name_client')
                )
                ->leftjoin('clients as cl', 'cl.id', '=', 'o.id_client')
                ->leftjoin('orders_status as o_st', 'o_st.id', '=', 'o.id_status')
                ->leftjoin('vehicles AS v', 'v.id', '=', 'o.id_vehicle')
                ->where('o.id', '=', $this->id)
                ->first();
        } catch (QueryException $e) {
            logger()->error('Error al consultar una orden.', [$e->getMessage()]);
            throw $e;
        }
    }



    /**
     * Guardar datos de la Orden de compra
     * @return self
     * @throws Exception
     */
    public function insert()
    {
        try {
            DB::beginTransaction();
            $this->saveOrFail();

            DB::commit();
            return $this;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al crear una orden 111.', [$e->getMessage()]);
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al crear una orden. 2222', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Actualizar una orden
     *
     * @return $this
     * @throws Exception
     */
    public function edit()
    {
        try {
            DB::beginTransaction();

            $this->update();


            if ($this->id_status == self::CERRADA) {
                Mail::send(new OrderFinished($this));
            }

            DB::commit();
            return $this;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar una orden.', [$e->getMessage()]);
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al actualizar una orden.', [$e->getMessage()]);
            throw $e;
        }
    }






    /**
     * Crear nuevos items a la orden
     *
     * @param array $item
     * @return void
     */
    public function insertItem(array $item)
    {
        try {
            DB::beginTransaction();

            $id = DB::table('order_details')->insertGetId($item);

            DB::commit();
            return $id;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('crear un item de una orden: ', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Actualizar los datos de un item
     *
     * @param int $id
     * @param array $item
     * @return mixed
     */
    public function updateItem(int $id, array $item)
    {
        try {
            DB::beginTransaction();

            $saved = DB::table('order_details')
                ->where('id', $id)
                ->update($item);

            DB::commit();
            return $saved;

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('crear un item de una orden: ', [$e->getMessage()]);
            throw $e;
        }
    }



    /**
     * obtiene los items de la orden especificada
     *
     * @param int $id
     * @return Collection
     */
    public function getItemsByIdOrder(int $id)
    {
        return DB::table('order_details AS od')
            ->select('od.id', 'od.created_at', 'od.amount', 'od.price', 'od.observation', 's.id as id_service', 's.name', 'act.execution_percent',
                'act.id AS id_activity')
            ->join('orders AS o', 'od.id_order', '=', 'o.id')
            ->join('services AS s', 'od.id_service', '=', 's.id')
            ->leftjoin('activities AS act', 'act.id_order_detail', '=', 'od.id')
            ->where('o.id', $id )
            ->get();
    }


    /**
     * obtener el id del order details que coincida con la order y service dados
     *
     * @param int $id_order
     * @param int $id_service
     * @return boolean
     */
    public static function getIdOrderDetails(int $id_order, int $id_service)
    {
        return DB::table('order_details')
            ->where('id_order', $id_order)
            ->where('id_service', $id_service)
            ->value('id');
    }


    /**
     * Eliminar un item de una oc
     *
     * @param int $id_orderDetail
     * @return mixed
     */
    public function deleteItem(int $id_orderDetail)
    {
        try {
            DB::beginTransaction();

            DB::table('activities')
                ->where('id_order_detail', $id_orderDetail)
                ->delete();

            DB::table('order_details')
                ->where('id', $id_orderDetail)
                ->delete();

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un item de una orden: ', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Obtener id de los items que sea de la order pero no tenga esos servicio
     * Con el fin de eliminar los items de las listas
     *
     * @param int $id_order
     * @param array $id_services
     * @return Collection
     */
    public function getItemsToDelete(int $id_order, array $id_services)
    {
        return DB::table('order_details')
            ->select('id')
            ->where('id_order', $id_order)
            ->whereNotIn('id_service', $id_services)
            ->get();
    }


    /**
     * Eliminar una order de compra
     */
    public function delete()
    {
        try {
            DB::beginTransaction();

            $items = $this->getItemsByIdOrder($this->id);

            foreach ($items as $item) {
                $this->deleteItem($item->id);
            }

            DB::table('orders')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar una orden de compra: ', [$e->getMessage()]);
            throw $e;
        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al eliminar una orden de compra: ', [$e->getMessage()]);
            throw $e;
        }
    }


    /**
     * Obtener el promedio de cumplimiento en entrega de ordenes de servicio
     *
     * @return int
     */
    public function getAvgCumplimiento()
    {
        return DB::table('orders')
            ->whereNotNull('expected_date')
            ->value(
                DB::raw('ROUND(((SELECT count(*) FROM orders WHERE fechaEntrega <= expected_date) / count(*) * 100), 2) AS average')
            );
    }


    /**
     * Obtener la cantidad de ventas por mes
     *
     * @param string $lang
     *
     * @return array|Collection
     */
    public function getAmountSalesByMonth(string $lang = null)
    {
        try {
            return DB::select('call getAmountSalesByMonth(?)', [ $lang ?? 'es_CO' ]);
        } catch (QueryException $e) {
            logger()->alert('Error al crear el reporte de ventas por mes.', [$e->getMessage()]);
            return [];
        }
    }


    /**
     * Obtener cantidad de actividades excepto la dada
     *
     * @param int $id_activity
     *
     * @return int
     */
    public function getCountOtherActivities(int $id_activity) : int
    {
        return DB::table('order_details AS od')
            ->join('activities AS act', 'act.id_order_detail', '=', 'od.id')
            ->where('od.id_order', $this->id)
            ->where('act.id', '<>', $id_activity)
            ->count();
    }


    /**
     * Obtener los empleados asociados a la orden
     */
    public function getRelatedEmployees()
    {
        return DB::table('employees AS e')
            ->select('e.email')
            ->join('activities AS act', 'act.id_employee', '=', 'e.id')
            ->join('order_details AS od', 'od.id', '=', 'act.id_order_detail')
            ->where('od.id_order', $this->id)
            ->get();
    }


}
