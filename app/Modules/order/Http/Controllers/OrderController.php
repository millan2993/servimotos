<?php

namespace Servimotos\Modules\order\Http\Controllers;

use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\clients\Repositories\ClientRepository;
use Servimotos\Modules\order\Http\Requests\OrderRequest;
use Servimotos\Modules\order\Models\Order;
use Servimotos\Modules\order\Repositories\OrderRepository;
use Servimotos\Modules\services\Models\Service;
use Servimotos\Modules\vehicles\models\vehicles;

class OrderController extends Controller
{

    /**
     * Order Repository
     *
     * @var $orderRepo
     */
    private $orderRepo;


    /**
     * OrderController constructor.
     */
    public function __construct()
    {
        $this->orderRepo = new OrderRepository(new Order());
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('viewAll', Order::class);

        $orders = $this->orderRepo->index();

        return view('order::Order.index', [
            'orders' => $orders,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Order::class);

        $clients = ClientRepository::select();

        $services = Service::findBy(['id', 'name']);

        return view('order::Order.create', [
            'clients' => $clients,
            'services' => $services
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|OrderRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
        $this->authorize('create', Order::class);

        try {
            $client      = $request->get('client');
            $observation = $request->get('observation');
            $service     = $request->get('service');
            $amount      = $request->get('amount');
            $price       = $request->get('price');
            $id_vehicle  = $request->get('vehicle');

            /* datetime */
            $expected_date = $request->get('expected_date');
            $expected_time = $request->get('expected_time');
            $date = (empty($expected_date)) ? null : Carbon::createFromFormat('Y-m-d H:iA', $expected_date .' '. $expected_time);

            $items = [];
            for ($i = 0; $i < count($service); $i++) {
                $item = [];
                $item['service'] = $service[$i];
                $item['amount'] = $amount[$i];
                $item['price'] = $price[$i];

                $items[] = $item;
            }

            $this->orderRepo->order->id_client = $client;
            $this->orderRepo->order->observation = $observation ;
            $this->orderRepo->order->expected_date = $date;
            $this->orderRepo->order->id_vehicle = $id_vehicle;
            $this->orderRepo->order->setItems($items);

            $this->orderRepo->create();

            return redirect('account/orders/show/'. $this->orderRepo->order->id);

        } catch (Exception $e) {
            logger()->error('Ocurrio un error al crear una orden de compra.', [$e->getMessage(), $e->getTrace()]);
            Session::push('error', 'Ocurrió un error al crear la orden de compra.');
            return back()
                ->withInput();
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $this->orderRepo->order->id = $id;
            $order = $this->orderRepo->show();

//            validar permisos
            if (!auth()->user()->can('view', [Order::class, $id])) {
                return response(view('errors.403'), 403);
            }

            $items = $this->orderRepo->getItemsById();

            return view('order::Order.show', [
                'order' => $order,
                'items' => $items
            ]);

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al mostrar una orden de compra', [$e->getMessage(), $e->getTrace()]);
            Session::push('info', 'Ocurrió un error al consultar la orden de compra.');
            return back();
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->orderRepo->order->id = $id;
        $order = $this->orderRepo->order->show();

//            validar permisos
        if (!auth()->user()->can('update', [Order::class, $id])) {
            return response(view('errors.403'), 403);
        }

        // formatear fecha y hora
        $order->expected_time = Carbon::parse($order->expected_date)->format('h:mA');
        $order->expected_date = Carbon::parse($order->expected_date)->format('Y-m-d');

        $items = $this->orderRepo->getItemsById();

        $clients = ClientRepository::select();
        $services = Service::findBy(['id', 'name']);
        $vehicles = Vehicles::byClient($order->id_client);

        return view('order::Order.edit', [
            'order' => $order,
            'items' => $items,
            'clients'  => $clients,
            'services' => $services,
            'vehicles' => $vehicles
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request|OrderRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(OrderRequest $request, $id)
    {
        try {

            $this->orderRepo->order->id = $id;

//            validar permisos
            if (!auth()->user()->can('update', [Order::class, $id])) {
                return response(view('errors.403'), 403);
            }


            $service     = $request->get('service');
            $amount      = $request->get('amount');
            $price       = $request->get('price');

            // formatear fecha
            $expected_date = $request->get('expected_date');
            $expected_time = $request->get('expected_time');
            $date = (empty($expected_date)) ? null : Carbon::createFromFormat('Y-m-d H:iA', $expected_date .' '. $expected_time);

            // organizar items
            $items = [];
            for ($i = 0; $i < count($service); $i++) {
                $item = [];
                $item['service'] = $service[$i];
                $item['amount'] = $amount[$i];
                $item['price'] = $price[$i];

                $items[] = $item;
            }


            $this->orderRepo->order = Order::find($id);
            $this->orderRepo->order->id_client = $request->get('client');
            $this->orderRepo->order->observation = $request->get('observation');
            $this->orderRepo->order->id_vehicle  = $request->get('vehicle');
            $this->orderRepo->order->expected_date = $date;
            $this->orderRepo->order->setItems($items);

            $this->orderRepo->edit();

            return redirect('account/orders/show/'. $id);

        } catch (Exception $e) {
            logger()->error('Ocurrio un error al editar la orden de compra.', [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withErrors('Ocurrió un error al editar la orden de compra.')
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->orderRepo->order = Order::find($id);

            if (empty($this->orderRepo->order)) {
                Session::push('info', 'La orden de compra no existe.');
                throw new Exception('');
            }

//            validar permisos
            if (!auth()->user()->can('update', [Order::class, $id])) {
                return response(view('errors.403'), 403);
            }

            $this->orderRepo->remove();

            return redirect('account/orders');

        } catch (Exception $e) {
            logger()->error('Ocurrio un error al eliminar la orden de compra.', [$e->getMessage(), $e->getTrace()]);
            return back()
                ->withErrors('Ocurrió un error al eliminar la orden de compra.')
                ->withInput();
        }
    }
}
