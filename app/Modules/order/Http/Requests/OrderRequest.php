<?php

namespace Servimotos\Modules\order\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{

    /**
     * Definir el tipo de error al formulario
     *
     * @var string
     */
    protected $errorBag = 'errores';


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client'        => 'nullable|integer',
            'vehicle'       => 'nullable|integer',
            'expected_date' => 'nullable|required_with:expected_time|date',
            'expected_time' => 'nullable|required_with:expected_date',  /*|date_format:h:mA*/
            'service.*'     => 'required|integer|distinct',
            'amount.*'      => 'required|integer',
            'price.*'  => 'required|integer'

        ];
    }
}
