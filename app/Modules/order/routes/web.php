<?php
/**
 * Created by PhpStorm.
 * User: Alex Millán
 * Date: 28/04/18
 * Time: 21:34
 */


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {


    /*
     * vehicles
     */
    Route::group(['prefix' => '/orders' ], function () {

        Route::get('/', 'OrderController@index');

        Route::get('/show/{id}', 'OrderController@show');

        Route::get('/create', 'OrderController@create');
        Route::post('/create', 'OrderController@store');

        Route::get('/edit/{id}', 'OrderController@edit');
        Route::post('/edit/{id}', 'OrderController@update');

        Route::get('/destroy/{id}', 'OrderController@destroy');
    });



});




