<?php

namespace Servimotos\Modules\order\Policies;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\order\Models\Order;

use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;


    /**
     * Si es administrador tiene todos los permisos
     *
     * @param $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isAdministrator()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the order.
     *
     * @param  User  $user
     * @return mixed
     */
    public function view(User $user, int $id_order)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_order');
    }

    /**
     * Determine whether the user can create orders.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'crear_order');
    }

    /**
     * Determine whether the user can update the order.
     *
     * @param  User $user
     * @param int $id_order
     *
     * @return mixed
     */
    public function update(User $user, int $id_order)
    {
        return (new Role())->hasPermission($user->id_role, 'editar_order');
    }

    /**
     * Determine whether the user can delete the order.
     *
     * @param  User  $user
     * @param  int   $id_order
     * @return mixed
     */
    public function delete(User $user, int $id_order)
    {
        return (new Role())->hasPermission($user->id_role, 'eliminar_order');
    }


    /**
     * Determine whether the user can create activities.
     *
     * @param  User  $user
     * @return mixed
     */
    public function viewAll(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_todos_order');
    }
}
