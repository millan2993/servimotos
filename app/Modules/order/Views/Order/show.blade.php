@extends('layouts.account')

@section('external_files_header')
  @parent
  <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endsection


@section('content')

  <form id="createOC" role="form" method="post">
    {{ csrf_field() }}

    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <a href="{{ url('account/orders/edit', $order->id) }}" id="btn_edit" class="btn btn-primary btn-sm">Editar</a>
        <a href="{{ url('account/orders/create') }}" id="btn_create" class="btn btn-primary btn-sm">Nuevo</a>
        <div class="pull-right">
          <span class="badge badge-primary">{{ $order->name_status }}</span>
        </div>
      </div>

      <div class="ibox-content">

        <div class="row">

          {{-- code --}}
          <div class="col-md-3 col-sm-6">
            <div class="form-group">
              <label for="code" class="control-label col-xs-10">Código</label>
              <input id="code" name="code" type="text" class="form-control" value="{{ $order->code }}" disabled>
            </div>
          </div>

          {{-- fecha creacion --}}
          <div class="col-md-3 col-md-push-6 col-sm-6">
            <div class="form-group">
              <label for="expected_created" class="control-label col-xs-10">Fecha creación</label>
              <input id="expected_created" name="expected_date" type="text" class="form-control"
                     value="{{ $order->created_at }}" disabled>
            </div>
          </div>

        </div>


        <div class="hr-line-dashed"></div>


        <div class="row">

          {{-- client --}}
          <div class="col-md-3 col-sm-6">
            <div class="form-group">
              <label for="client" class="control-label col-xs-10">Cliente</label>
              <input id="client" name="client" type="text" class="form-control" value="{{ $order->name_client }}"
                     disabled>
            </div>
          </div>

          {{-- vehicle --}}
          <div class="col-md-2 col-sm-6">
            <div class="form-group">
              <label for="vehicle" class="control-label col-xs-10">Vehículo</label>
              <input id="vehicle" name="vehicle" type="text" class="form-control" value="{{ $order->vehicle }}"
                     disabled>
            </div>
          </div>


          {{-- fecha esperada --}}
          <div class="col-md-2 col-md-push-3 col-sm-6">
            <div class="form-group">
              <label for="expected_created" class="control-label col-xs-10">F. esperada</label>
              <input id="expected_created" name="expected_date" type="text" class="form-control"
                     value="{{ $order->expected_date }}" disabled>
            </div>
          </div>

          {{-- fecha entregada --}}
          <div class="col-md-2 col-md-push-3 col-sm-6">
            <div class="form-group">
              <label for="expected_date" class="col-md-10 control-label">F. entregada</label>
              <input id="expected_date" name="expected_date" type="text" class="form-control"
                     value="{{ $order->delivered_date }}" disabled>
            </div>
          </div>

        </div>


        {{-- Items --}}
        <div class="row">

          <div class="ibox float-e-margins">
            <div class="ibox-title">
              <h5>Servicios soliticitados</h5>
            </div>


            <div class="ibox-content table-responsive">
              <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                <thead>
                <tr>
                  <th>Servicio</th>
                  <th id="orders-cant" data-content="Cant." data-content-sm="Cantidad"></th>
                  <th id="orders-vlr-unidad" data-content="Vlr. unidad" data-content-sm="Valor unidad" class="text-inline"></th>
                  <th>Subtotal</th>
                </tr>
                </thead>
                <tbody id="items">

                @foreach($items as $item)
                  <tr class="item" style="cursor: pointer" onclick="window.location.href = '{{ url('account/activities/show/' . $item->id_activity) }}'">

                    {{-- service --}}
                    <td class="form-group col-sm-5 text-inline">
                      <span class="form-control">{{ $item->name }}</span>
                    </td>

                    {{-- amount --}}
                    <td class="col-md-2 form-group col-sm-1">
                      <span class="form-control">{{ $item->amount }}</span>
                    </td>

                    {{-- price unit --}}
                    <td class="col-md-2 form-group col-sm-2">
                      <span class="form-control">{{ $item->price }}</span>
                    </td>

                    {{-- subtotal --}}
                    <td class="col-md-2 form-group col-sm-2">
                      <span class="form-control">{{ $item->amount * $item->price }}</span>
                    </td>

                    <td class="col-md-1 col-sm-1 form-group text-center">
                      <small class="text-inline">{{ $item->execution_percent or 0 }} %</small>
                      <div class="progress progress-mini">
                        <div style="width: {{ $item->execution_percent or 0 }}%;" class="progress-bar"></div>
                      </div>
                    </td>

                    {{-- comment --}}
                    <td class="col-md-1 col-sm-1 input-group-addon" style="cursor: pointer;">
                      <i class="fa fa-comment" aria-hidden="true" data-container="body" data-toggle="popover"
                         data-placement="left" data-content="{{ $item->observation }}"></i>
                    </td>


                  </tr>
                @endforeach

                </tbody>
              </table>
            </div>
          </div>

        </div>


        {{-- Observaciones --}}
        <div class="row">
          <div class="col-xs-12 form-group">
            <label for="observation" class="col-md-10 control-label">Observación</label>
            <textarea class="col-md-6 col-sm-12 col-xs-12" id="observation" name="observation" rows="4"
                      readonly>{{ $order->observation }}</textarea>
          </div>
        </div>


      </div>
    </div>
  </form>

  @section('external_files_footer')
    @parent

    {{-- iCheck --}}
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

    {{-- Select2 --}}
    <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

    <!-- Clock picker -->
    <script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>

  @endsection

@endsection
