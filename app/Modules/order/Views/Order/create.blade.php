@extends('layouts.account')

@section('external_files_header')
  @parent
  <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
  <link href="{{ asset('css/plugins/clockpicker/clockpicker.css') }}" rel="stylesheet">
@endsection


@section('content')

  <form id="createOC" role="form" method="post">
    {{ csrf_field() }}

    <div class="ibox float-e-margins">
      <div class="ibox-title">
        <button id="send" class="btn btn-primary btn-sm" onclick="$('#create').submit();">Guardar cambios</button>
      </div>

      <div class="ibox-content">

        <div class="row">

          {{-- client --}}
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="form-group {{ ($errors->errores->has('client')) ? 'has-error' : '' }}">
              <label for="client" class="control-label col-xs-10">Cliente *</label>

              <div class="input-group">
                <select id="client" name="client" class="form-control">
                  <option></option>
                  @foreach($clients as $client)
                    <option value="{{ $client->id }}" {{ (old('client') == $client->id ) ? 'selected' : '' }}>
                      {{ $client->name }}
                    </option>
                  @endforeach
                </select>

                <span class="input-group-addon">
                  <a id="add_client" href="{{ url('account/clients/create') }} " target="_blank">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                  </a>
                </span>

              </div>

              @include('layouts.components.error_input', ['input' => 'client'])
            </div>
          </div>


          {{-- vehicle --}}
          <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="form-group {{ ($errors->errores->has('vehicle')) ? 'has-error' : '' }}">
              <label for="vehicle" class="control-label col-xs-10">Vehículo</label>

              <div class="input-group">
                <select id="vehicle" name="vehicle" class="form-control"></select>

                <span class="input-group-addon">
                  <a id="add_client" href="{{ url('account/vehicles/create') }} " target="_blank">
                    <i class="fa fa-plus-circle" aria-hidden="true"></i>
                  </a>
                </span>

              </div>

              @include('layouts.components.error_input', ['input' => 'vehicle'])
            </div>
          </div>


          {{-- Date --}}
          <div class="col-md-3 col-md-push-1 col-sm-6 form-group {{ ($errors->errores->has('expected_date')) ? 'has-error' : '' }}">
            <label for="expected_date" class="col-md-10 control-label">Fecha esperada</label>
            <input id="expected_date" name="expected_date" type="date" class="form-control"
                   value="{{ old('expected_date') }}">

            @include('layouts.components.error_input', ['input' => 'expected_date'])
          </div>


          {{-- Time --}}
          <div class="col-md-2 col-md-push-1 col-sm-6 form-group {{ ($errors->errores->has('expected_time')) ? 'has-error' : '' }}">
            <label for="expected_time" class="col-md-10 control-label">Hora esperada</label>
            <div class="input-group clockpicker">
              <input id="expected_time" name="expected_time" type="text" readonly class="form-control"
                     value="{{ old('expected_time') }}">

              {{-- icono--}}
              <span class="input-group-addon">
                <span class="fa fa-clock-o"></span>
              </span>
            </div>
            @include('layouts.components.error_input', ['input' => 'expected_time'])
          </div>

        </div>


        <div class="hr-line-dashed"></div>


        <div class="row">

          <div class="ibox float-e-margins">

            <div class="ibox-title">
              <h5>Solicitar Servicios</h5>

              <div class="pull-right">
                <button id="add_item" type="button" class="btn btn-primary btn-xs">Adicionar servicio</button>
              </div>
            </div>


            <div class="ibox-content table-responsive">
              <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="15">
                <thead>
                <tr>
                  <th id="head-service">Servicio</th>
                  <th id="orders-cant" data-content="Cant." data-content-sm="Cantidad"></th>
                  <th id="orders-vlr-unidad" data-content="Vlr. unidad" data-content-sm="Valor unidad" class="text-inline"></th>
                  <th>Subtotal</th>
                </tr>
                </thead>
                <tbody id="items">

                <tr class="item">

                  {{-- service --}}
                  <td class="col-md-4 col-sm-4 form-group">
                    <select name="service[]" class="form-control order-service" onchange="getServicesDetailsOC(this)">
                      <option></option>
                      @foreach($services as $service)
                        <option value="{{ $service->id }}">{{ $service->name }}</option>
                      @endforeach
                    </select>
                  </td>

                  {{-- amount --}}
                  <td class="col-md-1 col-sm-1 form-group">
                    <input name="amount[]" type="number" class="form-control" value="1" min="1"
                           onchange="calcSubtotalOC(this)">
                  </td>

                  {{-- price unit --}}
                  <td class="col-md-2 col-sm-2 form-group">
                    <input name="price[]" type="number" class="form-control min-input" min="0" step="1000"
                           onchange="calcSubtotalOC(this)">
                  </td>

                  {{-- subtotal --}}
                  <td class="col-md-2 col-sm-2 form-group">
                    <input name="subtotal[]" type="text" class="form-control min-input" disabled>
                  </td>

                  {{-- delete --}}
                  <td class="col-md-1 col-sm-1 delete input-group-addon" style="cursor: pointer;">
                    <i class="fa fa-times" aria-hidden="true"></i>
                  </td>

                </tr>

                </tbody>
              </table>
            </div>
          </div>

        </div>


        {{-- Observaciones --}}
        <div class="row">
          <div class="col-xs-12">
            <div class="form-group {{ ($errors->errores->has('observation')) ? 'has-error' : '' }}">
              <label for="observation" class="col-md-10 control-label">Observación</label>
              <textarea class="col-md-6 col-sm-12 col-xs-12" id="observation" name="observation"
                        rows="4">{{ old('observation') }}</textarea>
            </div>
          </div>
        </div>


      </div>
    </div>
  </form>

@section('external_files_footer')
  @parent

  {{-- iCheck --}}
  <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

  {{-- Select2 --}}
  <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>

  <!-- Clock picker -->
  <script src="{{ asset('js/plugins/clockpicker/clockpicker.js') }}"></script>

  <script>
    $(document).ready(function () {

      $("#client").select2({
        placeholder: "Seleccione un cliente",
        width: 'resolve'
      });

      $(".order-service").select2({
        placeholder: "Seleccione un servicio",
        width: '100%'
      });

      $("#vehicle").select2({
        placeholder: "Seleccione un vehículo",
        allowClear: true
      });

      $('.clockpicker').clockpicker({
        default: 'now',
        donetext: 'Listo'
      });
    });


  </script>


@endsection

@endsection
