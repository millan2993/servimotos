@extends('layouts.account')

@section('external_files_header')
    @parent

    <link href={{ asset('css/plugins/dataTables/datatables.min.css') }} rel="stylesheet">

@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ url('account/orders/create') }}" class="btn btn-primary btn-xs">Crear Nuevo</a>
                </div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">

                                <div class="ibox-content">

                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover dataTables-example" >
                                            <thead>
                                            <tr>
                                                <th>Código</th>
                                                <th>Cliente</th>
                                                <th>Fecha esperada</th>
                                                <th>Estado</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($orders as $order)
                                                <tr>
                                                    <td class="col-md-1">{{ $order->code }}</td>
                                                    <td class="col-md-3">{{ $order->name_client }}</td>
                                                    <td class="col-md-2">{{ $order->expected_date }}</td>
                                                    <td class="col-md-1">{{ $order->name_status }}</td>
                                                    <td class="text-navy col-md-3">
                                                        <a href="{{ url('account/orders/show/'.$order->id) }}" class="btn btn-primary btn-xs">Ver</a>
                                                        <a href="{{ url('account/orders/edit/'.$order->id) }}" class="btn btn-primary btn-xs hidden-xs">Editar</a>
                                                        <a href="{{ url('account/orders/destroy/'.$order->id) }}" class="btn btn-primary btn-xs hidden-xs">Eliminar</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>

  @section('external_files_footer')
    @parent

    <script src= {{ asset('js/plugins/dataTables/datatables.min.js') }}></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            $('.dataTables-example').DataTable({
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    {extend: 'excel', title: 'Ordenes de compra'},
                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ]

            });
        });
    </script>

  @endsection

@endsection
