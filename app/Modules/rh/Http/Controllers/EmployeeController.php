<?php

namespace Servimotos\Modules\rh\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\URL;
use Servimotos\Http\Controllers\Controller;
use Servimotos\Modules\rh\Http\Requests\EmployeeRequest;
use Servimotos\Modules\rh\Models\Employee;
use Servimotos\modules\rh\Repositories\Employee\EmployeeRepository;
use Servimotos\Region;


class EmployeeController extends Controller
{
    /**
     * @var EmployeeRepository
     */
    private $employeeRepo;


    /**
     * EmployeeController constructor.
     * @param $employeeRepo
     */
    public function __construct(EmployeeRepository $employeeRepo)
    {
        $this->employeeRepo = $employeeRepo;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', Employee::class);

        $employees = $this->employeeRepo->all();

        return view('rh::Employee.index', [
            'employees' => $employees
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Employee::class);

        $countries = Region::getCountries();
        $states = Region::getStates(82);
        $cities = Region::getCities(1700);

        return view('rh::Employee.create', [
            'countries' => $countries,
            'states'    => $states,
            'cities'    => $cities
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request|EmployeeRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $this->authorize('create', Employee::class);

        try {

            $this->employeeRepo->employee->first_name = $request->get('first_name');
            $this->employeeRepo->employee->last_name  = $request->get('last_name');
            $this->employeeRepo->employee->sex        = $request->get('sex');
            $this->employeeRepo->employee->type_document = $request->get('type_document');
            $this->employeeRepo->employee->document   = $request->get('document');
            $this->employeeRepo->employee->email      = $request->get('email');
            $this->employeeRepo->employee->phone      = $request->get('phone');
            $this->employeeRepo->employee->cellphone  = $request->get('cellphone');

            $this->employeeRepo->employee->address    = $request->get('address');
            $this->employeeRepo->employee->city        = $request->get('city');

            $id = $this->employeeRepo->create();

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al crear el empleado.';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);
            return back()->withInput();
        }


        return redirect('account/employees/show/'.$id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $employee = $this->employeeRepo->show($id);

            if (empty($employee)) {
                Session::push('El empleado indicado no existe.');
                throw new Exception();
            }

            $this->authorize('view', [ Employee::class, $id ]);

            return view('rh::Employee.show', [
                'employee' => $employee
            ]);


        } catch (Exception $e) {
            $msg = 'Ocurrió un error al visualizar el empleado';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()] );
            Session::push('error', $msg);
            return redirect()->back();
        }
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $employee = $this->employeeRepo->show($id);

            if (empty($employee)) {
                Session::push('El empleado indicado no existe.');
                throw new Exception();
            }

            $this->authorize('update', [Employee::class, $id]);

            $employee = $this->employeeRepo->show($id);

            $countries = Region::getCountries();
            $states = Region::getStates(82);
            $cities = Region::getCities(1700);

            return view('rh::Employee.edit', [
                'employee'  => $employee,
                'countries' => $countries,
                'states'    => $states,
                'cities'    => $cities
            ]);

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al editar el empleado';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()] );
            Session::push('error', $msg);
            return redirect()->back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param EmployeeRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, $id)
    {
        $this->authorize('update', [Employee::class, $id ]);

        $data = $request->except('_token');

        try {

            $this->employeeRepo->employee->id           = $id;
            $this->employeeRepo->employee->first_name   = $data['first_name'];
            $this->employeeRepo->employee->last_name    = $data['last_name'];
            $this->employeeRepo->employee->sex          = $data['sex'];
            $this->employeeRepo->employee->type_document= $data['type_document'];
            $this->employeeRepo->employee->document     = $data['document'];
            $this->employeeRepo->employee->email        = $data['email'];
            $this->employeeRepo->employee->phone        = $data['phone'];
            $this->employeeRepo->employee->cellphone    = $data['cellphone'];
            $this->employeeRepo->employee->address   = $data['address'];
            $this->employeeRepo->employee->city         = $data['city'];

            $this->employeeRepo->update();

            return redirect('account/employees/show/'.$id);

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al actualizar el usuario';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);
            return back()->withInput();
        }

    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $this->employeeRepo->employee = Employee::find($id);

            $this->authorize('delete', $this->employeeRepo->employee );
            $this->employeeRepo->delete();

            return redirect('account/employees')
                ->with('success', 'El empleado ha sido eliminado.');

        } catch (Exception $e) {
            $msg = 'Ocurrió un error al eliminar el empleado';

            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);
            return back();
        }



    }


    /**
     * Cargar los datos para un select
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadToSelect()
    {
        try {
            $employees = $this->employeeRepo->loadToSelect();

            return response()->json([
                'status' => 'ok',
                'data'   => $employees
            ]);

        } catch (Exception $e) {
            return response()->json([
                'status'  => 'error',
                'data'    => [],
                'message' => 'Error al capturar la actividad en acción',
            ]);
        }
    }

}
