<?php

namespace Servimotos\Modules\rh\Policies;

use Servimotos\Modules\acl\Models\Role;
use Servimotos\User;
use Servimotos\Modules\rh\Models\Employee;
use Illuminate\Auth\Access\HandlesAuthorization;

class EmployeePolicy
{
    use HandlesAuthorization;


    /**
     * Si es administrador tiene todos los permisos
     *
     * @param $user
     * @param $ability
     *
     * @return bool
     */
    public function before($user, $ability)
    {
        if ($user->isAdministrator() && $ability != 'delete') {
            return true;
        }
    }


    /**
     * Determine whether the user can view the employee.
     *
     * @param  User $user
     * @param int $id_employee
     *
     * @return mixed
     *
     */
    public function view(User $user, int $id_employee)
    {
        if ($user->id_employee == $id_employee) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'ver_empleados');
    }

    /**
     * Determine whether the user can create employees.
     *
     * @param  User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'crear_empleados');
    }

    /**
     * Determine whether the user can update the employee.
     *
     * @param  User $user
     * @param int $id_employee
     *
     * @return mixed
     */
    public function update(User $user, int $id_employee)
    {
        if ($user->id_employee == $id_employee) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'editar_empleados');
    }

    /**
     * Determine whether the user can delete the employee.
     *
     * @param  User $user
     * @param Employee $employee
     *
     * @return mixed
     *
     */
    public function delete(User $user, Employee $employee)
    {
        if ($user->id_employee == $employee->id) {
            return false;
        }

        if ($user->isAdministrator()) {
            return true;
        }

        return (new Role())->hasPermission($user->id_role, 'eliminar_empleados');
    }


    public function index(User $user)
    {
        return (new Role())->hasPermission($user->id_role, 'ver_empleados');
    }
}
