<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 6/01/18
 * Time: 4:16
 */


Route::group([ 'prefix' => 'account',  'middleware' => 'auth' ], function () {


    /*
     * employees
     */
    Route::group(['prefix' => '/employees' ], function () {
        Route::get('/', 'EmployeeController@index');
        Route::get('/show/{id}', 'EmployeeController@show');

        Route::get('/create', 'EmployeeController@create');
        Route::post('/create', 'EmployeeController@store');

        Route::get('/edit/{id}', 'EmployeeController@edit');
        Route::post('/edit/{id}', 'EmployeeController@update');

        Route::get('/destroy/{id}', 'EmployeeController@destroy');

        Route::post('/loadToSelect/', 'EmployeeController@loadToSelect');
    });



});




