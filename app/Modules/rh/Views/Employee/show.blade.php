@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
@endsection


@section('content')

    <form id="create" role="form" method="post">

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <a class="btn btn-primary btn-sm" href="{{ url('account/employees/edit/'.$employee->id) }}">Editar</a>
                <a class="btn btn-primary btn-sm" href="{{ url('account/employees/destroy/'.$employee->id) }}">Eliminar</a>
                <a class="btn btn-primary btn-sm" href="{{ url('account/users/create/') }}">Crear usuario</a>

                <div class="pull-right">
                    <a class="btn btn-primary btn-sm" href="{{ url('account/employees/create') }}">Crear Nuevo</a>
                </div>
            </div>


            <div class="ibox-content">

                <div class="row">

                    {{-- first name --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Nombre</label>
                            <input id="first_name" name="first_name" type="text" class="form-control" value="{{ $employee->first_name }}" disabled>
                        </div>
                    </div>


                    {{-- last name --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Apellidos</label>
                            <input id="last_name" name="last_name" type="text" class="form-control" value="{{ $employee->last_name }}" disabled>
                        </div>
                    </div>


                    {{-- sex --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="control-label">Sexo</label>
                            <div class="form-control2">
                                <label class="i-checks col-md-6">
                                    <input name="sex" type="radio" value="1" {{ $employee->sex ? 'checked' : '' }} disabled> Hombre
                                </label>
                                <label class="i-checks col-md-6">
                                    <input name="sex" type="radio" value="0" {{ $employee->sex ?: 'checked' }} disabled> Mujer
                                </label>
                            </div>
                        </div>
                    </div>


                    {{-- type document --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="role" class="control-label col-xs-10">Tipo de documento</label>
                            <select id="type_document" name="type_document" class="form-control" disabled>
                                <option value="{{ $employee->id_type_document }}">{{ $employee->type_document }}</option>
                            </select>
                        </div>
                    </div>


                    {{-- document --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Numero de Documento</label>
                            <input id="document" name="document" type="text" class="form-control" value="{{ $employee->document }}" disabled>
                        </div>
                    </div>


                    {{-- email --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Correo electrónico</label>
                            <input id="email" name="email" type="email" class="form-control" value="{{ $employee->email }}" disabled>
                        </div>
                    </div>

                </div>


                <div class="hr-line-dashed"></div>
                <h2 class="text-navy">Información de contacto</h2>


                <div class="row">

                    {{-- phone --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Teléfono</label>
                            <input id="phone" name="phone" type="tel" class="form-control" value="{{ $employee->phone }}" disabled>
                        </div>
                    </div>


                    {{-- cellphone --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Celular</label>
                            <input id="cellphone" name="cellphone" type="tel" class="form-control" value="{{ $employee->cellphone }}" disabled>
                        </div>
                    </div>


                    {{-- country --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group {{ ($errors->errores->has('country')) ? 'has-error' : '' }}" disabled>
                            <label for="role" class="control-label col-xs-10">País</label>
                            <select id="country" name="country" class="form-control" disabled>
                                <option value="{{ $employee->id_country }}">{{ $employee->country }}</option>
                            </select>
                        </div>
                    </div>

                    {{-- state --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="role" class="control-label col-xs-10">Departamento</label>
                            <select id="state" name="state" class="form-control" disabled>
                                <option value="{{ $employee->id_state }}">{{ $employee->state }}</option>
                            </select>
                        </div>
                    </div>


                    {{-- city --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label for="role" class="control-label col-xs-10">Ciudad</label>
                            <select id="city" name="city" class="form-control" disabled>
                                <option value="{{ $employee->id_city }}">{{ $employee->city }}</option>
                            </select>
                        </div>
                    </div>


                    {{-- address --}}
                    <div class="col-sm-6 col-md-4">
                        <div class="form-group">
                            <label class="col-md-10 control-label">Dirección</label>
                            <input id="address" name="address" type="text" class="form-control" value="{{ $employee->address }}" disabled>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </form>




@section('external_files_footer')
    @parent

    {{-- iCheck --}}
    <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>


    <script>
        $(document).ready(function () {
            $('.i-checks').iCheck({
                checkboxClass: 'icheckbox_square-green',
                radioClass: 'iradio_square-green',
            });
        });
    </script>

@endsection

@endsection
