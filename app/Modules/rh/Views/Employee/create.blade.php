@extends('layouts.account')

@section('external_files_header')
    @parent
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@endsection


@section('content')

    <form id="create" role="form" method="post">
        {{ csrf_field() }}

        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <button id="send" class="btn btn-primary btn-sm" onclick="$('#create').submit();">Guardar cambios</button>
            </div>
            <div class="ibox-content">

                <div class="row">

                    {{-- first name --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('first_name')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Nombre *</label>
                            <input id="first_name" name="first_name" type="text" class="form-control" value="{{ old('first_name') }}">

                            @include('layouts.components.error_input', ['input' => 'first_name'])
                        </div>
                    </div>


                    {{-- last name --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('last_name')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Apellidos *</label>
                            <input id="last_name" name="last_name" type="text" class="form-control" value="{{ old('last_name') }}">

                            @include('layouts.components.error_input', ['input' => 'last_name'])
                        </div>
                    </div>


                    {{-- sex --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('sex')) ? 'has-error' : '' }}">
                            <label class="control-label">Sexo *</label>
                            <div class="form-control2">
                                <label class="i-checks col-md-6">
                                    <input name="sex" type="radio" value="1" {{ old('sex') ? 'checked' : '' }} > Hombre
                                </label>
                                <label class="i-checks col-md-6">
                                    <input name="sex" type="radio" value="0" {{ ( old('sex') !== null && !old('sex') ) ? 'checked' : '' }}> Mujer
                                </label>
                            </div>

                            @include('layouts.components.error_input', ['input' => 'sex'])
                        </div>
                    </div>


                    {{-- type document --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('type_document')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">Tipo de documento *</label>
                            <select id="type_document" name="type_document" class="form-control">
                                <option></option>
                                <option value="1" {{ (old('type_document', 1) == '1') ? 'selected' : '' }}>Cedula</option>
                                <option value="2" {{ (old('type_document') == '2') ? 'selected' : '' }}>Documento de identidad</option>
                                <option value="3" {{ (old('type_document') == '3') ? 'selected' : '' }}>Cedula extranjera</option>
                            </select>
                            @include('layouts.components.error_input', ['input' => 'type_document'])
                        </div>
                    </div>


                    {{-- document --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('document')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Numero de Documento *</label>
                            <input id="document" name="document" type="text" class="form-control" value="{{ old('document') }}">

                            @include('layouts.components.error_input', ['input' => 'document'])
                        </div>
                    </div>


                    {{-- email --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Correo electrónico</label>
                            <input id="email" name="email" type="email" class="form-control" value="{{ old('email') }}">

                            @include('layouts.components.error_input', ['input' => 'email'])
                        </div>
                    </div>

                </div>


                <div class="hr-line-dashed"></div>
                <h2 class="text-navy">Información de contacto</h2>


                <div class="row">

                    {{-- phone --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('phone')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Teléfono</label>
                            <input id="phone" name="phone" type="tel" class="form-control" value="{{ old('phone') }}">

                            @include('layouts.components.error_input', ['input' => 'phone'])
                        </div>
                    </div>


                    {{-- cellphone --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('cellphone')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Celular</label>
                            <input id="cellphone" name="cellphone" type="tel" class="form-control" value="{{ old('cellphone') }}">

                            @include('layouts.components.error_input', ['input' => 'cellphone'])
                        </div>
                    </div>


                    {{-- country --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('country')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">País</label>
                            <select id="country" name="country" class="form-control">
                                @foreach($countries as $country)
                                    <option value="{{ $country->id }}" {{ (old('country', 82) == $country->id ) ? 'selected' : '' }}>
                                        {{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'country'])
                        </div>
                    </div>


                    {{-- state --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('state')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">Departamento</label>
                            <select id="state" name="state" class="form-control">
                                @foreach($states  as $state)
                                    <option value="{{ $state->id }}" {{ (old('state', 1700) == $state->id ) ? 'selected' : '' }}>
                                        {{ $state->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'state'])
                        </div>
                    </div>


                    {{-- city --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('city')) ? 'has-error' : '' }}">
                            <label for="role" class="control-label col-xs-10">Ciudad</label>
                            <select id="city" name="city" class="form-control">
                                @foreach($cities as $city)
                                    <option value="{{ $city->id }}" {{ (old('state', 465167) == $city->id ) ? 'selected' : '' }}>
                                        {{ $city->name }}
                                    </option>
                                @endforeach
                            </select>
                            @include('layouts.components.error_input', ['input' => 'city'])
                        </div>
                    </div>


                    {{-- address --}}
                    <div class="col-md-4 col-sm-6">
                        <div class="form-group {{ ($errors->has('address')) ? 'has-error' : '' }}">
                            <label class="col-md-10 control-label">Dirección</label>
                            <input id="address" name="address" type="text" class="form-control" value="{{ old('address') }}">

                            @include('layouts.components.error_input', ['input' => 'address'])
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </form>

    @section('external_files_footer')
        @parent

        {{-- iCheck --}}
        <script src="{{ asset('js/plugins/iCheck/icheck.min.js') }}"></script>

        {{-- Select2 --}}
        <script src="{{ asset('js/plugins/select2/select2.full.min.js') }}"></script>


        <script>
            $(document).ready(function () {
                $('.i-checks').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                });

                $("#type_document").select2({
                    placeholder: "Seleccione un tipo de documento",
                    allowClear: true,
                });

                $("#country").select2({
                    placeholder: "Seleccione un país",
                    allowClear: true,
                });

                $("#state").select2({
                    placeholder: "Seleccione un departamento",
                    allowClear: true,
                });

                $("#city").select2({
                    placeholder: "Seleccione una ciudad",
                    allowClear: true,
                });

            });
        </script>
    @endsection

@endsection
