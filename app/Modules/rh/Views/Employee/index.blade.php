@extends('layouts.account')

@section('external_files_header')
    @parent

    <link href={{ asset('css/plugins/dataTables/datatables.min.css') }} rel="stylesheet">

@endsection


@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="{{ url('account/employees/create') }}" class="btn btn-primary btn-xs">Crear Nuevo</a>
                </div>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="ibox float-e-margins">

                                <div class="ibox-content">

                                    <div class="table-responsive">
                                        <table id="employees" class="table table-striped table-bordered table-hover" >
                                            <thead>
                                            <tr>
                                                <th>Nombre</th>
                                                <th class="hidden-xs">Teléfono</th>
                                                <th>Celular</th>
                                                <th class="hidden-xs hidden-sm">correo</th>
                                                <th>Acciones</th>
                                            </tr>
                                            </thead>

                                            <tbody>
                                            @foreach($employees as $employee)
                                                <tr>
                                                    <td class="col-md-3">{{ $employee->name }}</td>
                                                    <td class="col-md-2 hidden-xs">{{ $employee->phone }}</td>
                                                    <td class="col-md-2">{{ $employee->cellphone }}</td>
                                                    <td class="col-md-3 hidden-xs">{{ $employee->email }}</td>
                                                    <td class="text-navy col-md-3">
                                                        <a href="{{ url('account/employees/show/'.$employee->id) }}" class="btn btn-primary btn-xs">Ver</a>
                                                        <a href="{{ url('account/employees/edit/'.$employee->id) }}" class="btn btn-primary btn-xs hidden-xs">Editar</a>
                                                        <a href="{{ url('account/employees/destroy/'.$employee->id) }}" class="btn btn-primary btn-xs hidden-xs">Eliminar</a>
                                                    </td>
                                                </tr>
                                            @endforeach

                                            </tbody>

                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>


                </div>

            </div>
        </div>
    </div>

@section('external_files_footer')
    @parent

    <script src= {{ asset('js/plugins/dataTables/datatables.min.js') }}></script>

    <!-- Page-Level Scripts -->
    <script>
        $(document).ready(function(){
            createTable('#employees');
        });
    </script>

@endsection

@endsection
