<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 28/02/18
 * Time: 22:57
 */

namespace Servimotos\modules\rh\Repositories\Employee;

use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\acl\Repositories\Users\UserRepository;
use Servimotos\Modules\rh\Models\Employee;
use Servimotos\Region;
use Servimotos\User;

class EmployeeRepository
{

    /**
     * @var Employee Model
     */
    public $employee;


    /**
     * EmployeeRepository constructor.
     * @param Employee $employee
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }


    /**
     * @return mixed
     */
    public function all()
    {
        return $this->employee->index();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function show($id)
    {
        $this->employee->id = $id;
        return $this->employee->show();
    }

    /**
     * @param array $fields
     * @param array $wheres
     * @param bool $first
     * @return mixed
     */
    public function findBy($fields = array(), $wheres = array(), $first)
    {
        return $this->employee->findBy($fields, $wheres, $first);
    }

    /**
     * Crear un nuevo empleado
     *
     * @return mixed
     */
    public function create()
    {
        try {
            DB::beginTransaction();

            if (!empty($this->employee->city) || !empty($this->employee->address)) {
                $this->employee->id_address = Region::saveAddress($this->employee->city, $this->employee->address);
            }

            $employeeExists = self::byDocument($this->employee->document);
            if (!empty($employeeExists)) {
                Session::push('error', 'Ya existe un empleado registrado con el mismo documento de identidad.');
                throw new Exception();
            }

            $inserted = $this->employee->insert();

            DB::commit();
            return $inserted;

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Ocurrió un error al crear el empleado', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }


    /**
     * Actualizar un empleado
     *
     * @return mixed
     */
    public function update()
    {
        try {
            DB::beginTransaction();

            if (!empty($this->employee->city) || !empty($this->employee->address)) {
                $this->employee->id_address = Region::saveAddress($this->employee->city, $this->employee->address);
            }

            if (!empty($this->employee->email)) {
                $this->updateEmailUser();
            }

            $this->employee->edit();

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            $msg = 'Ocurrió un error al actualizar el empleado';
            logger()->error($msg, [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', $msg);
            throw $e;
        }
    }

    /**
     * Eliminar un empleado
     *
     * @return mixed
     */
    public function delete()
    {
        try {
            DB::beginTransaction();

//            validar si el usuario tiene histórico y no puede ser eliminado
            $hasHistorical = $this->hasHistorical();
            if ($hasHistorical) {
                Session::push('error', 'El empleado no se puede eliminar porque hace parte del histórico de la aplicación.');
                throw new Exception();
            }

            $id_address = Employee::findBy( 'id_address', ['id' => $this->employee->id], true);

            $this->employee->remove();

//            eliminar la dirección
            if (!empty($id_address)) {
                Region::deleteAddress($id_address);
            }

//           eliminar el usuario
            if (self::employeeHasUser($this->employee->id)) {
                UserRepository::deleteByEntity($this->employee);
            }

            DB::commit();

        } catch (Exception $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Cargar los datos de los empleados para un select
     *
     * @return array|Collection
     */
    public function loadToSelect()
    {
        try {
            return $this->employee->loadToSelect();

        } catch (Exception $e) {
            logger()->error('Ocurrió un error al consultar los empleados para un select');
            return array();
        }
    }


    /**
     * Actualizar el correo en usuarios
     *
     * @return void
     */
    private function updateEmailUser()
    {
        try {
            if (self::employeeHasUser($this->employee->id)) {
                $this->employee->updateEmailUser();
            }
        } catch (Exception $e) {
            logger()->error('Ocurrió un error al actualizar el correo del usuario del empleado.', [$e->getMessage(), $e->getTraceAsString()]);
            Session::push('error', 'Ocurrió un error al actualizar el correo electrónico del usuario asociado.');
            throw $e;
        }
    }


    /**
     * Verificar si el empleado tiene cuenta de usuario
     *
     * @param int $id_employee
     * @return boolean
     */
    public static function employeeHasUser(int $id_employee)
    {
        try {
            $employee = new Employee();
            $employee->id = $id_employee;
            return $employee->hasUser();

        } catch (Exception $e) {
            logger()->log('Ocurrió un error al validar si un empleado tiene usuarios.', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }


    /**
     * Obtener el empleado por documento de identidad
     *
     * @param  int $document
     * @return Collection|array
     */
    public static function byDocument(int $document)
    {
        return Employee::findBy(['id'], ['document' => $document], true);
    }


    /**
     * Validar si un empleado tiene histórico en la aplicación
     *
     * @return bool
     */
    public function hasHistorical()
    {
        try {
//            asociado a actividades
            $hasHistorical = $this->employee->hasHistoricalActivities();
            if ($hasHistorical) {
                return true;
            }

            return false;

        } catch (Exception $e) {
            return true;
        }
    }


}