<?php

namespace Servimotos\Modules\rh\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoriesProviders extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        /*
        $base = config('app.name') . '\Modules\rh\Repositories';

        $this->app->bind(
            $base . '\Employee\EmployeeRepositoryInterface',
            $base . '\Employee\EmployeeRepository'
        );
        */
    }
}
