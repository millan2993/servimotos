<?php

namespace Servimotos\Modules\rh\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

/**
 * @property int id
 * @property string first_name
 * @property string last_name
 * @property int    sex
 * @property int    type_document
 * @property int    document
 * @property string email
 * @property string cellphone
 * @property string phone
 * @property string updated_at
 * @property string created_at
 *
 * @property int id_address
 * @property int id_user
 */
class Employee extends Model
{

    /**
     * Obtiene todos los empleados
     *
     * @return mixed
     */
    public function index()
    {
        try {
            return DB::table('employees')
                ->select('id', 'phone', 'cellphone', 'email',
                    DB::raw('CONCAT(first_name, " ", last_name) as name')
                )
                ->get();
        } catch (QueryException $e) {
            throw $e;
        }
    }


    /**
     * @param string|array $fields
     * @param array     $wheres
     * @param boolean   $first
     *
     * @return mixed
     */
    public static function findBy($fields, $wheres = array(), $first = false)
    {
        try {
            $employees = DB::table('employees')
                ->where($wheres);

            if (is_string($fields)) {
                return $employees->value($fields);
            } elseif (count($fields) == 1 ) {
                return $employees->value(array_shift($fields));
            }

            $employees->select($fields);


            return ($first) ? $employees->first() : $employees->get();

        } catch (QueryException $e) {
            logger()->error('Ocurrió un error en el findBy de Employees', [$e->getMessage(), $e->getTrace()]);
            throw $e;
        }
    }

    /**
     * Obtener la información de un empleado
     *
     * @return mixed
     */
    public function show()
    {
        try {
            return DB::table('employees AS e')
                ->select( 'e.id', 'e.first_name', 'e.last_name', 'e.type_document as id_type_document', 'e.document', 'e.sex', 'e.email',
                    'e.phone', 'e.cellphone', 'e.created_at', 'e.updated_at', 'addr.primary as address',
                    'addr.id_city', 'cities.name as city',
                    'cities.id_state', 'states.name as state',
                    'states.id_country', 'countries.name as country' ,
                    DB::raw('CASE e.type_document 
                                WHEN 1 THEN "Cédula" 
                                WHEN 2 THEN "Documento de identidad" 
                                WHEN 3 THEN "Cédula extranjera" 
                            END AS type_document')
                )
                ->leftjoin('address as addr', 'addr.id', '=', 'e.id_address')
                ->leftjoin('cities', 'cities.id', '=', 'addr.id_city' )
                ->leftjoin('states', 'states.id', '=', 'cities.id_state' )
                ->leftjoin('countries', 'countries.id', '=', 'states.id_country' )
                ->where('e.id', '=', $this->id)
                ->first();

        } catch (QueryException $e) {
            throw $e;
        }
    }



    /**
     * Crear un empleado
     *
     * @return mixed
     * @throws QueryException
     */
    public function insert()
    {
        $this->created_at = Carbon::now();
        $this->updated_at = Carbon::now();

        DB::beginTransaction();
        try {
            $this->id = DB::table('employees')->insertGetId([
                'first_name' => $this->first_name,
                'last_name'  => $this->last_name,
                'sex'        => $this->sex,
                'type_document' => $this->type_document,
                'document'  => $this->document,
                'email'     => $this->email,
                'phone'     => $this->phone,
                'cellphone' => $this->cellphone,
                'id_address'   => $this->id_address,
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at
            ]);

            DB::commit();
            return $this->id;

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Editar un empleado
     *
     * @return mixed
     */
    public function edit()
    {
        $this->updated_at = Carbon::now();

        try {
            DB::beginTransaction();

            $data = [
                'first_name' => $this->first_name,
                'last_name'  => $this->last_name,
                'sex'        => $this->sex,
                'type_document' => $this->type_document,
                'document'   => $this->document,
                'email'      => $this->email,
                'phone'      => $this->phone,
                'cellphone'  => $this->cellphone,
                'id_address' => $this->id_address,
                'updated_at' => $this->updated_at
            ];

            $updated = DB::table('employees')
                ->where('id', '=', $this->id)
                ->update($data);

            DB::commit();

            return $updated;
        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Eliminar un empleado
     *
     * @return void
     */
    public function remove()
    {
        try {
            DB::beginTransaction();

            DB::table('employees')
                ->where('id', $this->id)
                ->delete();

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            throw $e;
        }
    }


    /**
     * Obtener los datos para crear un select
     *
     * @return mixed
     * @throws QueryException
     */
    public function loadToSelect()
    {
        try {
            return DB::table('employees')
                ->select('id',
                    DB::raw('concat(first_name, " ", last_name) as name'))
                ->get();

        } catch (QueryException $e) {
            logger()->error('Ocurrió un error al consulta los empleados', [$e->getMessage(), $e->getTrace()]);
            return array();
        }
    }


    /**
     * verificar si el empleado tiene un usuario
     *
     * return boolean
     */
    public function hasUser()
    {
        try {
            return DB::table('users')
                ->where('id_employee', $this->id)
                ->exists();

        } catch (QueryException $e) {
            logger()->error('Error al validar si un empleado tiene usuario.', [$e->getMessage(), $e->getTraceAsString()]);
            return false;
        }
    }


    /**
     * Actualizar el correo del usuario asociado
     *
     * @return void
     */
    public function updateEmailUser()
    {
        try {
            DB::table('users')
                ->where('id_employee', $this->id)
                ->update([
                    'email' => $this->email
                ]);

        } catch (QueryException $e) {
            logger()->error('Error al actualizar el correo del usuario asociado al empleado', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }


    /**
     * Validar si tiene histórico de actividades
     *
     * @return boolean
     */
    public function hasHistoricalActivities()
    {
        return DB::table('activities')
            ->where('id_employee', $this->id)
            ->exists();
    }


}
