<?php

namespace Servimotos\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;

class Module extends Model
{
    protected $fillable = [
        'name', 'desc', 'status'
    ];



    /**
     * @desc Ver todos los módulos en general
     * @return mixed
     *
     * @see     20/09/2017
     * @author  Alex Millán
     */
    public static function index()
    {
        try {
            return DB::table('modules')
                ->select('id', 'name', 'desc', 'status')
                ->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los módulos.', [$e->getMessage()]);
            return null;
        }
    }


    /**
     * @desc     Obtener las columnas de todos módulos
     *
     * @param array $fields
     * @param array $wheres
     * @param bool  $first
     *
     * @return mixed
     * @see      08/10/2017
     * @author   Alex Millán
     */
    public function findBy($fields, $wheres = array(), $first)
    {
        try {
            return DB::table('modules')
                ->select($fields)
                ->where($wheres)
                ->get();
        } catch (QueryException $e) {
            logger()->error('Error al buscar un módulo filtrado.', [$e->getMessage()]);
            return null;
        }
    }


    /**
     * @desc Ver un módulo
     *
     * @param int $id
     *
     * @return mixed
     *
     * @see     23/09/2017
     * @author  Alex Millán
     */
    public static function show($id)
    {
        try {
            return DB::table('modules')
                ->select('id', 'name', 'desc', 'status', 'created_at', 'updated_at')
                ->where('id', '=', $id)
                ->first();
        } catch (QueryException $e) {
            logger()->error('Error al consultar el módulo.', [$e->getMessage()]);
            return null;
        }

    }


    /**
     * @desc Guardar un módulo
     *
     * @param array $module
     *
     * @return mixed
     *
     * @see     23/09/2017
     * @author  Alex Millán
     */
    public static function insert($module)
    {
        $module['created_at'] = Carbon::now();
        $module['updated_at'] = Carbon::now();

        DB::beginTransaction();
        try {
            $id = DB::table('modules')->insertGetId($module);

            DB::commit();
            return $id;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al Crear un módulo.', [$e->getMessage()]);
            return null;
        }
    }


    /**
     * @desc Actualizar un módulo
     *
     * @param int   $id
     * @param array $module
     *
     * @return mixed
     *
     * @see     23/09/2017
     * @author  Alex Millán
     */
    public static function edit($id, $module)
    {
        $module['updated_at'] = Carbon::now()->toDateTimeString();
        DB::beginTransaction();
        try {
            $updated = DB::table('modules')
                ->where('id', '=', $id)
                ->update($module);

            DB::commit();
            return $updated;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar un módulo.', [$e->getMessage()]);
            return null;
        }

    }


    /**
     * @desc Eliminar un modulo
     *
     * @param $module
     *
     * @see     23/09/2017
     * @author  Alex Millán
     * @return boolean
     */
    public static function remove($module)
    {
        $removed = null;

        DB::beginTransaction();
        try {
            $removed = DB::table('modules')
                ->where('id', '=',$module)
                ->delete();

            DB::commit();
            return $removed;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar un módulo.', [$e->getMessage()]);
            return false;
        }

    }


    /**
     * Obtiene los componentes que perteneces a un módulo
     *
     * @param   boolean $permissions Filtrar permisos
     * @return  mixed
     * @see     30/09/2017
     * @author  Alex Millán
     */
    public function getComponents($permissions)
    {
        $wheres = array(
            [ 'id_module', '=', $this->id ],
            [ 'status', '=', $this->status ],
        );

        if ($permissions) {
            $wheres[] = [ 'name', '<>', 'permissions'];
        }

        $component = new Component();
        return $component->findBy(
            ['id', 'name'],
            $wheres
        );
    }

}
