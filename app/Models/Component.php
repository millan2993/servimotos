<?php

namespace Servimotos\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Servimotos\Modules\acl\Models\Permission;

class Component extends Model
{
    /**
     * Attributes for model
     *
     * @var array
     */
    protected $fillable = [
        'name', 'status', 'desc', 'type'
    ];


    public function findBy($fields = array(), $wheres = array(), $first = false)
    {
        try {
            $components = DB::table('components')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $components->first() : $components->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar componentes filtrados.', [$e->getMessage()]);
            return array();
        }
    }


    public function getPermissions()
    {
        $wheres = array(
            [ 'id_component', '=', $this->id ],
            [ 'type', '=', $this->type ],
            [ 'status', '=', $this->status ]
        );

        $permission = new Permission();
        return $permission->findBy(
            ['id', 'name'],
            $wheres
        );
    }


    public function getPermissionsByRole($id_role)
    {
        $params = [
            ':id_role' => $id_role,
            ':id_component' => $this->id
        ];

        $permissions = DB::select('
            SELECT p.id, p.name,
                exists(SELECT 1
                        FROM roles_permissions rp
                        WHERE rp.id_permission = p.id AND rp.id_role = :id_role) AS status
            FROM permissions p
            LEFT JOIN components comp ON (p.id_component = comp.id AND comp.status = TRUE )
            WHERE p.status = TRUE AND comp.id = :id_component
        ', $params);

        return $permissions;
    }


}
