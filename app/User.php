<?php

namespace Servimotos;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\QueryException;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Servimotos\Modules\acl\Models\Role;

/**
 * @property int    id
 * @property string name
 * @property string email
 * @property boolean status
 * @property int    id_role
 * @property int    id_owner temp
 * @property int    type
 * @property string password
 * @property int    id_employee
 * @property int    id_client
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'status', 'id_role', 'type', 'id_employee', 'id_client'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Verificar si el usuario actual o indicado tiene el rol dado
     *
     * @param  string   $role
     * @param  int      $id_user
     * @return int|null
     */
    public static function hasRole($role, $id_user)
    {
        $hasRole = false;
        $roles = array();

        if (is_string($role) || is_int($role)) {
            $roles[] = $role;
        } else {
            $roles = $role;
        }

        foreach ($roles as $role) {
            try {
                $hasRole = DB::table('roles')
                    ->leftjoin('users', 'users.id_role', '=', 'roles.id');

                if (is_int($role)) {
                    $hasRole = $hasRole->where('roles.id', '=', $role);
                } else {
                    $hasRole = $hasRole->where('roles.name', 'like', $role);
                }

                $hasRole = $hasRole->where('users.id', '=', $id_user)->count();

            } catch (QueryException $e) {
                logger()->error('Error al consultar el rol del usuario', [$e->getMessage()]);
                $hasRole = 'No se pudo consultar el rol del usuario.';
            }

            if ($hasRole > 0) {
                return true;
                break;
            }
        }

        return $hasRole;
    }



    /**
     * Obtener todos los usuarios excepto administrador
     *
     * @return array
     */
    public static function index()
    {
        try {
            return DB::table('users')
                ->select('users.id', 'users.name', 'users.email', 'users.status', 'roles.name AS role' )
                ->leftjoin('roles', 'roles.id', '=', 'users.id_role')
                ->orderBy('users.name')
                ->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar todos los roles.', [$e->getMessage()]);
            return array();
        }
    }


    /**
     * Consultar la información de un usuario
     *
     * @param $id
     *
     * @return array
     * @throws QueryException
     */
    public static function show($id)
    {
        try {
            return DB::table('users')
                ->select('users.id', 'users.name', 'users.email',
                    'users.status', 'users.created_at', 'users.updated_at', 'users.id_role', 'roles.name as role',
                    DB::raw('COALESCE(cl.id, emp.id) as entity'),
                    DB::raw('COALESCE(cl.photo, emp.photo) as avatar'),
                    DB::raw('CASE users.type 
                                WHEN 1 THEN "employees"
                                WHEN 2 THEN "clients" 
                                END AS module')
                )
                ->leftjoin('roles', 'roles.id', '=', 'users.id_role')
                ->leftjoin('clients AS cl', 'cl.id', '=', 'users.id_client')
                ->leftjoin('employees AS emp', 'emp.id', '=', 'users.id_employee')
                ->where('users.id', '=', $id)
                ->first();

        } catch (QueryException $e) {
            logger()->error('Error al consultar un usuario.', [$e->getMessage(), $e->getTraceAsString()]);
            throw $e;
        }
    }


    /**
     * Crear un nuevo usuario
     *
     * @return int
     */
    public function insert()
    {
        try {
            $user = [
                'name'       => $this->name,
                'email'      => $this->email,
                'type'       => $this->type,
                'id_role'    => $this->id_role,
                'status'     => $this->status,
                'password'   => bcrypt($this->password),
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'id_employee'=> $this->id_employee,
                'id_client'  => $this->id_client
            ];

            DB::beginTransaction();

            $id = DB::table('users')->insertGetId($user);

            DB::commit();

        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al Crear un usuario.', [$e->getMessage()]);
            throw $e;

        } catch (Exception $e) {
            DB::rollBack();
            logger()->error('Error al Crear un usuario.', [$e->getMessage()]);
            throw $e;
        }

        return $id;
    }



    /**
     * @desc    Editar los datos de un usuario
     *
     * @param   int   $id
     * @param   array $user
     * @return  mixed
     *
     * @see     26/11/2017
     * @author  Alex Millán
     */
    public static function edit($id, $user)
    {
        $user['updated_at'] = Carbon::now();

        DB::beginTransaction();
        try {
            $updated = DB::table('users')
                ->where('id', '=', $id)
                ->update($user);

            DB::commit();
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al actualizar el usuario: ' .$id, [$e->getMessage()]);
            $updated = null;
        }

        return $updated;
    }



    /**
     * @desc    Eliminar un usuario.
     *
     * @param   $id
     *
     * @return  boolean|null
     *
     * @see     26/11/2017
     * @author  Alex Millán
     */
    public static function remove($id)
    {
        DB::beginTransaction();
        try {
            $deleted = DB::table('users')
                ->where('id', $id)
                ->delete();

            DB::commit();

            return $deleted;
        } catch (QueryException $e) {
            DB::rollBack();
            logger()->error('Error al eliminar el usuario: ' .$id, [$e->getMessage()]);
            return null;
        }
    }


    /**
     * @desc    Obtener usuarios condicionales
     *
     * @param   array|string $fields
     * @param   array $wheres
     * @param   boolean $first
     * @return mixed
     * @see     26/11/2017
     * @author  Alex Millán
     */
    public static function findBy($fields, $wheres = array(), $first)
    {
        try {
            $users = DB::table('users')
                ->select($fields)
                ->where($wheres);

            return ($first) ? $users->first() : $users->get();

        } catch (QueryException $e) {
            logger()->error('Error al buscar usuarios.' [$e->getMessage()]);
            return 'Ha ocurrido un error al buscar usuarios.';
        }
    }


    /**
     * Obtener el nombre del rol para el usuario actual
     *
     * @return string
     */
    public function getRoleName() {
        return (new Role())->findBy('name', array( ['id', $this->id_role] ), true)->name;
    }


    /**
     * Obtener el correo según la entidad asociada
     *
     * @param string $table
     * @param int    $id
     * @return string
     */
    public function getEmailByType(string $table, int $id)
    {
        try {
            return DB::table($table)
                ->where('id', $id)
                ->value('email');

        } catch (QueryException $e) {
            logger()->error('Error al consultar el correo de una entidad asociada a usuarios.', $e->getMessage());
            return '';
        }
    }


    /**
     * Validar si un usuario con el correo dado existe
     *
     * @param $email
     * @return boolean
     */
    public static function userExistsByEmail(string $email)
    {
        return DB::table('users')
            ->where('email', $email)
            ->exists();
    }


    /**
     * Obtener el id del usuario asociado al empleado dado
     *
     * @param int $id
     * @return int
     */
    public static function getByEmployee(int $id)
    {
        return DB::table('users')
            ->where('id_employee', $id)
            ->value('id');
    }

    /**
     * Obtener el id del usuario asociado al empleado dado
     *
     * @param int $id
     * @return int
     */
    public static function getByClient(int $id)
    {
        return DB::table('users')
            ->where('id_client', $id)
            ->value('id');
    }


    /**
     * Validar si un usuario es administrador o superadmin
     *
     * @return boolean
     */
    public function isAdministrator()
    {
        return DB::table('users AS u')
            ->join('roles AS r', 'r.id', '=', 'u.id_role')
            ->where('u.id', $this->id)
            ->whereIn('r.name', ['superadmin', 'Administrador'])
            ->exists();
    }


    /**
     * Validar si el usuario actual es un empleado
     *
     * @return bool
     */
    public function isEmployee()
    {
        return empty(!$this->id_employee) ? true : false;
    }


    /**
     * Validar si el usuario actual es un cliente
     *
     * @return bool
     */
    public function isClient()
    {
        return empty(!$this->id_client) ? true : false;
    }
}
